<?php 

require_once("inc/configuration.php");

//header("location:calendario.php");

$page = new Page(array(
	"header"=>array(
		"title"=>"Dashboard",
		"subtitle"=>"visao geral do sistema",
		"head"=>true,
		"load_menu"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
	),
	"footer"=>array(
		"foot"=>true
	)

));
$page->setTpl("index");

?>