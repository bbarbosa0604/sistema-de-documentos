<?php 

require_once("inc/configuration.php");

//Listará apenas os usuarios ativos do sistema.
$usuarios = Usuario::usuario_list(1);

$menu = new Menu();


$page = new Page(array(
	"header"=>array(
		"title"=>"Permissoes",
		"subtitle"=>"permissoes de acesso ao sistema",
		"head-title"=>true,
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true

	),
	"usuarios"=>$usuarios,
));

$page->setTpl("permissoes");
?>