<?php
require_once("../inc/configuration.php");

$iduser = get("idusuario") != ""?get("idusuario"):0;

$menu = new Menu();

$menus = $menu->listMenus($iduser);

foreach ($menus as $key => $d) {

	if($d['nrordem'] <= 1){
		$data[$key] = array(
			"idmenu"=>$d[idmenu],
			"idpermissaomenu"=>$d[idpermissaomenu]?$d[idpermissaomenu]:0,
			"idusuario"=>$d[idusuario],
			"id"=>"jstree$d[nrordem]",
			"text"=>"$d[desmenu]",
			"icon"=>"$d[desicone]",
			"parent"=>"$d[nrordem]" == 0?"#":"jstree0",	
			"state"=>array(
				"opened"=>true,
				"disabled"=>true,
				"selected"=>$d[nrordem]==1?true:false
			)
		);

	}else{
		$data[$key] = array(
			"idmenu"=>$d[idmenu],
			"idpermissaomenu"=>$d[idpermissaomenu]?$d[idpermissaomenu]:0,
			"idusuario"=>$d[idusuario],
			"id"=>"jstree$d[nrordem]",
			"text"=>"$d[desmenu]",
			"icon"=>"$d[desicone]",
			"parent"=>"jstree0",
			"state"=>array(
				"selected"=>$d[instatus]?($d[instatus]?true:false):false
			)
		);
	}
}


echo json_encode($data);

?>