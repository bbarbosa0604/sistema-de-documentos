<?php

require_once("../inc/configuration.php");

$pessoas = new Pessoa();

$rs = $pessoas->pessoa_list();

foreach ($rs as $key => $value) {
	$content.="
		<tr>
			<td>".$value['despessoa']."</td>
			<td>".($value['desendereco']?$value['desendereco'].', '.$value['desnumero']:'')."</td>
			<td>".$value['desbairro']."</td>
			<td>".$value['descep']."</td>
			<td>".$value['descontato_telefone']."</td>
			<td>".$value['descontato_email']."</td>
			<td class='hidden-xs'>".
				"<div class='visible-md visible-lg hidden-sm hidden-xs'>
					<a href='#' class='btn btn-blue btn-xs tooltips' data-target='.pessoaview' data-original-title='Editar/Visualizar' data-url='pessoas.view.php' data-form-params='".$value['idpessoa']."'><i class='fa fa-edit'></i>
					</a>
					<a href='#' class='btn btn-xs btn-red tooltips' data-target='.removepessoa' data-form-url-delete ='actions/remove.pessoa.php' data-form-params='".$value['idpessoa']."' data-original-title='Remover'><i class='fa fa-times fa fa-white'></i>
					</a>
				</div>
			</td>
		</tr>";

}
$result ="<table class='table table-hover display' id='table_data_pessoas'>
			<thead>
				<tr>
					<th>Nome</th>
					<th>Endereco</th>
					<th>Bairro</th>
					<th>CEP</th>											
					<th>Telefones</th>									
					<th>E-mail</th>										
					<th></th>
				</tr>
			</thead>
			<tbody>"
			.$content.
			"</tbody>
		</table>";

echo $result;
?>