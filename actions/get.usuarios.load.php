<?php

require_once("../inc/configuration.php");

$usuarios = new Usuario();

$rs = $usuarios->usuario_list();

foreach ($rs as $key => $value) {
	$content.="
		<tr>
			<td>".$value['desnome']."</td>
			<td>".$value['deslogin']."</td>
			<td class='hidden-xs'>".
				($value['instatus']==0?"<span class='label label-sm label-danger'>Desativado</span>":"<span class='label label-sm label-success'>Ativo</span>").
			"</td>
			<td class='center'>".
				"<div class='visible-md visible-lg hidden-sm hidden-xs'>
					<a href='#' class='btn btn-xs btn-blue tooltips action_read' data-target='.usuario' data-toggle='modal' data-placement='top' data-original-title='Editar/Visualizar' data-form-url-load='actions/get.usuario.php' data-titlemodal='Editando Usuario' data-form-url-save='actions/save.usuario.php' data-form-params='".$value['idusuario']."'><i class='fa fa-edit'></i>
					</a>
					<a href='#' class='btn btn-xs btn-red tooltips action_read' data-target='.removeusuario' data-toggle='modal' data-placement='top' data-form-url-delete ='actions/remove.usuario.php' data-form-params='".$value['idusuario']."' data-original-title='Remover'><i class='fa fa-times fa fa-white'></i></a>
				</div>
			</td>
		</tr>";

}




$result ="<table class='table table-hover display' id='table_data_usuarios'>
			<thead>
				<tr>
					<th>Nome Completo</th>
					<th class='hidden-xs'>Login</th>
					<th class='hidden-xs'>Status</th>
					<th></th>
				</tr>
			</thead>
		<tbody>"
		.$content.
		"</tbody>
	</table>";


echo $result;
?>