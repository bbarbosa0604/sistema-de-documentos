<?php

require_once("../inc/configuration.php");

try{

$idpessoa = post("idpessoa");

$pessoa = new Pessoa();

$pessoa->get($idpessoa);


//Verifica se há alguma restrição para exclusão desta pessoa
if($pessoa->check_constraint($idpessoa)){
echo json_encode(
	array(
		"success"=>false,
		"msg"=>"Não foi possivel excluir essa PESSOA, há uma correspondencia relacionada a ela!
		</br>Se preferir, desabilite essa pesssoa para que não envie ou receba nenhuma correspondencia"
		)
	);

	exit;

}


// $pessoafisica = new PessoaFisica();
// $pessoajuridica = new PessoaJuridica();
// $pessoacontato = new Contato();
// $pessoadocumento = new Documento();
// $pessoaendereco = new Endereco();

// $pessoafisica->setidpessoa($idpessoa);
// $pessoajuridica->setidpessoa($idpessoa);
// $pessoa->setidpessoa($idpessoa);


// $pessoacontato->contato_pessoa_remove($idpessoa);
// $pessoaendereco->endereco_pessoa_remove($idpessoa);
// $pessoadocumento->documento_pessoa_remove($idpessoa);
// $pessoafisica->remove();
// $pessoajuridica->remove();
$pessoa->remove();


}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Pessoa"));

}
echo json_encode(array("success"=>true,"msg"=>"Pessoa removida com Sucesso!","titlemsg"=>"Pessoa"));

?>