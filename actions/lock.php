<?php 

require_once("../inc/configuration.php");

try{

	$string = new String();

	if(!isset($_SESSION['deslogin'])){

		throw new Exception($string->getString("lock_error_server_1"));

	}

	$usuario = Usuario::login($_SESSION['deslogin'], get("p"));

	if($usuario !== false){

		unset($_SESSION[Configuration::SESSION_KEY_LOCK]);

		$url = (get("url"))?get("url"):Configuration::URL_HOME;

		echo json_encode(array(
			"success"=>true,
			"url"=>$url,
			"successMsg"=>$string->getString("unlock_success_msg")
		));

	}else{

		throw new Exception($string->getString("login_error_server_1"));

	}

}catch(Exception $e){

	echo json_encode(array(
		"success"=>false,
		"error"=>$e->getMessage()
	));

}

?>