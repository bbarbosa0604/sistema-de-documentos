<?php

require_once("../inc/configuration.php");

$documentotipo = new DocumentoTipo();

$rs = $documentotipo->documentotipo_list(1);

foreach ($rs as $key => $value) {
	$content.="
		<tr>
			<td>".$value['desdocumentotipo']."</td>
			<td class='hidden-xs'>".
				($value['instatus']==0?"<span class='label label-sm label-danger'>Desativado</span>":"<span class='label label-sm label-success'>Ativo</span>").
				"</td>
				<td class='center'>".
				"<div class='visible-md visible-lg hidden-sm hidden-xs'>
					<a href='#' class='btn btn-xs btn-blue tooltips' data-toggle='modal' data-target='.documentotipo' data-placement='top' data-original-title='Editar/Visualizar' data-form-url-load='actions/get.documentotipo.php' data-titlemodal='Editando Tipo de Documento' data-form-url-save='actions/save.documentotipo.php' data-form-params='".$value['iddocumentotipo']."'><i class='fa fa-edit'></i>
					</a>
					<a href='#' class='btn btn-xs btn-red tooltips' data-target='.removedocumentotipo' data-toggle='modal' data-placement='top' data-form-url-delete='actions/remove.documentotipo.php' data-form-params='".$value['iddocumentotipo']."' data-original-title='Remover'><i class='fa fa-times fa fa-white'></i></a>
				</div>
			</td>
		</tr>";

}
$result ="<table class='table table-hover display' id='table_data_documentotipos'>
		<thead>
			<tr>
				<th>Tipo de Documento</th>									
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>"
		.$content.
		"</tbody>
	</table>";

echo $result;

?>