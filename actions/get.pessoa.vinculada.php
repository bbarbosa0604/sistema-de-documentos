<?php
require_once("../inc/configuration.php");

$pessoa = new Pessoa();

$rs = $pessoa->get_pessoa_vinculada(post('idpessoa'));

if(!count($rs)){
	$result = "<tr>
		<td></td>
		<td><h4><div class='text-red text-bold'>Nenhum Contato e/ou Empresa relacionados</div></h4><h4></h4></td>
		<td></td>
		<td></td>
	</tr>";
}

foreach ($rs as $key => $value) {

	$result .= $key%2==0?"<tr>":"";
	$result .="

	<td>
		<h4>
			<a class='tooltips' href='javascript:void(0)' data-target='.pessoaview' data-url='pessoas.view.php' data-form-params='".$value[idpessoa]."' data-original-title='Clique para acessar o cadastro'>
				<span class='fa ".($value[idpessoatipo]==1?'fa-user':'fa-building-o')."'></span> ".$value[despessoa]."
			</a>
			<a href='javascript:void(0)' data-target='.remove-pessoa-vinculo' data-form-url-remove='actions/remove.pessoa.vinculo.php' data-form-params='".$value[idpessoa]."' data-form-params-idpessoapai='".post('idpessoa')."' class='tooltips' data-original-title='Remover Vinculo'><i class='fa fa-times remove-pessoa-vinculo'></i>
			</a>
		</h4>
	</td>";

	$lastindex = $key;
	$result .= $key%2==1?"</tr>":"";
}
	$result .= ($lastindex + 1 > 0?($lastindex%2==0?!count($rs)?"</tr>":"<td></td></tr>":""):"");

echo($result);				

?>