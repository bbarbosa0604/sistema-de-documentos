<?php
require_once("../inc/configuration.php");

try{

$endereco = new Endereco();

$endereco->arrayToAttr($_POST);

$endereco->save();

}catch(Exception $e){

echo json_encode(
	array(
		"success"=>false,
		"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),
		"titlemsg"=>"Endereço"));
	exit;
}

echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Endereço"));

?>