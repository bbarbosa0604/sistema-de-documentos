<?php
require_once("../inc/configuration.php");

set_time_limit(300); 

try{

$body = post("mensagem");
$to = post("email");
$subject = post("assunto");
$idfiles = post("idarquivo");


$email = new Email();


$email->setPropertiesEmail(array(
	"to"=>$to,
	'subject'=>$subject,
	"body"=>$body 
));

$arquivo = new Arquivo();
$pathfileSend = array();

if(strlen($idfiles)){

	foreach (explode(";",$idfiles) as $key => $value) {
		$arquivo->get($value);
		array_push($pathfileSend,$arquivo->getdescaminho().$arquivo->getdesarquivo());	
	}

	$email->setAttachmentFiles($pathfileSend);
}


if (!$email->sendEmail()){
    echo json_encode(array("success"=>false,"msg"=>"Mailer Error: " .$email->ErrorInfo,"msgtitle"=>"Correspondencia Email"));
} else {
    echo json_encode(array("success"=>true,"msg"=>"E-mail enviado com Sucesso!","msgtitle"=>"Correspondencia Email"));

}

}catch(Exception $e){
	
	echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Correspondencia Email"));


}

//////////////////////////////////////////////////

?>