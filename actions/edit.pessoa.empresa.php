<?php

require_once("../inc/configuration.php");

try{

if (post("idpessoa") === post("idpessoapai")){

	echo json_encode(array("success"=>false,"msg"=>"Não é possivel vincular a mesma empresa e/ou pessoa a ela mesma!","titlemsg"=>"Pessoa Empresa"));
	exit;
	
}

$pessoa = new Pessoa();

$pessoa->pessoa_empresa_edit(post("idpessoa"),post("idpessoapai"));

}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Pessoa Empresa"));

}
echo json_encode(array("success"=>true,"msg"=>"Empresa alterada com Sucesso!","titlemsg"=>"Pessoa Empresa"));


?>