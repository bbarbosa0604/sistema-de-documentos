<?php
require_once("../inc/configuration.php");

try{

$sql = new SQL();


$correspondenciahistorico = $sql->arrays("CALL sp_correspondenciashistoricos_save(?, ?, ?, ?);",true, array(
	0,
	post('descorrespondenciahistorico'),
	post('idcorrespondencia'),
	$_SESSION['idusuario']
	));


$correspondenciahistorico["hour"] = date("H:i:s",strtotime($correspondenciahistorico['dtcadastro']));
$correspondenciahistorico["idday_weak"] = date("w",strtotime($correspondenciahistorico['dtcadastro']));
$correspondenciahistorico["day"]= date("d",strtotime($correspondenciahistorico['dtcadastro']));
$correspondenciahistorico["year"]=date("Y",strtotime($correspondenciahistorico['dtcadastro']));
$correspondenciahistorico["idmonth"] = date("n",strtotime($correspondenciahistorico['dtcadastro']));

}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Historico Correspondencia"));

}
echo json_encode(array("success"=>true,"result"=>$correspondenciahistorico,"msg"=>"Historico adicionando com sucesso!","titlemsg"=>"Historico Correspondencia"));

	
//////////////////////////////////////////////////

?>