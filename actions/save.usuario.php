<?php

require_once("../inc/configuration.php");

try{

$usuario = new Usuario();

$usuario->arrayToAttr($_POST);

$usuario->save();

if(!(post("idusuario")) && $usuario->getidusuario()){
	

	$menu = new Menu();
	//Definindo o acesso minimo ao sistema
	$menu->permissao_menu_save(0,1,$usuario->getidusuario(),1);
	$menu->permissao_menu_save(0,2,$usuario->getidusuario(),1);
}

}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Usuarios"));

}
echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Usuarios"));

?>