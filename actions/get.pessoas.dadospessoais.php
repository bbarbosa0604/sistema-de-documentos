<?php
require_once("../inc/configuration.php");

$pessoa = new Pessoa();

$rs = $pessoa->pessoa_fisica_juridica_get(post("idpessoa"));

$result .= 
"<tr>
	<td><h4>Nome:<h4></td>
	<td><h4><a href='#'>$rs[despessoa]</a><h4></td>
	<td></td>
	<td><h3></h3></td>
</tr>";

if($rs['idpessoatipo'] == 1){
	$result .= 
	"<tr>
		<td><h4>Nascimento:<h4></td>
		<td><h4><a href='#'>".($rs['dtnascimento']?date_format(new DateTime($rs['dtnascimento']),'d/m/Y'):NULL)."</a></h4></td>
		<td></td>	
		<td><h3></h3></td>
	</tr>
	<tr>
		<td><h4>Sexo:<h4></td>
		<td><h4><a href='#'>".($rs[dessexo] == 'F'?Feminino:Masculino)."</a></h4></td>
		<td></td>
		<td><h3></h3></td>
	</tr>";

}else{
	$result .="<tr>
		<td><h4>Ramo de Atividade:<h4></td>
		<td><h4><a href='#'>$rs[desramoatividade]</a></h4></td>
		<td></td>
		<td><h3></h3></td>
	</tr>";

}

$result .="<tr>
	<td><h4>Tipo de Pessoa:<h4></td>
	<td><h4>
			<a href='#'>".($rs['idpessoatipo']==1?'Física':'Juridica')."</a>							
	</h4></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td><h4>Status:<h4></td>
	<td><h4>".($rs[instatus] == 1?"<span class='label label-sm label-success'>Ativo</span>":"<span class='label label-sm label-danger'>Inativo</span>")."										
	</h4></td>
	<td></td>
	<td></td>
</tr>";

echo $result;


?>