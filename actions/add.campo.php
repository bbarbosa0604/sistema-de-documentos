<?php

require_once("../inc/configuration.php");

try{

	$campo = new Campo();

	$campo->setidformulario(post("idformulario"));
	$campo->setdesnomeexibicao(post("desnomeexibicao"));
	$campo->setidcampotipo(post("idcampotipo"));
	$campo->setidcampo(post("idcampo"));
	$campo->save();


	if(post("options")){
		foreach (post("options") as $val) {
			$campo->saveSelectOption($val["idcamposelect"], $val["desvalue"]);
		}
	}


}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Usuarios"));

}
echo json_encode(
	array(
		"success"=>true,
		"msg"=>"Campo inserido com sucesso!",
		"titlemsg"=>"Usuarios"
	));

?>