<?php
require_once("../inc/configuration.php");

try{

$enviotipo = new EnvioTipos();

$enviotipo->arrayToAttr($_POST);

$enviotipo->save();


}catch(Exception $e){

	echo json_encode(array("success"=>false,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Tipos de Envios"));
	exit;
}

echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Tipos de Envios"));

?>