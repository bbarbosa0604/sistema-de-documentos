<?php

require_once("../inc/configuration.php");

$sql = new Sql();

$rs_tipos = $sql->arrays("call sp_dashboard_correspondencias_tipos_get();");
$rs_status = $sql->arrays("call sp_dashboard_correspondencias_status_get();");


$color_chart = array("#20BFA9","#5E8194","#E56A6A");


foreach ($rs_tipos as $key => $value) {
		$rs_tipos[$key]['color'] = $color_chart[$key];
		$rs_status[$key]['color'] = $color_chart[$key];
}

echo json_encode(array(
	"status"=>$rs_status,
	"tipos"=>$rs_tipos,
	"colors"=>$color_chart
));


?>