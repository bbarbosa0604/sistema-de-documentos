<?php

require_once("../inc/configuration.php");

$correspondencia = new Correspondencia();

$rs = $correspondencia->correspondencia_list();

foreach ($rs as $key => $value) {
	$content.="<tr>
			<td>".date_format(new Datetime($value["dtcadastro"]),"d/m/Y")."</td>
			<td>".$value['desassunto']."</td>
			<td>".$value['despessoa_remetente']."</td>
			<td>".$value['despessoa_destinatario']."</td>
			<td>".$value['desdocumentotipo']."</td>
			<td class='hidden-xs'>";

			if($value['idstatus'] == 1){
				$content.="<span class='label label-sm label-success'>".$value['desstatus']."</span>";

			}elseif($value['idstatus'] == 2){
				$content.="<span class='label label-sm label-danger'>".$value['desstatus']."</span>";

			}else{				
				$content.="<span class='label label-sm label-primary'>".$value['desstatus']."</span>";
			}

			$content.="</td>
			<td class='center'>
				<div class='visible-md visible-lg hidden-sm hidden-xs'>
					<a href='#' class='btn btn-xs btn-blue tooltips' data-target='.correspondencia' data-toggle='modal' data-placement='top' data-original-title='Editar/Visualizar' data-form-url-load='actions/get.correspondencia.php' data-enable-all-tabs='true' data-titlemodal='Editando Correspondencia' data-form-url-save='actions/save.correspondencia.php' data-form-params='".$value['idcorrespondencia']."'><i class='fa fa-edit'></i>
					</a>";
				if($value['idstatus'] == 2){
					$content.=" <a href='#' class='btn btn-xs btn-red tooltips' data-target='.removecorrespondencia' data-toggle='modal' data-placement='top' data-form-url-delete='actions/remove.correspondencia.php' data-form-params='".$value['idcorrespondencia']."' data-form-params1='".$value['idcorrespondenciadocumento']."' data-original-title='Remover'><i class='fa fa-times fa fa-white'></i>
					</a>";
					}
				$content.="</div>
			</td>
		</tr>";

}
$result = "<table class='table table-hover display' id='table_data_correspondencia'>
			<thead>
				<tr>
					<th>Data</th>
					<th>Assunto</th>
					<th>Remetente</th>
					<th>Destinatario</th>										
					<th>Tipo</th>												
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>".$content."</tbody>
		</table>";

echo $result;
?>

