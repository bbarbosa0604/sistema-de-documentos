<?php 

require_once("../inc/configuration.php");


$obj = new Correspondencia();

$correspondencia_situacao = array();

$correspondencia = $obj->correspondencias_situacao_count();

foreach ($correspondencia as $key => $value) {


	$correspondencia_situacao[] = array(
		"title"=>$value['qtde']." - ".$value['dessituacao'],
		"start"=>$value['data_ocorrencia'],
		"idsituacao"=>$value['idsituacao'],
		"end"=>$value['data_ocorrencia'],
		"category"=>$value['dessituacao'],
		"className"=>"event-$value[dessituacao]",
		"qtde"=>$value['qtde'],	
		"allDay"=> true
		);
}


echo json_encode($correspondencia_situacao);
?>