<?php

require_once("../inc/configuration.php");

try{

	if(!post("idcorrespondenciatipo")){
		echo json_encode(array("success"=>false,"msg"=>"Parametro inválido para o campo Tipo de Correspondencia !","titlemsg"=>"Correspondencia"));

		exit;

		
	}

	if(!post("idenviotipo")){
		echo json_encode(array("success"=>false,"msg"=>"Parametro inválido para o campo Tipo de Envio !","titlemsg"=>"Correspondencia"));
		exit;
		
	}


	if(strlen($_POST['dtrecebido']) == 0){
		$_POST['dtrecebido'] = '0000-00-00';
	}
	$correspondencia = new Correspondencia();
	$correspondenciadocumento = new CorrespondenciaDocumento();

	$correspondencia->arrayToAttr($_POST);
	$correspondencia->arrayToAttr(array("idusuario"=>$_SESSION['idusuario']));

	$idcorrespondencia = $correspondencia->save();

	$correspondenciadocumento->arrayToAttr($_POST);
	$correspondenciadocumento->arrayToAttr(array("idcorrespondencia"=>$idcorrespondencia));

	$correspondenciadocumento->save();

	
}catch(Exception $e){

	echo json_encode(
		array(
			"success"=>false,
			"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Correspondencia"));
	exit;
}

echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Correspondencia"));

?>