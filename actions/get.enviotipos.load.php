<?php

require_once("../inc/configuration.php");

$enviotipo = new EnvioTipos();

$rs = $enviotipo->enviotipos_list();

foreach ($rs as $key => $value) {
	$content.="
		<tr>
			<td>".$value['desenviotipo']."</td>
			<td class='hidden-xs'>".
				($value['instatus']==0?"<span class='label label-sm label-danger'>Desativado</span>":"<span class='label label-sm label-success'>Ativo</span>").
			"</td>
			<td class='center'>".
				"<div class='visible-md visible-lg hidden-sm hidden-xs'>
					<a href='#' class='btn btn-xs btn-blue tooltips' data-toggle='modal' data-target='.enviotipo' data-placement='top' data-original-title='Editar/Visualizar' data-form-url-load='actions/get.enviotipo.php' data-titlemodal='Editando Tipo de Envio' data-form-url-save='actions/save.enviotipo.php' data-form-params='".$value['idenviotipo']."'><i class='fa fa-edit'></i>
					</a>
					<a href='#' class='btn btn-xs btn-red tooltips' data-target='.removeenviotipo' data-toggle='modal' data-placement='top' data-form-url-delete='actions/remove.enviotipo.php' data-form-params='".$value['idenviotipo']."' data-original-title='Remover'><i class='fa fa-times fa fa-white'></i></a>
				</div>
			</td>
		</tr>";

}

$result ="<table class='table table-hover display' id='table_data_enviotipos'>
			<thead>
				<tr>
					<th>Tipo de Envio</th>										
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
		<tbody>"
		.$content.
		"</tbody>
	</table>";


echo $result;
?>