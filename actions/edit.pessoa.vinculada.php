<?php

require_once("../inc/configuration.php");

try{

if (post("idpessoa") === post("idpessoapai")){

	echo json_encode(array("success"=>false,"msg"=>"Não é possivel vincular a mesma empresa e/ou pessoa a ela mesma!","titlemsg"=>"Pessoas e Empresas Relacionadas:"));
	exit;
	
}

$pessoa = new Pessoa();

$pessoa->edit_pessoa_vinculada(post("idpessoa"),post("idpessoapai"));

}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Pessoas e Empresas Relacionadas:"));

}
echo json_encode(array("success"=>true,"msg"=>"Pessoa Adicionada com Sucesso!","titlemsg"=>"Pessoas e Empresas Relacionadas:"));


?>