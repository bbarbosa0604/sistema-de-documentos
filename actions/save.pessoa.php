<?php
require_once("../inc/configuration.php");

try{

//////////////////////////////////////////////////
/*
	CRIANDO OU EDITANDO PESSOA
*/
$pessoa = new Pessoa();


if(strlen($_POST['dtnascimento']) == 0){
	$_POST['dtnascimento'] = "0000-00-00";
}

$pessoa->arrayToAttr($_POST);

$idpessoa = $pessoa->save();

//////////////////////////////////////////////////

if ($_POST["idpessoatipo"] == 1){

	$pessoafisica = new PessoaFisica();
	$pessoafisica->arrayToAttr($_POST);
	$pessoafisica->arrayToAttr(array("idpessoa"=>$idpessoa));
	$pessoafisica->save();

}else{

	$pessoajuridica = new PessoaJuridica();

	$pessoajuridica->arrayToAttr($_POST);
	$pessoajuridica->arrayToAttr(array("idpessoa"=>$idpessoa));

	$pessoajuridica->save();
}
//////////////////////////////////////////////////

//////////////////////////////////////////////////


}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Pessoa"));

}
echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Pessoa"));

	
//////////////////////////////////////////////////

?>