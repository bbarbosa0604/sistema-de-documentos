<?php
require_once("../inc/configuration.php");

try{

$menu = new Menu();

$idusuario = post("idusuario");
$idmenu = post("idmenu");
$instatus = post("instatus")?1:0;
$idpermissaomenu = post("idpermissaomenu");

$menu->permissao_menu_save($idpermissaomenu,$idmenu,$idusuario,$instatus);


}catch(Exception $e){

	echo json_encode(array("success"=>false,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Permissão de Acesso"));
	exit;
}

echo json_encode(array("success"=>true,"msg"=>"Permissão de Acesso alterada com Sucesso!","titlemsg"=>"Permissão de Acesso"));

?>