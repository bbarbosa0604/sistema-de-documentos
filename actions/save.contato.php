<?php
require_once("../inc/configuration.php");

try{

$contato = new Contato();

if(isset($_POST['inprincipal'])){

	$contato->arrayToAttr($_POST);

}else{

	$contato->arrayToAttr($_POST);
	$contato->arrayToAttr(array("inprincipal"=>"0"));

}

$contato->save();	

}catch(Exception $e){

	echo json_encode(array("success"=>false,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Contato"));
	exit;
}

echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Contato"));

?>