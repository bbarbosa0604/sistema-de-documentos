<?php

require_once("../inc/configuration.php");

try{

$usuario = new Usuario();

$usuario->setidusuario(post("idusuario"));

if($usuario->check_constraint($usuario->getidusuario())){
echo json_encode(
	array(
		"success"=>false,
		"msg"=>"Não foi possivel excluir esse Usuario, há uma(s) correspondencia(s) relacionada a ele!
		</br>Se preferir, inative esse úsuario para que não utilize mais o sistema.",
		"titlemsg"=>"Usuarios"
		)
	);

	exit;

}

$usuario->remove();


}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Usuarios"));

}
echo json_encode(array("success"=>true,"msg"=>"Usuario Removido com Sucesso!","titlemsg"=>"Usuarios"));

?>