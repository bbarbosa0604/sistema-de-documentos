<?php
require_once("../inc/configuration.php");

$arquivo = new Arquivo();

$arquivo->get(post("idarquivo"));

$full_path_file = PATH.$arquivo->getdescaminho().$arquivo->getdesarquivo();

$array_result = array(
	"success"=>false,
	"titlemsg"=>"Arquivo",
	"msg"=>"Erro ao fazer o download do arquivo e/ou talvez o arquivo tenha sido alterado"
);

if (file_exists($full_path_file) && is_file($full_path_file)) {

	$array_result = array(
		"success"=>true,
		"titlemsg"=>"Arquivo",
		"msg"=>"Download do Arquivo realizado com Sucesso!"
	);
}

echo json_encode($array_result);

?>