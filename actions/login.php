<?php 

require_once("../inc/configuration.php");

try{

	$usuario = Usuario::login(get("l"), get("p"));


	$string = new String();

	if($usuario !== false){

		foreach ($usuario->getFields() as $key => $value) {

			$_SESSION[$key] = $value;
		}

		//$pessoa = new Pessoa($usuario->getidpessoa());
		//$_SESSION['pessoa'] = $pessoa->getFields();
		//unset($pessoa);

		if(get("m")){

			setcookie(Configuration::SESSION_KEY_LOGIN, $usuario->getidusuario(), time()+(7*24*60*60));//COOKIE de uma semana

		}

/*        $projetos = new Projetos($pessoa);

        if($projetos->getSize() === 1){


            $itens = $projetos->getItens();

            $p = $itens[0];

            if($p->getdesprojeto()){

                $_SESSION[Configuration::SESSION_KEY_PROJETO] = array();

                foreach ($p->getFields() as $key => $value) {
                    if(!gettype($value)!=='object') $_SESSION[Configuration::SESSION_KEY_PROJETO][$key] = $value;
                }

                //carregando menus dos modulos do projeto selecionado
                Configuration::loadModulesMenu();

            }else{

                throw new Exception("O Projeto não existe ou você não possui permissão para acessá-lo.");

            }

        }*/

		$url = (get("url"))?get("url"):Configuration::URL_HOME;

		echo json_encode(array(
			"success"=>true,
			"url"=>$url,
			"successMsg"=>$string->getString("login_success_msg"),
			"session"=>$_SESSION
		));	

	}else{

		throw new Exception($string->getString("login_error_server_1"), 1);

	}

}catch(Exception $e){

	echo json_encode(array(
		"success"=>false,
		"error"=>$e->getMessage()
	));

}

?>