<?php
require_once("../inc/configuration.php");

try{

//////////////////////////////////////////////////
/*
	CRIANDO OU EDITANDO PESSOA
*/
$pessoa = new Pessoa();

$pessoa->arrayToAttr($_POST);

$idpessoa = $pessoa->save();

//////////////////////////////////////////////////

//////////////////////////////////////////////////

/*
	DEPENDENDO DO TIPO DA PESSOA
	ELE CRIARA E/OU EDITARA AS INFORMAÇÕES: 

	1 - PESSOA FISICA

	2 - PESSOA JURIDICA
*/
if ($_POST["idpessoatipo"] == 1){

	$pessoafisica = new PessoaFisica();
	$_POST['dtnascimento'] = "0000-00-00";
	$pessoafisica->arrayToAttr($_POST);

	$pessoafisica->arrayToAttr(array("idpessoa"=>$idpessoa));
	$pessoafisica->save();


}else{

	$pessoajuridica = new PessoaJuridica();

	$pessoajuridica->arrayToAttr($_POST);
	$pessoajuridica->arrayToAttr(array("idpessoa"=>$idpessoa));

	$pessoajuridica->save();
}
//////////////////////////////////////////////////

//////////////////////////////////////////////////

if (post("descontato-telefone") || post("descontato-email")){

	$contatos = new Contatos();
	$array_contatos = array();

	if (strlen(post("descontato-telefone")) > 0 && post("descontato-telefone")) {

		array_push($array_contatos,array(
			"idcontato"=>post("idcontato"),
			"descontato"=>post("descontato-telefone"),
			"idpessoa"=>$idpessoa,
			"idcontatotipo"=>1,
			"inprincipal"=>1
		));	
	}

	if (strlen(post("descontato-email")) > 0 && post("descontato-email")) {

		array_push($array_contatos,array(
			"idcontato"=>post("idcontato"),
			"descontato"=>post("descontato-email"),
			"idpessoa"=>$idpessoa,
			"idcontatotipo"=>2,
			"inprincipal"=>1
		));
	}

	foreach ($array_contatos as $key => $value) {

		$contatos->add(new Contato($value));

	}

	$contatos->save();

}

}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Cadastro de Pessoa"));

}
echo json_encode(array("success"=>true,"msg"=>"Cadastro da Pessoa realizado com Sucesso","titlemsg"=>"Cadastro de Pessoa"));

	
//////////////////////////////////////////////////

?>