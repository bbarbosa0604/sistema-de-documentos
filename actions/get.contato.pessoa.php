<?php
require_once("../inc/configuration.php");

$contato = new Contato();

$rs = $contato->contato_idpessoa_get(post('idpessoa'));

if(!count($rs)){
	$result = "<tr>
		<td></td>
		<td><h4><div class='text-red text-bold'>Nenhum Contato cadastrado</div></h4><h4></h4></td>
		<td></td>
		<td></td>
	</tr>";
}
foreach ($rs as $key => $value) {

	$result .="
	<tr data-mask=".$value['descontatotipo'].">
		<td><h4>".$value['descontatotipo'].":<h4></td>
		<td><h4><a href='#'>".$value['descontato']."</a><h4></td>
		<td>
			<h3><a href='#' data-toggle='modal' data-placement='top' data-original-title='Editar/Visualizar' data-target='.pessoa_contatos' class='show-tab tooltips' data-form-url-load='actions/get.contato.php' data-form-url-save='actions/save.contato.php' data-form-params-idpessoa=".$value['idpessoa']." data-form-params='".$value['idcontato']."' data-titlemodal='Editando Contato'><i class='fa fa-pencil edit-user-info'></i></a>
			</h3>
		</td>
		<td>
			<h3><a href='#' class='show-tab tooltips' data-target='.removecontato' data-original-title='Deletar' data-form-url-delete ='actions/remove.contato.php' data-form-params='".$value['idcontato']."'><i class='fa fa-times edit-user-info'></i></a>
			</h3>
		</td>
	</tr>";
	
}
echo($result);
?>