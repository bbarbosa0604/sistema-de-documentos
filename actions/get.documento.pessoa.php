<?php
require_once("../inc/configuration.php");


$obj = new Documento();

$rs = $obj->documento_idpessoa_get(post("idpessoa"));


if(!count($rs)){
	$result = "<tr>
		<td></td>
		<td><h4><div class='text-red text-bold'>Nenhum Documento cadastrado</div></h4><h4></h4></td>
		<td></td>
		<td></td>
	</tr>";
}
foreach ($rs as $key => $value) {

	$result .="
	<tr data-mask=".$value['desdocumentotipo'].">
		<td><h4>".$value['desdocumentotipo'].":<h4></td>
		<td><h4><a href='#'>".$value['desdocumento']."</a><h4></td>
		<td><h3><a href='#' data-toggle='modal' data-placement='top' data-original-title='Editar/Visualizar' data-target='.pessoa_documentos' class='show-tab tooltips' data-form-url-load='actions/get.documento.php' data-form-url-save='actions/save.documento.php' data-form-params-idpessoa=".$value['idpessoa']." data-form-params=".$value['iddocumento']." data-titlemodal='Editando Documento'><i class='fa fa-pencil edit-user-info'></i></a></h3></td>
		<td>
			<h3><a href='#' class='show-tab tooltips' data-target='.removedocumento' data-original-title='Deletar' data-form-url-delete ='actions/remove.documento.php' data-form-params='".$value['iddocumento']."'><i class='fa fa-times edit-user-info'></i></a></h3>
		</td>
	</tr>";
	
}
echo($result);

?>