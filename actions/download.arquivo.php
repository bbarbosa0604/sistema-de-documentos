<?php
require_once("../inc/configuration.php");

$arquivo = new Arquivo();

$arquivo->get(get("idarquivo"));

$full_path_file = PATH.$arquivo->getdescaminho().$arquivo->getdesarquivo();


header('Content-Type: application/octet-stream');
header('Content-Type: application/force-download');
header('Content-Disposition: attachment; filename='.basename($full_path_file));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: '.filesize($full_path_file));
ob_clean();	
flush();
readfile($full_path_file);

?>