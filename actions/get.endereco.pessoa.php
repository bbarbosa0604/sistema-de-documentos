<?php
require_once("../inc/configuration.php");

$endereco = new Endereco();

$rs = $endereco->endereco_idpessoa_get(post("idpessoa"));

if(!count($rs)){
	$result = "<tr>
		<td></td>
		<td><h4><div class='text-red text-bold'>Nenhum endereco cadastrado</div></h4><h4></h4></td>
		<td></td>
		<td></td>
	</tr>";
}
foreach ($rs as $key => $value) {

	$result .="
	<tr>
		<td><h4>End.:<h4></td>
		<td>
			<h4><a href='#'>".$value['desendereco'].", ".$value['desnumero']." - ".$value['desbairro']." - ".$value['desuf']."</a>
			<h4>
		</td>
		<td>
			<h3><a href='#' data-toggle='modal' data-placement='top' data-original-title='Editar/Visualizar' data-target='.pessoa_enderecos' class='show-tab tooltips' data-form-url-load='actions/get.endereco.php' data-form-url-save='actions/save.endereco.php' data-form-params-idpessoa=".$value['idpessoa']." data-form-params='".$value['idendereco']."' data-titlemodal='Editando Endereço'><i class='fa fa-pencil edit-user-info'></i></a></h3>
		</td>
		<td>
			<h3><a href='#' class='show-tab tooltips' data-target='.removeendereco' data-original-title='Deletar' data-form-url-delete ='actions/remove.endereco.php' data-form-params='".$value['idendereco']."'><i class='fa fa-times edit-user-info'></i></a>
			</h3>
		</td>
	</tr>";
	
}
echo($result);
?>