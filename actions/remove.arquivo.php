<?php
require_once("../inc/configuration.php");


$result_array = array(
	"success"=>true,
	"titlemsg"=>"Arquivo",
	"msg"=>"Arquivo Excluido com Sucesso!"
	);

$arquivo = new Arquivo();

$arquivo->get(post("idarquivo"));

$result = $arquivo->remove();

if(!$result){
	$result_array['success'] = $result;
	$result_array['msg'] = "Erro ao excluir o arquivo";
}

echo json_encode($result_array);	
?>