<?php

require_once("../inc/configuration.php");

try{

$enviotipo = new EnvioTipos();

$enviotipo->setidenviotipo(post("idenviotipo"));

//Verifica se há alguma restrição para exclusão deste tipo de documento
if($enviotipo->check_constraint($enviotipo->getidenviotipo())){
echo json_encode(
	array(
		"success"=>false,
		"msg"=>"Não foi possivel excluir esse Tipo de Envio, há uma(s) correspondencia(s) relacionada a ela!
		</br>Se preferir, você pode desabilitar essa opção, para que não envie ou receba nenhuma correspondencia com esse Tipo de Envio"
		)
	);

	exit;

}

$enviotipo->remove();


}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Tipo de Envio"));

}
echo json_encode(array("success"=>true,"msg"=>"Tipo de Documento removido com Sucesso!","titlemsg"=>"Tipo de Envio"));

?>