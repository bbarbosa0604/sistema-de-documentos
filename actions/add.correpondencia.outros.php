<?php

require_once("../inc/configuration.php");

try{

	$correspondencia = new Correspondencia();
	$correspondencia->setidcorrespondenciaoutros(post("idcorrespondenciaoutros"));
	$correspondencia->setidcorrespondencia(post("idcorrespondencia"));
	$correspondencia->setidcampo(post("idcampo"));
	$correspondencia->setdesvalue(post("desvalue"));
	$correspondencia->saveOutros();

}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Usuarios"));

}
echo json_encode(
	array(
		"success"=>true,
		"msg"=>"Campo inserido com sucesso!",
		"titlemsg"=>"Usuarios"
	));

?>