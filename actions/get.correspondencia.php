<?php

require_once("../inc/configuration.php");

$correspondencia = new Correspondencia();

if($_GET["idstatus"]){
	$result = $correspondencia->correspondencia_by_status(get("idstatus"), get("dt"));
} else {
	$result = $correspondencia->correspondencia_all_get(get("idcorrespondencia"));
}

echo json_encode($result);

?>