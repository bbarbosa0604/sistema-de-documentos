<?php
require_once("../inc/configuration.php");

$PATH = PATH;

$typefile_allow = array("application/pdf","image/jpeg","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/msword","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","text/plain","image/png","image/gif");

if(!in_array($_FILES[0]['type'], $typefile_allow,true)){

    echo json_encode(array(
        "success"=>false,
        "msg"=>"Tipo de arquivo invalido"
    ));

    exit;
}

$name_file = $_FILES[0]["name"];

function display_filesize($filesize){
    
    if(is_numeric($filesize)){
    $decr = 1024; $step = 0;
    $prefix = array('Byte','KB','MB','GB','TB','PB');
        
    while(($filesize / $decr) > 0.9){
        $filesize = $filesize / $decr;
        $step++;
    } 
    return round($filesize,2).' '.$prefix[$step];
    } else {

    return 'NaN';
    }
    
}

// Caminho onde os arquivos seráo salvos
$idcorrespondencia = (int)post('0');
$path = "/res/arquivos/$idcorrespondencia/".date("Y/m/d")."/";


foreach(explode("/", $path) as $dir){
    $PATH .= $dir . "/";
    if(!is_dir($PATH)){
        mkdir($PATH);
        chdir($PATH);
    }
}

if ($_FILES[0]["error"][0] > 0){

    echo json_encode(array("success"=>false,"msg"=>"Error: ". $_FILES[0]["error"]));
    exit;
}


if(file_exists($PATH.$name_file)){
    $file_extension = pathinfo($_FILES[0]["name"], PATHINFO_EXTENSION);
    $name_file = pathinfo($_FILES[0]["name"], PATHINFO_FILENAME);
    $name_file .="(".time().").".$file_extension;
}

if(move_uploaded_file($_FILES[0]["tmp_name"],$PATH.$name_file)){


    //$arquivo = new Arquivo();
    $arquivo = new Arquivo();

    $arquivo->setidarquivo(0);
    $arquivo->setdesarquivo($name_file);
    $arquivo->setdescaminho($path);
    $arquivo->setnrtamanho($_FILES[0]["size"]);

    $arquivo->save();

    $arquivo->correspondencia_arquivo_save($idcorrespondencia);

echo json_encode(
		array(
			"success"=>true,
            "idarquivo"=>$arquivo->getidarquivo(),
			"idcorrespondencia"=>$idcorrespondencia,
			"msg"=>"Arquivo enviado com Sucesso!",
			"desarquivo"=>$name_file,
			"descaminho"=>$path,
			"type"=>$_FILES[0]["type"],
			"nrtamanho"=>$_FILES[0]["size"],
			"sizeformated"=>display_filesize($_FILES[0]["size"])
			));
}else{

	echo json_encode(array("success"=>false,"msg"=>"Erro ao enviar o arquivo"));
}

?>