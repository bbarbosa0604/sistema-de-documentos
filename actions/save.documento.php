<?php
require_once("../inc/configuration.php");

try{

$documento = new Documento();

$documento->arrayToAttr($_POST);

$documento->save();	

}catch(Exception $e){

	echo json_encode(array("success"=>false,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Documento"));
	exit;
}

echo json_encode(array("success"=>true,"msg"=>"Salvo com Sucesso!","titlemsg"=>"Documento"));

?>