<?php

require_once("../inc/configuration.php");

try{

$documentotipo = new DocumentoTipo();

$documentotipo->setiddocumentotipo(post("iddocumentotipo"));

//Verifica se há alguma restrição para exclusão deste tipo de documento
if($documentotipo->check_constraint($documentotipo->getiddocumentotipo())){
echo json_encode(
	array(
		"success"=>false,
		"msg"=>"Não foi possivel excluir esse Tipo de Documento, porque há uma correspondencia relacionada a ele!
		</br>Se preferir, inative esse Tipo de Documento para que não cadastre nenhum correspondencia."
		)
	);

	exit;

}

$documentotipo->remove();


}catch(Exception $e){

echo json_encode(array("success"=>false,"error"=>$e,"msg"=>$e->getMessage()." Number Error:" . $e->getCode(),"titlemsg"=>"Tipo de Documento"));

}
echo json_encode(array("success"=>true,"msg"=>"Tipo de Documento removido com Sucesso!","titlemsg"=>"Tipo de Documento"));

?>