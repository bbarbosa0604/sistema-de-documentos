<?php 


require_once("inc/configuration.php");

$sql = new Sql();

$obj_correspondencia = new Correspondencia();
$obj_documentotipo = new DocumentoTipo();
$obj_enviotipo = new EnvioTipos();
$obj_correspodenciatipo = new CorrespondenciaTipo();

$correspondencias = $obj_correspondencia->correspondencia_list();
$documentotipos = $obj_documentotipo->documentotipo_list(1,1);
$enviotipos = $obj_enviotipo->enviotipos_list(1);
$correspondenciatipos = $obj_correspodenciatipo->correspondenciatipo_list();

$status = $sql->arrays("call sp_status_list()");

foreach ($correspondencias as $key => $value) {
 	$correspondencias[$key]["dtcadastro"] = date_format(new Datetime($value["dtcadastro"]),"d/m/Y");
}

$campo = new Campo();
$c = array();

$permissao = array();
$permissao["delecao"] = false;
$permissao["edicao"] = false;
$permissao["inclusao"] = false;

foreach ($sql->arrays("select * from tb_permissaoacao where idmenu = 3 and idusuario = ".$_SESSION["idusuario"]) as $value) {
	if($value["idacao"] == 1) $permissao["delecao"] = true;
	if($value["idacao"] == 2) $permissao["edicao"] = true;
	if($value["idacao"] == 3) $permissao["inclusao"] = true;
}


foreach ($campo->getFormulario(2) as $val) {
	$options = "";
	if($val["idcampotipo"] == 5){
		$options = $campo->getOptions($val["idcampo"]);
	}
	array_push($c, array(
		"idcampo"=>$val["idcampo"],
		"idcampotipo"=>$val["idcampotipo"],
		"desnomeexibicao"=>$val["desnomeexibicao"],
		"options"=>$options,
	));
}

$page = new Page(array(
	"header"=>array(
		"title"=>"Correspondencias",
		"subtitle"=>"visualize todos os correspondencias",
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
	),
	"correspondencias"=>$correspondencias,
	"documentotipos"=>$documentotipos,
	"enviotipos"=>$enviotipos,
	"correspondenciatipos"=>$correspondenciatipos,
	"status"=>$status,
	"campos"=>$c,
	"p"=>$permissao
));

$page->setTpl("correspondencias");
?>