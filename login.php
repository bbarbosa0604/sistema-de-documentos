<?php 

require_once("inc/configuration.php");

if((int)Configuration::getCookieLoginRemenber() > 0){
	$to = (get("url"))?get("url"):Configuration::URL_HOME;
	header("Location: ".$to);
	exit;
}

$page = new Page(array(
	"header"=>(array(
		"head"=>true
	)),
	"layout"=>array(
		"sidebar"=>false,
		"topbar"=>false,
		"footer"=>false,
    	"wrapper"=>false
	),
	"footer"=>(array("foot"=>true)),
	"body"=>array(
		"class"=>"login"
	)
));

$page->setTpl("login");

?>