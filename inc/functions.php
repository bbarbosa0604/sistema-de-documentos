<?php

if((float)PHP_VERSION < 5.3) require_once("functions-5-2.php");
if(!isset($GLOBALS["MODULE"])){
	function __autoload($class){

		// if(Configuration::getModuleRealPath("inc/class/".strtolower($class).".php")!==false){

		// 	require_once(Configuration::getModuleRealPath("inc/class/".strtolower($class).".php"));

		// }else{

			$path = __DIR__."/class/";
		    if(file_exists($path.strtolower($class).".php")){
		    	require_once($path.strtolower($class).".php");
		    }elseif(file_exists($path."objects/".strtolower($class).".php")){
		    	require_once($path."objects/".strtolower($class).".php");
		    }

		//}

	}
}
function removeSimplesQuotes($val){
	return str_replace("'", "", $val);
}
function request($key){
	return removeSimplesQuotes($_REQUEST[$key]);
}
function r($key){
	return request($key);
}
function post($key){
	return removeSimplesQuotes($_POST[$key]);
}

function postFull(){

	foreach ($_POST as $key => $value) {
		 
	}
	return $p;
}

function p($key){
	return post($key);
}
function get($key){
	return removeSimplesQuotes($_GET[$key]);
}
function g($key){
	return get($key);
}
function in($t, $arrays = true, $keyEncode=''){
	if(is_array($t)){
		if($arrays){
			$b = array();
			foreach($t as $i){
				$n = array();
				foreach($i as $k=>$v){
					$n[chgName($k)] = $v;
				}
				$n['_'.chgName($keyEncode)] = in($i[$keyEncode]);
				array_push($b, $n);
			}
			return $b;
		}else{
			$n = array();
			foreach($t as $k=>$v){
				$n[chgName($k)] = $v;
			}
			$n['_'.chgName($keyEncode)] = in($t[$keyEncode]);
			return $n;
		}
	}else{
		$encode = base64_encode(time());
		return base64_encode(str_pad(strlen($encode), 3, '0', STR_PAD_LEFT).$encode.base64_encode($t));
	}
}
function out($t){
	$t = base64_decode($t);	
	$len = substr($t, 0, 3);
	return requestFIT(base64_decode(substr($t, ($len+3), strlen($t)-($len+3))));
}
function pre(){
	echo "<pre>";
	foreach(func_get_args() as $arg){
		print_r($arg);
	}
	echo "</pre>";
}
function arrayToBase64($data = array()){

	return base64_encode(json_encode($data));

}
function errorRedirect($errstr){

	$backtrace = debug_backtrace();

	if(getContentType()==="application/json"){
		echo json_encode(array(
			"success"=>false,
			"error"=>$errstr
		));
	}elseif(!headers_sent()){

		header("Location: ".PATH_URL."/usererror.php?".arrayToBase64(array(
			"errstr"=>$errstr,
			"errline"=>$backtrace[0]["line"],
			"errfile"=>$backtrace[0]["file"],
			"backtrace"=>$backtrace
		)));

	}else{

		pre(array(
			"errstr"=>$errstr,
			"errline"=>$backtrace[0]["line"],
			"errfile"=>$backtrace[0]["file"],
			"backtrace"=>$backtrace
		));

	}
	
	exit;

}
function getContentType(){

	foreach(headers_list() as $header){
		if (substr(strtolower($header),0,13) == "content-type:"){
			$data = explode(";", trim(substr($header, 14), 2));
			$contentType = (count($data)>0)?$data[0]:'';
			$charset = (count($data)>1)?$data[1]:'';
			if (strtolower(trim($charset)) != "charset=utf-8"){
				return $contentType;
			}
		}
	}

	return NULL;

}
/*function getProjeto(){

	if(isset($_SESSION[Configuration::SESSION_KEY_PROJETO])){

		$projeto = new Projeto();
		foreach ($_SESSION[Configuration::SESSION_KEY_PROJETO] as $key => $value) {
			$projeto->{"set".$key}($value);
		}
		return $projeto;

	}else{

		if(getContentType()==="application/json"){
			echo json_encode(array(
				"success"=>false,
				"error"=>"Selecione um projeto."
			));
		}elseif(!headers_sent()){
			header("Location: ".PATH_URL."/projects.php");
		}else{

			pre(array(
				"error"=>'Está tela necessita de um projeto selecionado. <a href="'.PATH_URL.'/projects.php">Clique aqui para selecionar um projeto.</a>',
				"trace"=>debug_backtrace()
			));

		}
		exit;

	}	

}*/
function getUsuario(){

	if(isset($_SESSION[SESSION_KEY_LOGIN])){

		return new Usuario($_SESSION[SESSION_KEY_LOGIN]);

	}else{

		errorRedirect("Nenhum usuário autenticado.");

	}	

}
/*function getPessoa(){

	if(isset($_SESSION["idpessoa"])){

		$pessoa = new Pessoa();
		foreach ($_SESSION["pessoa"] as $key => $value) {
			$pessoa->{"set".$key}($value);
		}
		return $pessoa;

	}else{

		errorRedirect("Nenhuma pessoa autenticada.");

	}	

}*/
function getUrlPage($params = array()){

	$pqs = explode("&", $_SERVER['QUERY_STRING']);

	foreach ($pqs as &$value) {
		$value = explode("=", $value);
	}

	$qs = array();

	foreach ($pqs as $p) {
		$qs[$p[0]] = $p[1];
	}

	foreach ($params as $key => $value) {
		$exists = false;
		foreach ($qs as $pkey => &$pvalue) {
			if($key === $pkey){
				$pvalue = $value;
				$exists = true;
			}
		}
		if(!$exists) $qs[$key] = $value;
	}

	$params = array();

	foreach ($qs as $key=>$value) {
		array_push($params, $key."=".$value);
	}

	return PATH_URL.Configuration::getPathSelf()."?".implode("&", $params);

}
?>