<?php 

set_error_handler('errorHandler');

function errorHandler( $errno, $errstr, $errfile, $errline, $errcontext = array()) {

	$backtrace = debug_backtrace();
	
	$trace = array(); 
	$ignore = 0;
    foreach (debug_backtrace() as $k => $v) { 
        if ($k < $ignore) { 
            continue; 
        }

        array_push($trace, '#' . ($k - $ignore) . '<br>' . $v['file'] . '<br>Linha: ' . $v['line'] . '<br>' . (isset($v['class']) ? $v['class'] . '->' : '') . $v['function'] . '(' . implode(', ', $v['args']) . ')'); 
    }

	$error = array(
		"errorno"=>$errno,
		"errstr"=>$errstr,
		"errfile"=>$errfile,
		"errline"=>$errline,
		//"errcontext"=>(gettype($errcontext)==="array")?implode(", ", $errcontext):$errcontext,
		"function"=>__FUNCTION__,
		"backtrace"=>$trace
	);

	switch(getContentType()){

		case "application/json":
		echo json_encode(array(
			"success"=>false,
			"error"=>$error
		));
		exit;
		break;

		case "text/html":
		$page = new Page(array(
			"data"=>array(
				"layout"=>array(
					"sidebar"=>false,
					"topbar"=>false,
					"bgcolorwhite"=>true
				),
				"error"=>$error
			)
		));
		$page->setTpl("error");
		exit;
		break;

	}

}

//register_shutdown_function( "fatal_handler" );

/*function fatal_handler() {
  $errfile = "unknown file";
  $errstr  = "shutdown";
  $errno   = E_CORE_ERROR;
  $errline = 0;

  $error = error_get_last();

  if( $error !== NULL) {

    $errno   = $error["type"];
    $errfile = $error["file"];
    $errline = $error["line"];
    $errstr  = $error["message"];

    

    errorHandler( $errno, $errstr, $errfile, $errline);

  }
}*/

?>