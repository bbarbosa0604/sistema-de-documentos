<?php 

class String {

	private $language_default = "pt-br";
	private $strings = array();
	private $cache = array();

	public function setLanguage($value){

		return $this->language_default = $value;

	}

	public function getLanguage(){

		return $this->language_default;

	}

	public function loadString(){

		if($this->strings) return $this->strings;

		$file_string = PATH."/res/strings/".$this->language_default.".xml";

		$strings = array();

		if(file_exists($file_string)){

			$xml = simplexml_load_file($file_string);

			foreach($xml->children() as $string){

				$val = $string->attributes();
            	$strings[(string)$val[0]] = (string)$string;
//				$strings[(string)$string->attributes()[0]] = (string)$string;
				

			}

		}

		return $this->strings = $strings;

	}

	public function getString($name){

		if(isset($this->cache[$name])) return $this->cache[$name];

		$strings = $this->loadString();

		return $this->cache[$name] = $strings[$name];

	}

}

?>