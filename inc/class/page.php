<?php
class Page {
  	
	private $Tpl = NULL;

	public $options = array(
		"data"=>array(
			"js"=>"",
			"header"=>array(
				"title"=>"",
				"description"=>"",
				"author"=>"",
				"subtitle"=>"",
				"head"=>false,
				"head-title"=>false,
				"load_menu"=>false,
				"breadcrumb"=>array(
					
				)
			),
			"layout"=>array(
				"sidebar"=>false,
				"topbar"=>false,
				"footer"=>true,
				"bgcolorwhite"=>false,
				"wrapper"=>true
			),
			"body"=>array(),
			"footer"=>array(
				"foot"=>false
			)
		)
	);
 
	public function getStringClass(){

		return (isset($this->String))?$this->String:new String();

	}

	public function __construct($options = array()){

		//header("content-type: text/html");

		$rootdir = PATH;

		raintpl::configure("base_url", $rootdir );
		raintpl::configure("tpl_dir", $rootdir."/res/tpl/" );
		raintpl::configure("cache_dir", $rootdir."/res/tpl/tmp/" );
		raintpl::configure("path_replace", false );

		$this->options['data'] = Configuration::array_merge_recursive_distinct($this->options['data'], $options);


		$string = $this->getStringClass();

		if(isset($_SESSION["lang"])) $string->setlanguage($_SESSION["lang"]);
 	
		$this->options['data']['string'] = $string->loadString();

		if(isset($_SESSION)) $this->options['data']['session'] = $_SESSION;
		if(isset($_SERVER)) $this->options['data']['server'] = $_SERVER;

		if(Configuration::isLogged() && $this->options['data']['header']['load_menu']){
			/*$this->options['data']['menu_data'] = base64_encode(json_encode(Configuration::loadUserMenu()));*/
			$this->options['data']['menu_data'] = Menu::listMenus($_SESSION['idusuario']);
		}


		$this->options['data']['path'] = PATH_URL;

		$tpl = $this->getTpl();

		if(gettype($this->options['data'])=='array'){
			foreach($this->options['data'] as $key=>$val){
				$tpl->assign($key, $val);
			}
		}


		if($this->options["data"]["js"])$this->js();


		if($this->options["data"]["header"]["head"]) $tpl->draw("header", false);
		if($this->options["data"]["header"]["head-title"]) $tpl->draw("header-title", false);

		if(Configuration::isPageModule()){

			$PATH_SELF_MODULE = substr($_SERVER["PHP_SELF"], strlen(PATH_URL."/modules/"), strlen($_SERVER["PHP_SELF"]));

			$dirs = explode("/", $PATH_SELF_MODULE);
			$module = array_shift($dirs);

			$tplname = "../../modules/".$module."/res/tpl/".str_replace(".php", "", implode("/", $dirs));


			$this->setTpl($tplname);

		}
 
	}

	public function loadString($lang){

		if($this->strings) return $this->strings;

		$string = $this->getStringClass();

		return $string->loadString();

	}

	public function getString($name){

		$string = $this->getStringClass();
		return $string->getString($name);

	}

	public function js(){

		$script = new Tag('script');
		$script->src = $_SERVER["DOCUMENT_ROOT"].PATH_URL."/res/js/".str_replace('.php','.js?_='.time(),basename($_SERVER['PHP_SELF']));

		return $script;
	}	
 
	public function __destruct(){
 
		$tpl = $this->getTpl();
 
		if(gettype($this->options['data'])=='array'){
			foreach($this->options['data'] as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
 
		if($this->options["data"]["footer"]["foot"]) $tpl->draw("footer", false);
 
	}
 
	public function setTpl($tplname, $data = array(), $returnHTML = false){
 
		$tpl = $this->getTpl();
 
		if(gettype($data)=='array'){
			foreach($data as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
 
		return $tpl->draw($tplname, $returnHTML);
 
	}
  
	public function getTpl(){
 
		return ($this->Tpl)?$this->Tpl:$this->Tpl = new RainTPL;
 
	}
 
}
?>