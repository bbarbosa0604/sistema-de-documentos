<?php
/*
 * @AUTHOR João Rangel
 * joaohcrangel@gmail.com
 *
 */
abstract class Collection extends Model {
  
	protected $itens = array();
	protected $type = "object";
	protected $class = "stdClass";

	//* Exemplo */
	//protected $saveQuery = "EXECUTE sp_model_save ?, ?";
	//protected $saveArgs = array("idmodel","desmodel");	

	/** Insere um novo item na coleção : Mixed */
	public function add($object){

		if(gettype($object) === "object" && get_class($object) === $this->class){

			array_push($this->itens, $object);

			return $object;

		}elseif(gettype($object) === $this->type){

			array_push($this->itens, $object);

			return $object;

		}else{

			throw new Exception("A coleção não aceito o tipo ".gettype($object).".");

		}

	}

	private function pkcompare($item1, $item2){

		$equals = true;

		foreach ($this->pk as $pk) {
			
			if($item1->{'get'.$pk}() !== $item2->{'get'.$pk}()){
				$equals = false;
				break;
			}

		}

		return $equals;

	}

	/** Remove um item da coleção e o retorna : boolean/Mixed */
	public function remove(){

		$object = func_get_arg(0);

		if(!isset($object) || $object == NULL) throw new Exception("Informe o objeto que será removido");

		$itens = array();
		$removed = false;		

		if($this->type === "object"){
			foreach ($this->itens as $item) {
				if(gettype($this->pk) === "string" && $item->{'get'.$this->pk}() === $object->{'get'.$this->pk}()){
					$removed = $item;
				}elseif(gettype($this->pk) === "array" && $this->pkcompare($item, $object)){
					$removed = $item;
				}else{
					array_push($itens, $item);
				}
			}
		}else{
			foreach ($this->itens as $item) {
				if($item === $object){
					$removed = $item;
				}else{
					array_push($itens, $item);
				}
			}
		}

		$this->itens = $itens;

		return $removed;

	}

	public function &getItens(){

		return $this->itens;

	}

	public function setItens($itens){

		if(!gettype($itens) === "array") throw new Exception("Uma coleção só pode definir itens à partir de um Array");

		$this->itens = array();

		foreach ($itens as $item) {
			$this->add($item);
		}

		return $this->getItens();

	}

	public function getSize(){

		return count($this->itens);

	}

	public function getSizeTotal(){

		return (isset($this->fields->sizetotal))?$this->fields->sizetotal:$this->getSize();

	}

	/** Converte a coleção em Array : array */
	public function getFields(){

		$fields = array();

		foreach ($this->itens as $item) {
			if(gettype($item) === "object"){
				array_push($fields, $item->getFields());
			}else{
				array_push($fields, $item);
			}
		}

		return $fields;

	}

	/** Retorna os itens alterados : array */
	public function &getChangedItens(){

		$itens = array();

		foreach ($this->itens as $object) {
			if($object->getChanged()) array_push($itens, $object);
		}

		return $itens;

	}
	
	/** Salva todos os modelos alterados da coleção : boolean */
	public function save(){

		if(!isset($this->saveQuery)) throw new Exception("A coleção não possui o atributo saveQuery");
		if(!isset($this->saveArgs)) throw new Exception("A coleção não possui o atributo saveArgs");

		$itens = &$this->getChangedItens();

		$querys = array();
		$params = array();

		if(count($itens) === 0) return true;

		foreach ($itens as $item) {
			
			if(method_exists($item, "beforesave")) $item->beforesave();

			array_push($querys, $this->saveQuery);

			$args = array();
			foreach ($this->saveArgs as $arg) {
				array_push($args, $item->{"get".$arg}());
			}

			unset($arg);

			array_push($params, $args);

		}

		unset($args);

		$results = $this->getSql()->querys($querys, $params);
		
		$success = true;

		$savedItens = array();
		for ($i = 0; $i < count($itens); $i++) {
			
			$item = &$itens[$i];
			$result = $results[$i];

			if(gettype($this->pk) === "string" && isset($result[$this->pk])){

				$item->{'set'.$this->pk}((int)$result[$this->pk]);
				if(method_exists($item, "aftersave")) $item->aftersave();
				$item->setSaved();

			}elseif(gettype($this->pk) === "array" && count($result) >= count($this->pk)){

				foreach ($this->pk as $pk) {
					$item->{'set'.$pk}((int)$result[$pk]);
				}

				if(method_exists($item, "aftersave")) $item->aftersave();
				$item->setSaved();

			}else{

				$success = false;

			}

			array_push($savedItens, $item);

		}

		$newItens = array();

		foreach ($this->itens as $item) {
			
			$newItem = NULL;

			foreach ($savedItens as $nitem) {
					
				if(gettype($this->pk) === "array" && $this->pkcompare($item, $nitem)){

					$newItem = $nitem;
					break;

				}elseif((int)$item->{"get".$this->pk}() === (int)$nitem->{"get".$this->pk}()){

					$newItem = $nitem;
					break;

				}

			}

			if($newItem !== NULL){
				array_push($newItens, $newItem);
			}else{
				array_push($newItens, $item);
			}

		}

		$this->setItens($newItens);

		unset($result, $results, $querys);

		return $success;

	}

	private function rowsToItens($rows){

		pre($rows);
		exit;

		if(gettype($rows) === "array" && count($rows)){

	    	foreach ($rows as $row) {
	    		if($this->type === "object"){
	    			pre(new $this->class($row));
	    			$this->add(new $this->class($row));	
	    		}else{
					$this->add($row);
	    		}	    		
	    	}

	    	return $this->getItens();

		}else{

			return false;

		}

    }

    public function getFirst(){

    	if($this->getSize() > 0){

    		return $this->itens[0];

    	}else{

    		return NULL;

    	}

    }

    public function getLast(){

    	if($this->getSize() > 0){

    		return $this->itens[($this->getSize()-1)];

    	}else{

    		return NULL;

    	}

    }
	
}
?>