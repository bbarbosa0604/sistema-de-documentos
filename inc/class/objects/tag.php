<?php

class Tag{
    private $__tagName__;
    public $__value__;
    private $__attr__ = array();
    private $__autoCloseTags__ = array('area', 'base', 'br', 'col', 'frame', 'hr', 'img', 'input', 'link', 'meta', 'param');
    
    public function __construct($name){
        $this->__tagName__ = strtolower($name);
    }
    public function __toString(){
        return $this->__toString();     
    }
    public function toString(){
        $html = "<".$this->__tagName__;
        foreach($this->__attr__ as $name=>$value){
            if($name=='value' && !in_array($this->__tagName__, $this->__autoCloseTags__)){
                $this->__value__ = $value;
            }else{
                $html.=' '.strtolower($name).'="'.$value.'"';
            }
        }
        if(in_array($this->__tagName__, $this->__autoCloseTags__)){
            $html.=' />';
        }else{
            $html.='>'.$this->__value__.'</'.$this->__tagName__.'>';    
        }
        return $html;
    }
    public function __set($name, $value){
        $this->__attr__[$name] = $value;
    }
    public function setValue($value){
        return $this->__value__ = $value;
    }
}
?>