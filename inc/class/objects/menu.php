<?php

class Menu extends Model {

    const ROOT = 0;
    public $required = array("desmenu", "desicone", "nrordem");
    protected $pk = "idmenu";

    public static function getRootMenu(){

        return new Menu(array(
            "idmenu"=>Menu::ROOT
        ));

    }

/*    public static function getAdmsMenu(){

        return new Menu(array(
            "idmenu"=>Menu::ADMS
        ));

    }*/

    public function getdeshandler(){

        return (isset($this->fields->deshandler))?$this->fields->deshandler:"javascript:void(0)";

    }

    public function setdeshandler($value){

        if(strlen(trim($value)) === 0){

            return $this->fields->deshandler = "javascript:void(0)";

        }else{

            return $this->fields->deshandler = $value;

        }

    }

    public function get($idmenu){
    
        $this->queryToAttr("CALL sp_menu_get(".$idmenu.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidmenu($this->queryGetID("CALL sp_menu_save(?, ?, ?, ?, ?, ?, ?);", array(
                $this->getidmenu(),
                $this->getidmenupai(),
                $this->getdesmenu(),
                $this->getdesicone(),
                $this->getnrordem(),
                $this->getdeshandler()
            )));

            return $this->getidmenu();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_menu_remove(".$this->getidmenu().")");

        return true;
        
    }

    public function getAllSubMenus($idusuario = NULL){
        
        return Menu::getMenus($this->getidmenu(), $idusuario, true);   
        
    }
    
    public function getSubMenus($idusuario = NULL){
        
        return Menu::getMenus($this->getidmenu(), $idusuario);
        
    }
    
/*    public static function getModuleMenus(Projeto $projeto, $idusuario, $loadSubMenus = false){

        if(!$projeto->getidprojeto() > 0) throw new Exception("O projeto não possui a chave primária.");

        $sql = new Sql();

        $data = $sql->arrays("CALL sp_menuprojetomodulos_list(".Menu::ADMS.", ".$projeto->getidprojeto().", ".$idusuario.")");

        $menus = new Menus();
        
        foreach($data as $row){

            $row['deshref'] = (isset($row['deshandler']) && strlen(trim($row['deshandler'])) > 0 && $row['deshandler']!=="javascript:void(0)")?PATH_URL."/".$row['deshandler']:"javascript:void(0)";

            $m = new Menu($row);
            
            if ($loadSubMenus === true && $m->getnrsubmenu() > 0) {
                
                $m->setsubmenus(Menu::getModuleMenus($projeto, $idusuario, $loadSubMenus));
                
            }

            $menus->add($m);
            
        }
        
        return $menus;

    }*/

/*    public function list(){
        $sql = new Sql();

        return $sql->arrays("CALL sp_menus_list();");

    }*/

    public static function listMenus($i = 0){
        $sql = new Sql();

        if ($i == 0){

            return $sql->arrays("CALL sp_menus_list();");
        }else{
            return $sql->arrays("CALL sp_permissao_menus_usuario_list($i);");
        }
    }


   public function permissao_menu_save($idpermissaomenu,$idmenu,$idusuario,$instatus){
        //$sql = new Sql();
        $idpermissaomenu = $this->queryGetID("CALL sp_permissao_menus_save(?, ?, ?, ?);", array(
            $idpermissaomenu,
            $idmenu,
            $idusuario,
            $instatus
        ));

        //pre($idpermissaomenu);

        return $idpermissaomenu;

    }

    public static function getMenus($idmenupai = 0, $idusuario = NULL, $loadSubMenus = false){
        
        $sql = new Sql();


        if($idusuario === NULL){
            
            $data = $sql->arrays("CALL sp_menu_list(".$idmenupai.")");
            
        }else{

            $data = $sql->arrays("CALL sp_menuusuario_list(".$idmenupai.", ".$idusuario.")");
            
        }
        
        $menus = new Menus();
        
        foreach($data as $row){
            
            $row['deshref'] = (isset($row['deshandler']) && strlen(trim($row['deshandler'])) > 0 && $row['deshandler']!=="javascript:void(0)")?PATH_URL."/".$row['deshandler']:"javascript:void(0)";

            $m = new Menu($row);


/*
            if ($loadSubMenus === true && $m->getnrsubmenu() > 0){

                if(isset($_SESSION["idprojeto"]) && $_SESSION["idprojeto"] > 0){
                    $m->setsubmenus(Menu::getModuleMenus(new Projeto(array("idprojeto"=>(int)$_SESSION["idprojeto"])), $idusuario, $loadSubMenus));
                } else {
                    $m->setsubmenus(Menu::getMenus($m->getidmenu(), $idusuario, $loadSubMenus));
                }
                
            }*/

            $menus->add($m);
            
        }
        
        return $menus;
        
    }

    //@overwrite
    public function getFields(){

        $fields = (array)$this->fields;

        unset($fields['conn'], $fields['Sql']);

        if(isset($fields['submenus'])){

            switch(gettype($fields['submenus'])){

                case "array":
                foreach ($fields['submenus'] as &$menu) {
                    if(gettype($menu) === "object" && in_array(get_class($menu), array("Menu", "Menus"))) {
                        $menu = $menu->getFields();
                    }
                }
                break;
                case "object":
                $menus = $fields['submenus']->getItens();
                foreach ($menus as &$menu) {
                    $menu = $menu->getFields();
                }
                $fields['submenus'] = $menus;
                break;

            }           

        }

        return $fields;

    }

}

?>