<?php

class Projetos extends Collection {

    protected $class = "Projetos";
    protected $saveQuery = "CALL sp_projeto_save(?, ?, ?);";
    protected $saveArgs = array("idprojeto", "desprojeto", "desurl");
    protected $pk = "idprojeto";

    public function get($idprojetos){}
    public function save(){}
    public function remove(){}

    public function getByPessoa(Pessoa $p){

        if(!$p->getidpessoa()) throw new Exception("O objeto Pessoa não possue valor na chave primária");

    	$data = $this->getSql()->arrays("CALL sp_projetospessoas_list(".$p->getidpessoa().")");

    	foreach ($data as $row) {
    		$this->add(new Projeto($row));
    	}

    	return $this->getItens();

    }

    public static function listAll(){

        $sql = new Sql();

        $projetos = new Projetos();

        foreach ($sql->arrays("CALL sp_projeto_list()") as $row) {
            $projetos->add(new Projeto($row));
        }

        return $projetos;

    }

}

?>