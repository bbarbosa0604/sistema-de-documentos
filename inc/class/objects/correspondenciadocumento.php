<?php

class CorrespondenciaDocumento extends Model {

    public $required = array("idcorrespondencia", "iddocumentotipo", "desassunto");
    protected $pk = "idcorrespondenciadocumento";

    public function get($idcorrespondenciadocumento){
    
        return $this->queryToAttr("CALL sp_correspondenciadocumento_get(".$idcorrespondenciadocumento.");");
        
    }


    public function idcorrespondencia_get($idcorrespondencia){
    
        return $this->queryToAttr("CALL sp_correspondenciadocumento_idcorrespondencia_get(".$idcorrespondencia.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidcorrespondenciadocumento($this->queryGetID("CALL sp_correspondenciadocumento_save(?, ?, ?, ?, ?);", array(
                $this->getidcorrespondenciadocumento(),
                $this->getidcorrespondencia(),
                $this->getiddocumentotipo(),
                $this->getdesassunto(),
                $this->getdtdocumento()
            )));

            return $this->getidcorrespondenciadocumento();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_correspondenciadocumento_remove(".$this->getidcorrespondenciadocumento().")");

        return true;
        
    }

    public function correspondenciadocumento_list(){
        $sql = new Sql();
        return $sql->arrays("CALL sp_correspondenciadocumento_list();");

    }


}

?>