<?php

class CorrespondenciaTipo extends Model {

    public $required = array("idcorrespondenciatipo", "descorrespondenciatipo");
    protected $pk = "idcorrespondenciatipo";

    public function get($idcorrespondenciatipo){
    
        return $this->queryToAttr("CALL sp_correspondenciatipo_get(".$idccorrespondenciatipo.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidcorrespondenciatipo($this->queryGetID("CALL sp_correspondenciatipo_save(?, ?, ?, ?);", array(
                $this->getidcorrespondenciatipo(),
                $this->getdescorrespondenciatipo()
            )));

            return $this->getidcorrespondenciatipo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_correspondenciatipo_remove(".$this->getidcorrespondenciatipo().")");

        return true;
        
    }

    public function correspondenciatipo_list(){
        $sql = new Sql();
        return $sql->arrays("CALL sp_correspondenciatipo_list();");

    }


}

?>