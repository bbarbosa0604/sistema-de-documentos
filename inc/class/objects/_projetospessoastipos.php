<?php

class ProjetosPessoasTipos extends Collection {

    protected $class = "ProjetoPessoaTipo";
    protected $saveQuery = "CALL sp_projetospessoastipo_save(?, ?);";
    protected $saveArgs = array("idprojetopessoatipo", "desprojetopessoatipo");
    protected $pk = "idprojetopessoatipo";

    public function get($idprojetopessoatipo){}
    public function save(){}
    public function remove(){}

    public static function listAll(){

    	$sql = new Sql();

        $ppt = new ProjetosPessoasTipos();

        foreach ($sql->arrays("CALL sp_projetopessoatipoadmin_list()") as $row) {
            $ppt->add(new ProjetoPessoaTipo($row));
        }

        return $ppt;

    }

}

?>