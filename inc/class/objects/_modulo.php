<?php

class Modulo extends Model {

    const MANIFEST = "manifest.json";

    const STATUS_MANIFEST_NOT_EXIST = 1001;
    const STATUS_MANIFEST_INVALID_NAME = 1002;
    const STATUS_MANIFEST_INVALID_VERSION = 1003;
    const STATUS_MANIFEST_INVALID_URLS = 1004;
    const STATUS_MANIFEST_INVALID_MENU = 1005;
    const STATUS_MANIFEST_PARSE_ERROR = 1006;

    public static $PATH;
    public $newversion = false;
    public $installed = false;

    public $required = array("desmodulo", "nrversao", "desversao");
    protected $pk = "idmodulo";

    public function __construct(){

        $args = func_get_args();

        if(count($args) === 1 && gettype($args[0]) === "string"){

            $this->setdesmodulo($args[0]);
            $this->getWithModule();

        }else{

            call_user_func_array(array($this, 'parent::__construct'), $args);

        }

    }

    public function setnrversao($value){

        if($this->fields->nrversao < $value){
            $this->setnrversaoantiga($this->fields->nrversao);
        }

        return $this->fields->nrversao = $value;

    }

    public function setdesversao($value){

        if(is_numeric($value) && is_numeric($this->fields->desversao)){
            if((float)$this->fields->desversao < (float)$value){
                $this->setdesversaoantiga($this->fields->desversao);
            }    
        }else{
            if($this->fields->desversao !== $value){
                $this->setdesversaoantiga($this->fields->desversao);
            }
        }

        return $this->fields->desversao = $value;

    }

    public function setstatus($id){

        $this->fields->status = $id;

        switch($id){

            case Modulo::STATUS_MANIFEST_NOT_EXIST:
            $this->setdesstatus("Manifest não encontrado.");
            break;

            case Modulo::STATUS_MANIFEST_INVALID_NAME:
            $this->setdesstatus("O nome do módulo não é válido.");
            break;

            case Modulo::STATUS_MANIFEST_INVALID_VERSION:
            $this->setdesstatus("A versão deve ser do tipo inteiro.");
            break;

            case Modulo::STATUS_MANIFEST_INVALID_URLS:
            $this->setdesstatus("As URLs de permissão devem estar um Array");
            break;

            case Modulo::STATUS_MANIFEST_INVALID_MENU:
            $this->setdesstatus("Os Menus devem estar um Array");
            break;

            case Modulo::STATUS_MANIFEST_PARSE_ERROR:
            $this->setdesstatus("O arquivo Manifest não é um JSON válido.");
            break;

        }

        return $this->fields->status;

    }

    public function setNewVersion(){

        return $this->newversion = true;

    }

    public function isNewVersion(){

        return $this->newversion;

    }

    public function setInstalled(){

        return $this->installed = true;

    }

    public function setUpdated(){

        $this->setInstalled();

        $this->getSql()->query("CALL sp_moduloclear_remove(".$this->getidmodulo().")");

    }

    public function isInstalled(){

        return $this->installed;

    }

    public function get($idmodulo){
    
        $this->queryToAttr("CALL sp_modulo_get(".$idmodulo.");");
        
    }

    public function getWithModule(){
        
        if(!$this->getdesmodulo()) throw new Exception("Informe o nome único do módulo");        

        $this->queryToAttr("CALL sp_modulobydesmodulo_get(?);", array(
            $this->getdesmodulo()
        ));

        $this->setSaved();
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidmodulo($this->queryGetID("CALL sp_modulo_save(?, ?, ?, ?, ?, ?);", array(
                $this->getidmodulo(),
                $this->getdesmodulo(),
                $this->getdesdescricao(),
                $this->getnrversao(),
                $this->getdesversao(),
                (int)$this->getinstatus()
            )));

            return $this->getidmodulo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_modulo_remove(".$this->getidmodulo().")");

        return true;
        
    }

    private function getPathManifest(){

        return Modulo::$PATH.$this->getdesmodulo()."/".Modulo::MANIFEST;

    }

    public static function getManifestData($path_manifest){

        if(file_exists($path_manifest)){

            return json_decode(file_get_contents($path_manifest), true);

        }else{

            return false;

        }

    }

    public function loadManifest(){

        if(!$this->getdesmodulo()) throw new Exception("É necessário informar o campo desmodulo para carregar o manifest.");

        $manifest = Modulo::getManifestData($this->getPathManifest());

        if($manifest === false){

            $this->setstatus(Modulo::STATUS_MANIFEST_NOT_EXIST);

        }elseif($manifest === NULL){

            $this->setstatus(Modulo::STATUS_MANIFEST_PARSE_ERROR);

        }else{

            foreach ($manifest as $key => $value) {
                
                switch($key){

                    case "module":
                    if($this->getdesmodulo() !== $value){
                        $this->setstatus(Modulo::STATUS_MANIFEST_INVALID_NAME);
                    }
                    break;

                    case "name":
                    $this->setdesdescricao($value);
                    break;

                    case "author":
                    $this->setdesautor($value);
                    break;

                    case "version_code":
                    if(gettype($value) === "integer"){
                        $this->setnrversao($value);
                    }else{
                        $this->setstatus(Modulo::STATUS_MANIFEST_INVALID_VERSION);
                    }
                    break;

                    case "version_name":
                    $this->setdesversao($value);
                    break;

                    case "urls":
                    if(gettype($value) === "array"){
                        $this->seturls($value);
                    }else{
                        $this->setstatus(Modulo::STATUS_MANIFEST_INVALID_URLS);
                    }
                    break;

                    case "menu":
                    if(gettype($value) === "array"){
                        $this->setmenu($value);
                    }else{
                        $this->setstatus(Modulo::STATUS_MANIFEST_INVALID_MENU);
                    }
                    break;

                }

            }

        }

    }

    public function getFields(){

        $this->setisinstalado($this->isInstalled());
        $this->setisatualizacao($this->isNewVersion());

        $fields = (array)$this->fields;

        unset($fields['conn'], $fields['Sql']);

        return $fields;

    }

}

Modulo::$PATH = PATH . '/modules/';

?>