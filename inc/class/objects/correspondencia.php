<?php

class Correspondencia extends Model {

    public $required = array("idcorrespondencia");
    protected $pk = "idcorrespondencia";

    public function get($idcorrespondencia){

        return $this->queryToAttr("CALL sp_correspondencia_get(".$idcorrespondencia.");");
        
    }


    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidcorrespondencia($this->queryGetID("CALL sp_correspondencia_save(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", array(
                $this->getidcorrespondencia(),
                $this->getidusuario(),
                $this->getidpessoaremetente(),
                $this->getidpessoadestinatario(),
                $this->getidenviotipo(),
                $this->getidcorrespondenciatipo(),
                $this->getdtenviado(),
                $this->getdtrecebido(),
                $this->getidstatus(),
                $this->getdesobservacao()
                )));

            return $this->getidcorrespondencia();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_correspondencia_remove(".$this->getidcorrespondencia().")");

        return true;
        
    }

    public function correspondencia_list(){

        $sql = new Sql();
        
        return $sql->arrays("CALL sp_correspondencia_list();");

    }

    public function correspondencias_situacao_count(){

        $sql = new Sql();
        return $sql->arrays("CALL sp_correspondencias_situacao_count();");

    }

    public function correspondencias_situacao_date_get($dt_search){

        $sql = new Sql();
        return $sql->arrays("CALL sp_correspondencias_situacao_date_get('$dt_search');");
        //pre("CALL sp_correspondencias_situacao_date_get($date_search);");

    }

    public function correspondencia_all_get($idcorrespondencia){
        //return $sql->arrays("CALL sp_correspodencia_all_get($idcorrespondencia);");
        return $this->queryToAttr("CALL sp_correspodencia_all_get($idcorrespondencia);");
    }

    public function correspondencia_by_status($idstatus, $dt){
        //return $sql->arrays("CALL sp_correspodencia_all_get($idcorrespondencia);");
        return $this->queryToAttr("CALL sp_correspondencia_by_status($idstatus, '$dt');");
    }

    public function saveOutros(){
        $sql = new Sql();

        if($this->getidcorrespondenciaoutros() == 0){
            $sql->query("Insert tb_correspondencias_campos(idcorrespondencia, idcampo, desvalue) values(?, ?, ?);", array(
                $this->getidcorrespondencia(),
                $this->getidcampo(),
                $this->getdesvalue()
            ));
        } else {
            $sql->query("update tb_correspondencias_campos set desvalue = ? where idcorrespondenciaoutros = ?", array(
                $this->getdesvalue(),
                $this->getidcorrespondenciaoutros()
            ));
        }


    }

    public function getOutros(){

        $sql = new Sql();
        return $sql->arrays("select * from tb_correspondencias_campos inner join tb_campos b on a.idcampo = b.idcampo where idcorrespondencia = ".$this->getidcorrespondencia());

    }



}

?>