<?php

class Usuarios extends Collection {

    protected $class = "Usuario";
    protected $saveQuery = "CALL sp_usuario_save(?, ?, ?, ?, ?);";
    protected $saveArgs = array("idusuario", "idpessoa", "deslogin", "dessenha", "instatus");
    protected $pk = "idusuario";

    public function get($idusuario){}

    public static function getUsuarios(Projeto $projeto, $skip = 0, $limit = 25){

        if(!$projeto->getidprojeto() > 0) throw new Exception("O projeto não possui a chave primária.");

        $sql = new Sql();

        $data = $sql->arrays("CALL sp_usuariosprojeto_list(".$projeto->getidprojeto().", ".$skip.", ".$limit.")");
        $total = $sql->select("CALL sp_usuariosprojetototal_list(".$projeto->getidprojeto().")");

        $usuarios = new Usuarios();

        foreach ($data as $row) {
        	$row['desdtcadastro'] = date("d/m/Y H:i", strtotime($row['dtcadastro']));
            $usuarios->add(new Usuario($row));
        }

        $usuarios->setsizetotal((int)$total['nrtotal']);

        return $usuarios;

    }

    public static function getUsuariosPesquisar(Projeto $projeto, $despequisar, $skip = 0, $limit = 25){

        if(!$projeto->getidprojeto() > 0) throw new Exception("O projeto não possui a chave primária.");

        $sql = new Sql();

        $data = $sql->arrays("CALL sp_usuariosprojetopesquisa_list(".$projeto->getidprojeto().", '".utf8_decode($despequisar)."', ".$skip.", ".$limit.")");
        $total = $sql->select("CALL sp_usuariosprojetopesquisatotal_list(".$projeto->getidprojeto().", '".utf8_decode($despequisar)."')");

        $usuarios = new Usuarios();

        foreach ($data as $row) {
            $row['desdtcadastro'] = date("d/m/Y H:i", strtotime($row['dtcadastro']));
            $usuarios->add(new Usuario($row));
        }

        $usuarios->setsizetotal((int)$total['nrtotal']);

        return $usuarios;

    }

    

}

?>