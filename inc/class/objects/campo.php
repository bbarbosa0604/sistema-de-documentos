<?php

class Campo extends Model {

    public $required = array("idcampo");
    protected $pk = "idcampo";

    public function get($idcampo){

        return $this->queryToAttr("select * from tb_campos where instatus = 1 and idcampo = ".$idcampo);
        
    }

    public function getFormulario($idformulario){

        $sql = new Sql();
        return $sql->arrays("select * from tb_campos where instatus = 1 and idformulario = ".$idformulario);
        
    }

    public function getOptions($idcampo){

        $sql = new Sql();
        return $sql->arrays("select * from tb_camposselect where instatus = 1 and idcampo = ".$idcampo);
        
    }

    public function save(){ 
        
        $sql = new Sql();
        
        if(!$this->getidcampo()){

            $sql->query("Insert tb_campos(desnomeexibicao, idcampotipo, idformulario) values(?, ?, ?);", array(
                $this->getdesnomeexibicao(),
                $this->getidcampotipo(),
                $this->getidformulario()
            ));


            $this->setidcampo($sql->id());

        } else {
            $sql->query("Update tb_campos set desnomeexibicao = ?, idcampotipo = ? where idcampo = ?;", array(
                $this->getdesnomeexibicao(),
                $this->getidcampotipo(),
                $this->getidcampo()
            ));
        }
    }

    public function saveSelectOption($idcamposelect, $value){
        $sql = new Sql();

        if($idcamposelect==0){
            $sql->query("Insert tb_camposselect(idcampo, desvalue) values(?, ?);", array(
                $this->getidcampo(),
                $value
            ));
        } else {
            $sql->query("Update tb_camposselect set desvalue = ? where idcamposelect = ?;", array(
                $value,
                $idcamposelect
            ));
        }
    }

    public function remove(){ 
        $sql = new Sql();
        $sql->query("Update tb_campos set instatus = 0 where idcampo = ?;", array(
            $this->getidcampo()
        ));
    }

    public function removeOption(){ 
        $sql = new Sql();
        $sql->query("Update tb_camposselect set instatus = 0 where idcamposelect = ?;", array(
            $this->getidcamposelect()
        ));
    }

    public function load(){
        return $this->queryToAttr("select * from tb_campos where instatus = 1");
    }

    public function loadTipos(){
        return $this->queryToAttr("select * from tb_campotipos where instatus = 1");
    }

}

?>