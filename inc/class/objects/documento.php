<?php

class Documento extends Model {

    public $required = array("iddocumento", "idpessoa", "desdocumento","iddocumento");
    protected $pk = "iddocumento";

    public function get($iddocumento){
    
        return $this->queryToAttr("CALL sp_documento_get(".$iddocumento.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setiddocumento($this->queryGetID("CALL sp_documento_save(?, ?, ?, ?);", array(
                $this->getiddocumento(),
                $this->getidpessoa(),
                $this->getdesdocumento(),
                $this->getiddocumentotipo()
            )));

            return $this->getiddocumento();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_documento_remove(".$this->getiddocumento().")");

        return true;
        
    }

    public function documento_list(){

        return $this->queryToAttr("CALL sp_documento_list();");

    }


    public function documento_idpessoa_get($idpessoa){
        $sql = new Sql();
        return $sql->arrays("CALL sp_documento_idpessoa_get(".$idpessoa.");");

    }

    public function documento_pessoa_remove($idpessoa){
        
        $this->execute("CALL sp_documento_idpessoa_remove(".$idpessoa.");");

        return true;

    }


}

?>