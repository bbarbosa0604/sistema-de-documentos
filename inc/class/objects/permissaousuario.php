<?php

class PermissaoUsuario extends Model {

    public $required = array();
    protected $pk = array("idpermissao", "idusuario");

    public function get($idpermissao, $idusuario){
    
        $this->queryToAttr("CALL sp_permissoesusuario_get(".$idpermissao.", ".$idusuario.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpermissao($this->queryGetID("CALL sp_permissoesusuario_save(?, ?);", array(
                $this->getidpermissao(),
                $this->getidusuario()
            )));

            return array($this->getidpermissao(), $this->getidusuario());

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_permissoesusuario_remove(".$this->getidpermissao().", ".$this->getidusuario().")");

        return true;
        
    }

}

?>