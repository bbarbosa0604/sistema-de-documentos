<?php

class UsuarioAcesso extends Model {

    public $required = array("idusuario");
    protected $pk = "idusuarioacesso";

    public function get($idusuarioacesso){
    
        $this->queryToAttr("CALL sp_usuariosacesso_get(".$idusuarioacesso.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidusuarioacesso($this->queryGetID("CALL sp_usuariosacesso_save(?, ?);", array(
                $this->getidusuarioacesso(),
                $this->getidusuario()
            )));

            return $this->getidusuarioacesso();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_usuariosacesso_remove(".$this->getidusuarioacesso().")");

        return true;
        
    }

}

?>