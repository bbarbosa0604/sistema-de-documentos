<?php

class TecnologiaTipo extends Model {

    public $required = array("destecnologiatipo");
    protected $pk = "idtecnologiatipo";

    public function get($idtecnologiatipo){
    
        $this->queryToAttr("CALL sp_tecnologiastipo_get(".$idtecnologiatipo.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidtecnologiatipo($this->queryGetID("CALL sp_tecnologiastipo_save(?, ?);", array(
                $this->getidtecnologiatipo(),
                $this->getdestecnologiatipo()
            )));

            return $this->getidtecnologiatipo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_tecnologiastipo_remove(".$this->getidtecnologiatipo().")");

        return true;
        
    }

}

?>