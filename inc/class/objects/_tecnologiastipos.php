<?php

class TecnologiasTipos extends Collection {

    protected $class = "TecnologiaTipo";
    protected $saveQuery = "CALL sp_tecnologiastipo_save(?, ?);";
    protected $saveArgs = array("idtecnologiatipo", "destecnologiatipo");
    protected $pk = "idtecnologiatipo";

    public function get($idtecnologiatipo){}

    public static function listAll(){

    	$sql = new Sql();

        $tecnologiastipos = new TecnologiasTipos();

        foreach ($sql->arrays("CALL sp_tecnologiastipos_list()") as $row) {
            $tecnologiastipos->add(new TecnologiaTipo($row));
        }

        return $tecnologiastipos;

    }

}

?>
