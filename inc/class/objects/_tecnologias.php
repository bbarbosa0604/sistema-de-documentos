<?php

class Tecnologias extends Collection {

    protected $class = "Tecnologia";
    protected $saveQuery = "CALL sp_tecnologia_save(?, ?, ?);";
    protected $saveArgs = array("idtecnologia", "idtecnologiatipo", "destecnologia");
    protected $pk = "idtecnologia";

    public function get($idtecnologia){}

    public function getByProjeto(Projeto $p){

    	if(!$p->getidprojeto()) throw new Exception("O projeto não possui chave primária");

    	foreach($this->getSql()->arrays("CALL sp_tecnologiasprojeto_list(".$p->getidprojeto().")") as $row){

    		$this->add(new Tecnologia($row));

    	}

    	return $this->getItens();

    }

    public static function listAll(){

        $sql = new Sql();

        $tecnologias = new Tecnologias();

        foreach ($sql->arrays("CALL sp_tecnologias_list()") as $row) {
            $tecnologias->add(new Tecnologia($row));
        }

        return $tecnologias;

    }

}

?>