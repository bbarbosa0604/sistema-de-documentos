<?php

class Projeto extends Model {

    public $required = array("desprojeto");
    protected $pk = "idprojeto";

    public function __construct(){

        $args = func_get_args();

        switch(count($args)){

            case 1:

            switch(gettype($args[0])){

                case "array":

                if(isset($args[0]["idprojeto"]) && isset($args[0]["idpessoa"])){

                    return $this->getIfAccess($args[0]["idprojeto"], $args[0]["idpessoa"]);

                }

                break;

            }
            
            break;

        }

        call_user_func_array(array($this, 'parent::__construct'), $args);

    }

    public function get($idprojeto){
    
        $this->queryToAttr("CALL sp_projeto_get(".$idprojeto.");");
        
    }

    public function getIfAccess($idprojeto, $idpessoa){
    
        $this->queryToAttr("CALL sp_projetoifaccess_get(".$idprojeto.", ".$idpessoa.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidprojeto($this->queryGetID("CALL sp_projeto_save(?, ?, ?);", array(
                $this->getidprojeto(),
                $this->getdesprojeto(),
                $this->getdesurl()
            )));

            return $this->getidprojeto();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_projeto_remove(".$this->getidprojeto().")");

        return true;
        
    }

    public function addModulo(Modulo &$m){

        if(!$this->getidprojeto()) throw new Exception("O projeto não possui chave primária");
        if(!$m->getidmodulo()) throw new Exception("O módulo não possui chave primária");

        $this->getSql()->query("CALL sp_projetomodulo_save(?, ?)", array(
            $this->getidprojeto(),
            $m->getidmodulo()
        ));

        $m->setidprojeto($this->getidprojeto());

        return true;

    }

    public function removeModulo(Modulo &$m){

        if(!$this->getidprojeto()) throw new Exception("O projeto não possui chave primária");
        if(!$m->getidmodulo()) throw new Exception("O módulo não possui chave primária");

        $this->getSql()->query("CALL sp_projetomodulo_remove(?, ?)", array(
            $this->getidprojeto(),
            $m->getidmodulo()
        ));

        return true;

    }

    public function getPessoas(){

        if(!$this->getidprojeto()) throw new Exception("O projeto não possui chave primária");

        return new Pessoas($this);

    }

    public function getModulos(){

        if(!$this->getidprojeto()) throw new Exception("O projeto não possui chave primária");

        return new Modulos(new Projeto(array("idprojeto"=>$this->getidprojeto())));

    }

    public function getTecnologias(){

        if(!$this->getidprojeto()) throw new Exception("O projeto não possui chave primária");

        return new Tecnologias(new Projeto(array("idprojeto"=>$this->getidprojeto())));
        
    }

    public function addTecnologias(Tecnologias $tecnologias){

        $ids = array();

        foreach ($tecnologias->getItens() as $tecnologia) {
            array_push($ids, $tecnologia->getidtecnologia());
        }

        $this->getSql()->query("CALL sp_projetotecnologia_save(".$this->getidprojeto().", '".implode(",",$ids)."')");

        return true;

    }

    public function addPessoa(Pessoa $p, ProjetoPessoaTipo $ppt){

        if(!$p->getidpessoa()) throw new Exception("A pessoa não possui chave primária");
        if(!$this->getidprojeto()) throw new Exception("O projeto não possui chave primária");
        if(!$ppt->getidprojetopessoatipo()) throw new Exception("A pessoa não possui tipo de usuário");

        $this->execute("CALL sp_projetopessoa_save(?, ?, ?)", array(
            $this->getidprojeto(),
            $p->getidpessoa(),
            $ppt->getidprojetopessoatipo()
        ));

        return true;

    }

}

?>