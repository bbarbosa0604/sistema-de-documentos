<?php

class PessoaJuridica extends Model {

    public $required = array("idpessoajuridica","idpessoa");
    protected $pk = "idpessoajuridica";

    public function get($idpessoajuridica){
    
        return $this->queryToAttr("CALL sp_pessoajuridica_get(".$idpessoajuridica.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpessoajuridica($this->queryGetID("CALL sp_pessoajuridica_save(?, ?, ?);", array(
                $this->getidpessoajuridica(),
                $this->getidpessoa(),   
                $this->getdesramoatividade()
            )));

            return $this->getidpessoajuridica();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_pessoajuridica_remove(".$this->getidpessoa().")");

        return true;
        
    }


}

?>