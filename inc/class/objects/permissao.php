<?php

class Permissao extends Model {

    const DEVELOPER = 2;

    public $required = array("despermissao");
    protected $pk = "idpermissao";

    public function get($idpermissao){
    
        $this->queryToAttr("CALL sp_permissoe_get(".$idpermissao.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpermissao($this->queryGetID("CALL sp_permissoe_save(?, ?);", array(
                $this->getidpermissao(),
                $this->getdespermissao()
            )));

            return $this->getidpermissao();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_permissoe_remove(".$this->getidpermissao().")");

        return true;
        
    }

}

?>