<?php

class Endereco extends Model {

    public $required = array("idpessoa");
    protected $pk = "idendereco";

    public function get($idendereco){
    
        return $this->queryToAttr("CALL sp_endereco_get($idendereco);");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidendereco($this->queryGetID("CALL sp_endereco_save(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", array(
                $this->getidendereco(),
                $this->getidpessoa(),
                $this->getdescep(),
                $this->getdesendereco(),
                $this->getdesnumero(),
                $this->getdescomplemento(),
                $this->getdesbairro(),
                $this->getdescidade(),
                $this->getidestado(),
                $this->getdesreferencia(),
                $this->getinprincipal()
            )));

            return $this->getidendereco();

        }else{

            return false;

        }
        
    }


    public function remove(){

        $this->execute("CALL sp_endereco_remove(".$this->getidendereco().")");

        return true;
        
    }

    public function endereco_idpessoa_get($idpessoa){
         $sql = new Sql();         
         return $sql->arrays("CALL sp_endereco_idpessoa_get($idpessoa)");
    }

    public function endereco_pessoa_remove($idpessoa){
        
        $this->execute("CALL sp_endereco_idpessoa_remove($idpessoa);");

        return true;

    }

}

?>