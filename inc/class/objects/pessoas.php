<?php

class Pessoas extends Collection {

    protected $class = "Pessoa";
    protected $saveQuery = "CALL sp_pessoa_save(?, ?, ?, ?, ?);";
    protected $saveArgs = array("idpessoa", "idpessoapai", "idpessoatipo", "despessoa", "instatus");
    protected $pk = "idpessoa";

    public function get($idpessoa){}

    public function getByProjeto(Projeto $p){

    	if(!$p->getidprojeto()) throw new Exception("O projeto não possui chave primária");

    	foreach($this->getSql()->arrays("CALL sp_pessoasprojeto_list(".$p->getidprojeto().")") as $row){

    		$this->add(new Pessoa($row));

    	}

    	return $this->getItens();

    }

}

?>