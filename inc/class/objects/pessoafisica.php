<?php

class PessoaFisica extends Model {

    public $required = array("idpessoafisica","idpessoa");
    protected $pk = "idpessoafisica";

    public function get($idpessoafisica){
    
        return $this->queryToAttr("CALL sp_pessoafisica_get(".$idpessoafisica.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpessoafisica($this->queryGetID("CALL sp_pessoafisica_save(?, ?, ?, ?);", array(
                $this->getidpessoafisica(),
                $this->getidpessoa(),
                $this->getdtnascimento(),
                $this->getdessexo()
            )));

            return $this->getidpessoafisica();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_pessoafisica_remove(".$this->getidpessoa().")");

        return true;
        
    }


}

?>