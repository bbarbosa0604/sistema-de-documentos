<?php

class Usuario extends Model {

    public $required = array("idusuario","desnome", "deslogin", "dessenha");
    protected $pk = "idusuario";

    public function get($idusuario){
    
        return $this->queryToAttr("CALL sp_usuario_get($idusuario);");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidusuario($this->queryGetID("CALL sp_usuario_save(?, ?, ?, ?, ?, ?)", array(
                $this->getidusuario(),
                $this->getdesnome(),
                $this->getdeslogin(),
                $this->getdessenha(),
                $this->getinstatus(),
                $this->getidpessoa()
            )));

            return $this->getidusuario();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_usuario_remove(".$this->getidusuario().")");

        return true;
        
    }

    public static function login($deslogin, $dessenha){
        
        $sql = new Sql();

        $data = $sql->select("CALL sp_usuariologin_get('".utf8_decode($deslogin)."', '".utf8_decode($dessenha)."');");

        if(isset($data["idusuario"]) && $data["idusuario"] > 0){

            $usuario = new Usuario($data);

            return $usuario;

        }else{

            return false;

        }

    }

    public function usuario_list($instatus = 0){
        
        $sql = new Sql();

        return $sql->arrays("CALL sp_usuario_list($instatus);");
    }

    public function check_constraint($idusuario){

        $isconstraints = $this->queryToAttr("CALL sp_usuarios_check_constraints($idusuario)");

        return $isconstraints['isconstraints']?true:false;

    }

}

?>