<?php

class PermissaoUrl extends Model {

    public $required = array();
    protected $pk = array("idpermissao", "idurl");

    public function get($idpermissao, $idurl){
    
        $this->queryToAttr("CALL sp_permissoesurl_get(".$idpermissao.", ".$idurl.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpermissao($this->queryGetID("CALL sp_permissoesurl_save(?, ?);", array(
                $this->getidpermissao(),
                $this->getidurl()
            )));

            return array($this->getidpermissao(), $this->getidurl());

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_permissoesurl_remove(".$this->getidpermissao().", ".$this->getidurl().")");

        return true;
        
    }

}

?>