<?php

class PermissaoMenu extends Model {

    public $required = array();
    protected $pk = array("idpermissao", "idmenu");

    public function get($idpermissao, $idmenu){
    
        $this->queryToAttr("CALL sp_permissoesmenu_get(".$idpermissao.", ".$idmenu.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpermissao($this->queryGetID("CALL sp_permissoesmenu_save(?, ?);", array(
                $this->getidpermissao(),
                $this->getidmenu()
            )));

            return array($this->getidpermissao(), $this->getidmenu());

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_permissoesmenu_remove(".$this->getidpermissao().", ".$this->getidmenu().")");

        return true;
        
    }

}

?>