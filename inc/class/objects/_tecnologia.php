<?php

class Tecnologia extends Model {

    public $required = array("idtecnologiatipo", "destecnologia");
    protected $pk = "idtecnologia";

    public function get($idtecnologia){
    
        $this->queryToAttr("CALL sp_tecnologia_get(".$idtecnologia.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidtecnologia($this->queryGetID("CALL sp_tecnologia_save(?, ?, ?);", array(
                $this->getidtecnologia(),
                $this->getidtecnologiatipo(),
                $this->getdestecnologia()
            )));

            return $this->getidtecnologia();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_tecnologia_remove(".$this->getidtecnologia().")");

        return true;
        
    }

}

?>