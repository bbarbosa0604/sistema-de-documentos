<?php

class Pessoa extends Model {

    public $required = array("despessoa");
    protected $pk = "idpessoa";

    public function get($idpessoa){
    
        return $this->queryToAttr("CALL sp_pessoa_get(".$idpessoa.");");        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidpessoa($this->queryGetID("CALL sp_pessoa_save(?, ?, ?, ?);", array(
                $this->getidpessoa(),
                $this->getdespessoa(),
                $this->getidpessoatipo(),
                $this->getinstatus()
            )));

            return $this->getidpessoa();

        }else{

            return false;

        }
        
    }

    public function getByNome($despessoa){
        return $this->getSql()->arrays("call sp_pessoa_getByDespessoa ('" . $despessoa . "')");
    }

    public function remove(){

        $this->execute("CALL sp_pessoa_remove(".$this->getidpessoa().")");

        return true;
        
    }

    public function pessoa_list($idpessoatipo = 'NULL'){
        $sql = new Sql();
         return $sql->arrays("CALL sp_pessoa_list($idpessoatipo)");

    }


    public function pessoa_fisica_juridica_get($idpessoa){

        return $this->queryToAttr("CALL sp_pessoa_fisica_juridica_get(".$idpessoa.")");
    }

    public function check_constraint($idpessoa){

        $isconstraints = $this->queryToAttr("CALL sp_pessoas_check_constraints(".$idpessoa.")");

        return $isconstraints['isconstraints']?true:false;

    }

    public function get_pessoa_vinculada($idpessoa){
        $sql = new Sql();
        return $sql->arrays("call sp_pessoa_vinculada_idpessoa_get($idpessoa)");
    }

    public function remove_pessoa_vinculada($idpessoa,$idpessoapai){
        $sql = new Sql();
        return $sql->arrays("call sp_pessoa_vinculada_remove($idpessoa,$idpessoapai)");
    }

    public function edit_pessoa_vinculada($idpessoa,$idpessoapai){
        $sql = new Sql();
        return $sql->arrays("call sp_pessoa_vinculada_edit($idpessoa,$idpessoapai)");
    }


    public function pessoa_empresa_edit($idpessoa,$idpessoapai){
        $sql = new Sql();
        return $sql->arrays("CALL sp_pessoa_empresa_edit($idpessoa,$idpessoapai)");
    }

    public function pessoa_empresa_remove($idpessoa,$idpessoapai){
        $sql = new Sql();
        return $sql->arrays("CALL sp_pessoa_empresa_remove($idpessoa,$idpessoapai)");
        
    }

    public function saveOutros(){
        $sql = new Sql();

        if($this->getidpessoaoutros() == 0){
            $sql->query("Insert tb_pessoas_campos(idpessoa, idcampo, desvalue) values(?, ?, ?);", array(
                $this->getidpessoa(),
                $this->getidcampo(),
                $this->getdesvalue()
            ));
        } else {
            $sql->query("update tb_pessoas_campos set desvalue = ? where idpessoaoutros = ?", array(
                $this->getdesvalue(),
                $this->getidpessoaoutros()
            ));
        }


    }

    public function getOutros(){

        $sql = new Sql();
        return $sql->arrays("select * from tb_pessoas_campos a inner join tb_campos b on a.idcampo = b.idcampo where idpessoa = ".$this->getidpessoa());

    }
}

?>