<?php

class ContatoTipo extends Model {

    public $required = array("descontatotipo");
    protected $pk = "idcontatotipo";

    public function get($idcontatotipo){
    
        return $this->queryToAttr("CALL sp_contatostipo_get(".$idcontatotipo.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidcontatotipo($this->queryGetID("CALL sp_contatostipo_save(?, ?);", array(
                $this->getidcontatotipo(),
                $this->getdescontatotipo()
            )));

            return $this->getidcontatotipo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_contatostipo_remove(".$this->getidcontatotipo().")");

        return true;
        
    }

    public function contatotipos_list(){
        $sql = new Sql();
        return $sql->arrays("Call sp_contatotipos_list()");
    }

}

?>