<?php

class Url extends Model {

    public $required = array("desurl");
    protected $pk = "idurl";

    public function get($idurl){
    
        $this->queryToAttr("CALL sp_url_get(".$idurl.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidurl($this->queryGetID("CALL sp_url_save(?, ?, ?);", array(
                $this->getidurl(),
                $this->getdesurl(),
                $this->getidmodulo()
            )));

            return $this->getidurl();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_url_remove(".$this->getidurl().")");

        return true;
        
    }

}

?>