<?php

class DocumentoTipo extends Model {

    public $required = array("iddocumentotipo", "desdocumentotipo");
    protected $pk = "iddocumentotipo";

    public function get($iddocumentotipo){
    
        return $this->queryToAttr("CALL sp_documentotipo_get(".$iddocumentotipo.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setiddocumentotipo($this->queryGetID("CALL sp_documentotipo_save(?, ?, ?, ?);", array(
                $this->getiddocumentotipo(),
                $this->getdesdocumentotipo(),
                $this->getinstatus(),
                $this->getidcategoria()
            )));

            return $this->getiddocumentotipo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_documentotipo_remove(".$this->getiddocumentotipo().")");

        return true;
        
    }

    public function documentotipo_list($idcategoria = 1,$instatus = 0){
        $sql = new Sql();
        return $sql->arrays("CALL sp_documentotipo_idcategora_list($idcategoria,$instatus);");

    }

    public function check_constraint($iddocumentotipo){

        $isconstraints = $this->queryToAttr("CALL sp_documentotipos_check_constraints(".$iddocumentotipo.")");

        return $isconstraints['isconstraints']?true:false;

    }


}

?>