<?php

class Modulos extends Collection {

    protected $class = "Modulo";

    protected $saveQuery = "CALL sp_modulo_save(?, ?, ?, ?, ?, ?);";
    protected $saveArgs = array("idmodulo", "desmodulo", "desdescricao", "nrversao", "desversao", "instatus");
    protected $pk = "idmodulo";

    public function get($idmodulo){}

    public function loadItens(Projeto $projeto, $overwrite = false){

        $desmodulos = array();
        $itens = $this->getItens();

        foreach ($itens as $m) {
            array_push($desmodulos, $m->getdesmodulo());
        }

        if($projeto){

            if(!$projeto->getidprojeto()) throw new Exception("O projeto não possui chave primária");

            $rows = $this->getSql()->arrays("CALL sp_projetomodulosbydesmodulos_list(".$projeto->getidprojeto().", '".implode(",", $desmodulos)."')");

        }else{

            $rows = $this->getSql()->arrays("CALL sp_modulosbydesmodulos_list('".implode(",", $desmodulos)."')");

        }

        foreach ($rows as $row) {

            foreach ($itens as &$m) {

                if($m->getdesmodulo() === $row["desmodulo"]){

                    $m->setInstalled();

                    if($m->getnrversao() > $row["nrversao"]){

                        $m->setNewVersion();
                        $m->setnrversaoantiga($row["nrversao"]);
                        $m->setdesversaoantiga($row["desversao"]);
                        $overwrite = false;

                    }

                    foreach ($row as $key => $value) {

                        if($overwrite === true || $m->{"get".$key}() === NULL){

                            $m->{"set".$key}($value);

                        }

                    }

                    unset($key, $value);

                }

            }

        }

        unset($row);

        return $this->setItens($itens);

    }

    public function getByProjeto(Projeto $p){

    	if(!$p->getidprojeto()) throw new Exception("O objeto Projeto não possue valor na chave primária");

        foreach($this->getSql()->arrays("CALL sp_modulosprojeto_list(".$p->getidprojeto().")") as $row){

            $this->add(new Modulo($row));

        }

        return $this->getItens();

    }

    public static function getAll(){

    	$modulos = new Modulos();

    	$sql = new Sql();
    	foreach ($sql->arrays("CALL sp_modulos_list()") as $row) {
    		$modulos->add(new Modulo($row));
    	}

    	return $modulos;

    }

    public static function getListFromDisk(){

        $modulos = new Modulos();
        foreach (scandir(Modulo::$PATH) as $item) {

            if($item !== "." && $item !== ".." && is_dir(Modulo::$PATH."/".$item)){

                $modulo = new Modulo(array(
                    "desmodulo"=>$item
                ));

                $modulo->loadManifest();

                $modulos->add($modulo);

            }
        }

        return $modulos;

    }

}

?>