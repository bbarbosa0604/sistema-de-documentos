<?php

require_once('PHPMailer/class.phpmailer.php');
require_once('PHPMailer/class.smtp.php');


class Email extends Model {


    /////////////// FASTCODE/////////////////////
    private $email;
    private $from = "contato@fastcode.com.br";
    private $fromName = "Nome do email";
    private $host = 'smtp.gmail.com';
    private $port = 465;
    private $smtpsecure = 'ssl';
    private $reply;
    private $replyName;
    private $pwd = "casa2012";

    /////////////// FASTCODE/////////////////////


/*    private $email;
    private $from = "foslilde@gmail.com";
    private $fromName = "888888888";
    private $host = 'smtp.gmail.com';
    private $port = 587;
    private $smtpsecure = 'tls';
    private $reply;
    private $replyName;
    private $pwd = "QGZvc3RvbiM=";*/
    //private $to;
    //private $subject;
    //private $body;


    //preg_replace("/[\$\s,]/", '', explode(";",$to));

    public function __construct(){

        $this->email = new PHPMailer();

        $this->email->isSMTP();

        $this->email->SMTPDebug = 0;

        //Ask for HTML-friendly debug output
        $this->email->Debugoutput = 'html';

        //Set the hostname of the mail server
        $this->email->Host = $this->host;

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->email->Port = $this->port;

        //Set the encryption system to use - ssl (deprecated) or tls
        $this->email->SMTPSecure = $this->smtpsecure;

        //Whether to use SMTP authentication
        $this->email->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $this->email->Username = $this->email->fromName?$this->fromName:$this->from;

        //Password to use for SMTP authentication
        //$this->email->Password = base64_decode($this->pwd);
        $this->email->Password = $this->pwd;

        //Set who the message is to be sent from
        $this->email->setFrom($this->from,$this->fromName);

        //Set an alternative reply-to address
        $this->email->addReplyTo($this->from,$this->fromName);  
        //Set who the message is to be sent to
/*        foreach ($to as $key => $value) {
            $mail->addAddress($value);
        }*/
        //$mail->addAddress('foslilde@gmail.com');
         //$this->email->addAddress($this->to);

        //Set the subject line
        //$this->email->Subject = $this->subject;

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //$this->email->msgHTML(file_get_contents('contents.html'));
        //$this->email->msgHTML($this->body);

        //Replace the plain text body with one created manually
        $this->email->AltBody = 'This is a plain-text message body';

    }

    public function sendEmail(){

        return $this->email->send();
    }

    public function setPropertiesEmail($options = array()){

        foreach (explode(";",$options['to']) as $key => $value) {
            $this->email->addAddress($value);
        }

        foreach (explode(";",$options['cc']) as $key => $value) {
            $this->email->AddCC($value);
        }

        $this->email->Subject = $options['subject'];
        $this->email->msgHTML($options['body']);

    }

    public function setAttachmentFiles($files){
        foreach ($files as $key => $value) {
            $this->email->addAttachment(PATH.$value);        
        }

    }
    public function save(){}
    public function remove(){}

}

?>