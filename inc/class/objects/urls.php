<?php

class Urls extends Collection {

    protected $class = "Url";

    protected $saveQuery = "CALL sp_url_save(?, ?, ?);";
    protected $saveArgs = array("idurl", "desurl", "idmodulo");
    protected $pk = "idurl";

    public function get($idurl){}

    public function getByUsuario(Usuario $u){

        if(!$this->getidusuario()) throw new Exception("O usuário informado não possui chave primária");

        foreach ($this->getSql()->arrays("CALL sp_urlusuario_list(".$this->getidusuario().")") as $row) {
            
            $this->add(new Url($row));

        }

        return $this->getItens();

    }

}

?>