<?php 

class Arquivo extends Model {

	public $required = array("desarquivo","descaminho","nrtamanho");
  protected $pk = "idarquivo";


  public function get($idarquivo){
    return $this->queryToAttr("CALL sp_arquivo_get(".$idarquivo.");");
  }

  public function save(){
    if($this->getChanged() && $this->isValid()){

            $this->setidarquivo($this->queryGetID("CALL sp_arquivo_save(?, ?, ?, ?);", array(
                $this->getidarquivo(),
                $this->getdesarquivo(),
                $this->getdescaminho(),
                $this->getnrtamanho()
            )));

            return $this->getidarquivo();

        }else{

            return false;

    }
  }

  public function remove(){    
    $this->execute("CALL sp_arquivo_remove(".$this->getidarquivo().")");
    return true;
        
    }

  public function arquivo_correspondencia_list($idcorrespondencia){
    $sql = new Sql();
    return $sql->arrays("CALL sp_arquivo_idcorrespondencia_list($idcorrespondencia);");
  }

  public function correspondencia_arquivo_save($idcorrespondencia){
    
    $this->execute("CALL sp_correspondencia_arquivo_save($idcorrespondencia,".$this->getidarquivo().");");
  }

}

?>