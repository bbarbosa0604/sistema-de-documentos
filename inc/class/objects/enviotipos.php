<?php

class EnvioTipos extends Model {

    public $required = array("idenviotipo");
    protected $pk = "idenviotipo";

    public function get($idenviotipo){
    
        return $this->queryToAttr("CALL sp_enviotipo_get(".$idenviotipo.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidenviotipo($this->queryGetID("CALL sp_enviotipo_save(?, ?, ?);", array(
                $this->getidenviotipo(),
                $this->getdesenviotipo(),
                $this->getinstatus()
            )));

            return $this->getidenviotipo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_enviotipo_remove(".$this->getidenviotipo().")");

        return true;
        
    }

    public function enviotipos_list($instatus = 0){

        $sql = new Sql();
        return $sql->arrays("CALL sp_enviotipo_list($instatus);");

    }

    public function check_constraint($idenviotipo){

        $isconstraints = $this->queryToAttr("CALL sp_enviotipos_check_constraints(".$idenviotipo.")");

        return $isconstraints['isconstraints']?true:false;

    }


}

?>