<?php

class Menus extends Collection {

    protected $class = "Menu";
    protected $saveQuery = "CALL sp_menu_save(?, ?, ?, ?, ?, ?, ?);";
    protected $saveArgs = array("idmenu", "idmenupai", "desmenu", "desicone", "deshandler", "nrordem", "idmodulo");
    protected $pk = "idmenu";

    public function get($idmenus){}
    public function save(){}
    public function remove(){}

    public static function getMenus(Menu $menupai = NULL){

    	if($menupai === NULL){

    		$menupai = Menu::getRootMenu();

    	}

    	if(!is_numeric($menupai->getidmenu())) throw new Exception("Informe a chave primária do Menu");

    	return $menupai->getSubMenus();

    }

}

?>