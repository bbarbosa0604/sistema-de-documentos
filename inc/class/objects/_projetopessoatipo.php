<?php

class ProjetoPessoaTipo extends Model {

    public $required = array("desprojetopessoatipo");
    protected $pk = "idprojetopessoatipo";

    public function get($idprojetopessoatipo){
    
        $this->queryToAttr("CALL sp_projetospessoastipo_get(".$idprojetopessoatipo.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidprojetopessoatipo($this->queryGetID("CALL sp_projetospessoastipo_save(?, ?);", array(
                $this->getidprojetopessoatipo(),
                $this->getdesprojetopessoatipo()
            )));

            return $this->getidprojetopessoatipo();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_projetospessoastipo_remove(".$this->getidprojetopessoatipo().")");

        return true;
        
    }

}

?>