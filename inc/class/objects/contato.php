<?php
class Contato extends Model {

    public $required = array("idcontatotipo", "idpessoa", "descontato");
    protected $pk = "idcontato";

    public function get($idcontato){
    
        return $this->queryToAttr("CALL sp_contato_get(".$idcontato.");");
        
    }

    public function save(){

        if($this->getChanged() && $this->isValid()){

            $this->setidcontato($this->queryGetID("CALL sp_contato_save(?, ?, ?, ?, ?);", array(
                $this->getidcontato(),
                $this->getdescontato(),
                $this->getidpessoa(),
                $this->getidcontatotipo(),
                $this->getinprincipal()
           )));

            return $this->getidcontato();

        }else{

            return false;

        }
        
    }

    public function remove(){

        $this->execute("CALL sp_contato_remove(".$this->getidcontato().")");

        return true;
        
    }

    public function contato_idpessoa_get($idpessoa){
        $sql = new Sql();
        return $sql->arrays("CALL sp_contato_idpessoa_get(".$idpessoa.")");
     
    }
    public function contato_pessoa_remove($idpessoa){
        
        $this->execute("CALL sp_contato_idpessoa_remove(".$idpessoa.");");

        return true;

    }


}

?>