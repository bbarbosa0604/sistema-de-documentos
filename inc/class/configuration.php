<?php 
 class Configuration {
 	
 	const URL_LOGIN = "login.php";
 	const URL_LOGOUT = "logout.php";
 	const URL_LOCK = "lock.php";
 	const URL_HOME = "index.php";
 	const URL_SYSTEM = "home.php";
 	const SESSION_KEY_LOGIN = "idusuario";
 	const SESSION_KEY_LOCK = "lock";

 	private $nologinurls = array(
 		"login.php",
 		"lock.php",
 		"actions/login.php",
 		"actions/lock.php"
 	);

	public function __construct(){

		$this->checkLock();
		$this->checkLogin();

	}

	public static function getCookieLoginRemenber(){

		return (isset($_COOKIE[Configuration::SESSION_KEY_LOGIN]))?$_COOKIE[Configuration::SESSION_KEY_LOGIN]:NULL;

	}

	private function checkLock(){

		if(isset($_SESSION[Configuration::SESSION_KEY_LOCK]) && basename($_SERVER['PHP_SELF']) !== Configuration::URL_LOCK && basename($_SERVER['PHP_SELF']) !== Configuration::URL_LOGOUT){

			$params = ($_SESSION[Configuration::SESSION_KEY_LOCK]['url'])?$_SESSION[Configuration::SESSION_KEY_LOCK]['url']:"";
			if(strlen($params)) $params = "?url=".$params;

			header("Location: ".Configuration::URL_LOCK.$params, TRUE, 307);
			exit;

		}

	}

	public static function isLogged(){

		return (isset($_SESSION[Configuration::SESSION_KEY_LOGIN]) && !isset($_SESSION[Configuration::SESSION_KEY_LOCK]));

	}	

	private function isNoLoginURL(){
		return (in_array(Configuration::getPathSelf(), $this->nologinurls));

	}

	private function checkLogin(){

		if(!$this->isNoLoginURL() && !isset($_SESSION[Configuration::SESSION_KEY_LOGIN]) && Configuration::getCookieLoginRemenber()){

			$usuario = new Usuario((int)Configuration::getCookieLoginRemenber());

			if($usuario->getidusuario() && $usuario->getinstatus()){

				foreach ($usuario->getFields() as $key => $value) {
					$_SESSION[$key] = $value;
				}

			}
		}			
		
		if(!$this->isNoLoginURL() && !isset($_SESSION[Configuration::SESSION_KEY_LOGIN])){
			header("Location: ".Configuration::URL_LOGIN."?url=".$_SERVER["PHP_SELF"], TRUE, 307);
			exit;
		}

	}

	public function logout(){

		setcookie(Configuration::SESSION_KEY_LOGIN, "", time()-(60*60));
		session_unset();
		$params = (get("url"))?get("url"):"";
		if(strlen($params)) $params = "?url=".$params;
		header("Location: ".Configuration::URL_LOGIN.$params, TRUE, 307);
		exit;

	}

	public function lock(){

		if(!isset($_SESSION[Configuration::SESSION_KEY_LOCK])){

			$_SESSION[Configuration::SESSION_KEY_LOCK] = array(
				"time"=>time(),
				"url"=>$_SERVER["URL"]
			);

		}

	}

	public function __desstruct(){

		session_write_close();
		
	}

	public static function array_merge_recursive_distinct ( array &$array1, array &$array2 ){
	  $merged = $array1;

	  foreach ( $array2 as $key => &$value )
	  {
	    if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
	    {
	      $merged [$key] = Configuration::array_merge_recursive_distinct ( $merged [$key], $value );
	    }
	    else
	    {
	      $merged [$key] = $value;
	    }
	  }

	  return $merged;
	}

	public static function loadUserMenu(){

		// if(isset($_SESSION[SESSION_KEY_MENU])) return $_SESSION[SESSION_KEY_MENU];

		// return $_SESSION[SESSION_KEY_MENU] = Menu::getMenus(Menu::ROOT, $_SESSION['idusuario'], true)->getFields();

	}

	public static function loadModulesMenu(){

		/*if(!isset($_SESSION[SESSION_KEY_MENU])) Configuration::loadUserMenu();

		$menus = Menu::getModuleMenus(new Projeto($_SESSION[Configuration::SESSION_KEY_PROJETO]), $_SESSION['idusuario'], true);

		foreach ($_SESSION[SESSION_KEY_MENU] as &$menu) {
			
			if($menu['idmenu'] == Menu::ADMS){

				$menu['submenus'] = $menus->getFields();
				$menu['nrsubmenu'] = $menus->getSize();
				break;

			}

		}

		return $menus;

	*/}	

	public static function reloadPermissions(){

		if(isset($_SESSION[SESSION_KEY_MENU])) unset($_SESSION[SESSION_KEY_MENU]);
		Configuration::loadUserMenu();

	}

	public static function isPageModule(){

		return (strpos($_SERVER["PHP_SELF"], PATH_URL."/modules/") === 0);

	}

	public static function getPathSelf(){
		return substr($_SERVER['PHP_SELF'], strlen(PATH_URL), strlen($_SERVER['PHP_SELF']));

	}

	public static function getModuleRealPath($path){

		/*$PATH_SELF_MODULE = substr($_SERVER["PHP_SELF"], strlen(PATH_URL."/modules/"), strlen($_SERVER["PHP_SELF"]));

		$dirs = explode("/", $PATH_SELF_MODULE);
		$module = array_shift($dirs);

		$filepath = PATH."/modules/".$module."/".$path;

		if(file_exists($filepath)){

			return $filepath;

		}else{

			return false;

		}*/	

	}

	public static function errorToArray(Exception $e){

		return array(
			"success"=>false,
			"error"=>$e->getMessage(),
			"error_line"=>$e->getLine(),
			"error_file"=>$e->getFile(),
			"error_code"=>$e->getCode(),
			"error_trace"=>$e->getTrace()
		);

	}

 } 
 ?>