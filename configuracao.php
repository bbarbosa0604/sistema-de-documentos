<?php 

require_once("inc/configuration.php");

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$sql = new Sql();

$formulario = new Formulario();
$campo = new Campo();

$permissao = array();
$permissao["delecao"] = false;
$permissao["edicao"] = false;
$permissao["inclusao"] = false;

foreach ($sql->arrays("select * from tb_permissaoacao where idmenu = 10 and idusuario = ".$_SESSION["idusuario"]) as $value) {
	if($value["idacao"] == 1) $permissao["delecao"] = true;
	if($value["idacao"] == 2) $permissao["edicao"] = true;
	if($value["idacao"] == 3) $permissao["inclusao"] = true;
}

$page = new Page(array(
	"header"=>array(
		"title"=>"Personalizar Campos nos formularios",
		"subtitle"=>"Lista de todos os formularios permitidos para acrescentar novos campos",
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
		
	),
	"formulario"=>$formulario->load(),
	"campotipos"=>$campo->loadTipos(),
	"p"=>$permissao
));


$page->setTpl("formulario");



?>