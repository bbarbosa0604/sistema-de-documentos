<?php 

require_once("inc/configuration.php");

$obj = new Usuario();

//Lista todos os usuarios ativos e nao ativos
$usuarios = $obj->usuario_list();

$sql = new Sql();

$permissao = array();
$permissao["delecao"] = false;
$permissao["edicao"] = false;
$permissao["inclusao"] = false;

foreach ($sql->arrays("select * from tb_permissaoacao where idmenu = 8 and idusuario = ".$_SESSION["idusuario"]) as $value) {
	if($value["idacao"] == 1) $permissao["delecao"] = true;
	if($value["idacao"] == 2) $permissao["edicao"] = true;
	if($value["idacao"] == 3) $permissao["inclusao"] = true;
}

$page = new Page(array(
	"header"=>array(
		"title"=>"Usuarios",
		"subtitle"=>"todos os usuarios do sistemas",
		"head-title"=>true,
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true

	),"usuarios"=>$usuarios,
	"p"=>$permissao
));

$page->setTpl("usuarios");



?>