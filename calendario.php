<?php 

require_once("inc/configuration.php");

$sql = new Sql();

$obj_documentotipo = new DocumentoTipo();
$obj_enviotipo = new EnvioTipos();
$obj_correspodenciatipo = new CorrespondenciaTipo();

$documentotipos = $obj_documentotipo->documentotipo_list();
$enviotipos = $obj_enviotipo->enviotipos_list();
$correspondenciatipos = $obj_correspodenciatipo->correspondenciatipo_list();

$status = $sql->arrays("call sp_status_list()");

$permissao = array();
$permissao["delecao"] = false;
$permissao["edicao"] = false;
$permissao["inclusao"] = false;

foreach ($sql->arrays("select * from tb_permissaoacao where idmenu = 4 and idusuario = ".$_SESSION["idusuario"]) as $value) {
	if($value["idacao"] == 1) $permissao["delecao"] = true;
	if($value["idacao"] == 2) $permissao["edicao"] = true;
	if($value["idacao"] == 3) $permissao["inclusao"] = true;
}

$page = new Page(array(
	"header"=>array(
		"title"=>"Calendário",
		"subtitle"=>"visualize as correspondencias no periodo",
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
	),
	"documentotipos"=>$documentotipos,
	"enviotipos"=>$enviotipos,
	"correspondenciatipos"=>$correspondenciatipos,
	"status"=>$status,
	"p"=>$permissao
));

$page->setTpl("calendario");
?>