-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- M�quina: root.com.br
-- Data de Cria��o: 28-Jan-2015 �s 07:47
-- Vers�o do servidor: 5.5.40
-- vers�o do PHP: 5.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin5 */;

--
-- Base de Dados: `sistemadedocumentos`
--
CREATE DATABASE IF NOT EXISTS `sistemadedocumentos` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sistemadedocumentos`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`%` PROCEDURE `sp_arquivo_get`(IN `var_idarquivo` INT)
BEGIN
	Select 
        idarquivo, desarquivo, descaminho,
        nrtamanho,dtcadastro
	From tb_arquivos
	where idarquivo = var_idarquivo;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_arquivo_idcorrespondencia_list`(in var_idcorrespondencia int)
BEGIN

    Select 
        a.idarquivo, a.descaminho,a.desarquivo,a.nrtamanho,
        a.dtcadastro,b.idcorrespondencia 
    from tb_arquivos a
    inner join tb_correspondencias_arquivos b
    on a.idarquivo = b.idarquivo
    where b.idcorrespondencia = var_idcorrespondencia;
    
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_arquivo_remove`(IN `var_idarquivo` INT)
    NO SQL
BEGIN
	Delete from tb_arquivos
	Where idarquivo = var_idarquivo;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_arquivo_save`(IN `var_idarquivo` INT, IN `var_desarquivo` VARCHAR(1000), IN `var_descaminho` VARCHAR(1000), IN `var_nrtamanho` INT)
    NO SQL
BEGIN

if var_idarquivo = 0 THEN
    INSERT INTO tb_arquivos(
        	desarquivo,descaminho,nrtamanho
    )values(
        	var_desarquivo,var_descaminho,var_nrtamanho
    );
	
	Select last_insert_id() as idarquivo;
ELSE
    UPDATE tb_arquivos
        SET 
            desarquivo = var_desarquivo,
			descaminho = var_descaminho,
			nrtamanho = var_descaminho
    WHERE idarquivo = var_idarquivo;

	Select var_idarquivo() as idarquivo;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_contatotipos_list`()
BEGIN
    Select idcontatotipo, descontatotipo
    from tb_contatotipos;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_contato_get`(IN `var_idcontato` INT)
    NO SQL
BEGIN
	Select 
		idcontato,descontato,idpessoa,idcontatotipo,
		inprincipal,dtcadastro
	From tb_contatos
	Where idcontato = var_idcontato;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_contato_idpessoa_get`(IN `var_idpessoa` INT)
    NO SQL
BEGIN
	Select 
		a.idcontato,a.descontato,a.idpessoa,a.idcontatotipo,b.descontatotipo,
		a.inprincipal,a.dtcadastro
	From tb_contatos a
  inner join tb_contatotipos b
  on a.idcontatotipo = b.idcontatotipo
	Where a.idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_contato_idpessoa_remove`(in `var_idpessoa` INT)
    NO SQL
BEGIN

Delete from tb_contatos
where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_contato_remove`(IN `var_idcontato` INT)
    NO SQL
BEGIN
	Delete from tb_contatos
	where idcontato = var_idcontato;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_contato_save`(IN `var_idcontato` INT, IN `var_descontato` VARCHAR(200), IN `var_idpessoa` INT, IN `var_idcontatotipo` INT, IN `var_inprincipal` BIT)
    NO SQL
BEGIN

if(var_inprincipal = 1) then
    call sp_pessoa_contato_principal_edit(var_idpessoa,var_idcontatotipo);
end if;

if var_idcontato = 0 THEN
    INSERT INTO tb_contatos(
        	descontato,idpessoa,idcontatotipo,inprincipal
    )values(
        	var_descontato,
        	var_idpessoa,
        	var_idcontatotipo,
         var_inprincipal
    );
    
    Select last_insert_id() as idcontato;
ELSE
    UPDATE tb_contatos
        SET 
       descontato = var_descontato,
			idpessoa = var_idpessoa,
			idcontatotipo = var_idcontatotipo,
			inprincipal = var_inprincipal
    WHERE idcontato = var_idcontato;
    
    Select var_idcontato as idcontato;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspodencia_all_get`(IN `var_idcorrespondencia` INT)
BEGIN
	Select 
        a.idcorrespondencia,h.idcorrespondenciadocumento,b.deslogin,a.idusuario,
        c_remetente.despessoa as despessoa_remetente,a.idpessoaremetente,
        d_destinatario.despessoa as despessoa_destinatario,a.idpessoadestinatario,
        e.desenviotipo,a.idenviotipo,f.descorrespondenciatipo,a.idcorrespondenciatipo,
        a.dtenviado,        
        case 
            when a.dtrecebido = "0000-00-00"
            then NULL
            else a.dtrecebido
        end as dtrecebido,
        
        a.idstatus,g.desstatus,h.desassunto,i.iddocumentotipo,
        i.desdocumentotipo,h.dtdocumento,a.desobservacao,a.dtcadastro
	From sistemadedocumentos.tb_correspondencias a
  left join sistemadedocumentos.tb_usuarios b on 
    a.idusuario = b.idusuario
  left join sistemadedocumentos.tb_pessoas c_remetente on
    c_remetente.idpessoa = a.idpessoaremetente
  left join sistemadedocumentos.tb_pessoas d_destinatario on
    d_destinatario.idpessoa = a.idpessoadestinatario
  inner join sistemadedocumentos.tb_enviotipos e on
    e.idenviotipo = a.idenviotipo
  inner join sistemadedocumentos.tb_correspondenciatipos f on
    f.idcorrespondenciatipo = a.idcorrespondenciatipo
  inner join sistemadedocumentos.tb_status g on
    g.idstatus = a.idstatus
  left join sistemadedocumentos.tb_correspondencias_documentos h on
    h.idcorrespondencia = a.idcorrespondencia
  left join sistemadedocumentos.tb_documentotipos i on 
    i.iddocumentotipo = h.iddocumentotipo
where a.idcorrespondencia = var_idcorrespondencia;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciadocumento_get`(in var_idcorrespondenciadocumento int)
BEGIN
    	Select 
        idcorrespondenciadocumento,idcorrespondencia,
        desassunto,dtdocumento,dtcadastro
	From sistemadedocumentos.tb_correspondencias_documentos
	Where idcorrespondenciadocumento = var_idcorrespondenciadocumento;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciadocumento_idcorrespondencia_get`(in var_idcorrespondencia int)
BEGIN
    	Select 
        idcorrespondenciadocumento,idcorrespondencia,
        desassunto,dtdocumento,dtcadastro
	From sistemadedocumentos.tb_correspondencias_documentos
	Where idcorrespondencia = var_idcorrespondencia;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciadocumento_remove`(IN `var_idcorrespondenciadocumento` INT)
BEGIN

DELETE from tb_correspondencias_documentos 
where idcorrespondenciadocumento = var_idcorrespondenciadocumento;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciadocumento_save`(IN `var_idcorrespondenciadocumento` INT, IN `var_idcorrespondencia` INT,IN `var_iddocumentotipo` INT, IN `var_desassunto` VARCHAR(200), IN `var_dtdocumento` DATE)
    NO SQL
BEGIN

if var_idcorrespondenciadocumento = 0 THEN
    INSERT INTO tb_correspondencias_documentos(
        	idcorrespondencia,iddocumentotipo,
        	desassunto,dtdocumento
    )values(
        	var_idcorrespondencia,
        	var_iddocumentotipo,
        	var_desassunto,
        	var_dtdocumento
    );
    
    Select last_insert_id() as idcorrespondenciadocumento;
ELSE
    UPDATE tb_correspondencias_documentos
        SET 
       idcorrespondencia = var_idcorrespondencia,
			iddocumentotipo = var_iddocumentotipo,
			desassunto = var_desassunto,
			dtdocumento = var_dtdocumento
    WHERE idcorrespondenciadocumento = var_idcorrespondenciadocumento;
    
    Select var_idcorrespondenciadocumento as idcorrespondenciadocumento;
    
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciahistorico_get`(in var_idcorrespondenciahistorico int)
BEGIN
    Select 
        a.idcorrespondenciahistorico, a.descorrespondenciahistorico, 
        a.idcorrespondencia, a.idusuario, a.dtcadastro,b.deslogin
    from tb_correspondenciashistoricos a
    inner join tb_usuarios b
    on a.idusuario = b.idusuario    
    where idcorrespondenciahistorico = var_idcorrespondenciahistorico
    order by a.idcorrespondenciahistorico desc;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciahistorico_idcorrespondencia_list`(in var_idcorrespondencia int)
BEGIN
    Select 
        a.idcorrespondenciahistorico, a.descorrespondenciahistorico, 
        a.idcorrespondencia, a.idusuario, a.dtcadastro,b.deslogin
    from tb_correspondenciashistoricos a
    inner join tb_usuarios b
    on a.idusuario = b.idusuario    
    where idcorrespondencia = var_idcorrespondencia
    order by a.idcorrespondenciahistorico desc;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciashistoricos_list`()
BEGIN
    Select 
        a.idcorrespondenciahistorico, a.descorrespondenciahistorico, 
        a.idcorrespondencia, a.idusuario, a.dtcadastro,b.deslogin
    from tb_correspondenciashistoricos a
    inner join tb_usuarios b
    on a.idusuario = b.idusuario
    order by a.idcorrespondenciahistorico desc;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciashistoricos_save`(
    in var_idcorrespondenciahistorico int,in var_descorrespondenciahistorico varchar(500),
    in var_idcorrespondencia int, in var_idusuario int
)
BEGIN
    if var_idcorrespondenciahistorico = 0 then
        Insert tb_correspondenciashistoricos
        (
            descorrespondenciahistorico, 
            idcorrespondencia,
            idusuario
        )values
        (
            var_descorrespondenciahistorico,
            var_idcorrespondencia,
            var_idusuario
        );
       call sp_correspondenciahistorico_get(LAST_INSERT_ID());

end if;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencias_situacao_count`()
    NO SQL
BEGIN

/*
(Select 
    count(idcorrespondencia) as qtde,date(dtenviado) as data_ocorrencia, 
    1 as idsituacao, "Enviados" as dessituacao 
from sistemadedocumentos.tb_correspondencias
Group by date(dtenviado))
union 

(Select 
    count(idcorrespondencia) as qtde,
    date(dtrecebido) as dtrecebido, 2 as idsituacao, "Recebidos" as dessituacao   
 from sistemadedocumentos.tb_correspondencias
Group by date(dtrecebido))
Union 


(Select count(idcorrespondencia) as qtde, date(dtcadastro) as dtcadastro, 
    3 as idsituacao,"Cadastrados" as dessituacao 
from sistemadedocumentos.tb_correspondencias
group by date(dtcadastro));
*/

Select 
    a.idcorrespondenciatipo as idsituacao, count(b.idcorrespondenciatipo) as qtde,
    a.descorrespondenciatipo as dessituacao,
    date(b.dtcadastro) as data_ocorrencia
from tb_correspondenciatipos a
left join tb_correspondencias b on
a.idcorrespondenciatipo = b.idcorrespondenciatipo
Group by a.idcorrespondenciatipo,a.descorrespondenciatipo,date(b.dtcadastro);

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencias_situacao_date_get`(IN dt_search date)
BEGIN
-- (IN dt_dia INT, in dt_mes int, in dt_ano int)

/*(Select 
    count(idcorrespondencia) as qtde,date(dtenviado) as data_ocorrencia, 
    1 as idsituacao, "Enviados" as dessituacao 
from sistemadedocumentos.tb_correspondencias
where 
    date(dtenviado) = dt_search
    Group by date(dtenviado))
union 

(Select 
    count(idcorrespondencia) as qtde,
    date(dtrecebido) as dtrecebido, 2 as idsituacao, "Recebidos" as dessituacao   
 from sistemadedocumentos.tb_correspondencias
    where 
    date(dtrecebido) = dt_search
    Group by date(dtrecebido))
Union 


(Select count(idcorrespondencia) as qtde, date(dtcadastro) as dtcadastro, 
    3 as idsituacao,"Cadastrados" as dessituacao 
from sistemadedocumentos.tb_correspondencias
    where 
        date(dtcadastro) = dt_search
    Group by date(dtcadastro));
*/

Select 
    a.idcorrespondenciatipo as idsituacao, count(b.idcorrespondenciatipo) as qtde,
    a.descorrespondenciatipo as dessituacao,
    date(b.dtcadastro) as data_ocorrencia
from tb_correspondenciatipos a
left join tb_correspondencias b on
a.idcorrespondenciatipo = b.idcorrespondenciatipo and date(dtcadastro) = dt_search
where 
        date(dtcadastro) = dt_search or date(dtcadastro) is null
Group by a.idcorrespondenciatipo,a.descorrespondenciatipo,date(b.dtcadastro);

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciatipo_list`()
BEGIN
    	Select idcorrespondenciatipo, descorrespondenciatipo 
    from tb_correspondenciatipos;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondenciatipo_save`(in var_idcorrespondenciatipo int, in var_descorrespondenciatipo varchar(50))
BEGIN

if var_idcorrespondenciatipo = 0 THEN
    INSERT INTO tb_correspondenciatipos(
        	descorrespondenciatipo
    )values(
        	var_descorrespondenciatipo
    );
    Select last_insert_id() as idcorrespondenciatipo;
ELSE
    UPDATE tb_correspondenciatipos
        SET 
			descorrespondenciatipo = var_descorrespondenciatipo
    WHERE idcorrespondenciatipo = var_idcorrespondenciatipo;
    
    Select var_idcorrespondenciatipo as idcorrespondenciatipo;
END IF ; 
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencia_arquivo_add`(IN `var_idcorrespondencia` INT, IN `var_idarquivo` INT)
    NO SQL
BEGIN
	Insert tb_correspondencia_arquivos
	(
        idcorrespondencia,idarquivo
    )
	values
	(
    	var_idcorrespondencia,var_idarquivo
    );
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencia_arquivo_save`(
    in var_idcorrespondencia int, in var_idarquivo int
)
BEGIN
    Insert tb_correspondencias_arquivos
    (
        idcorrespondencia,idarquivo
    )
        values
    (
        var_idcorrespondencia,var_idarquivo
    );
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencia_get`(IN `var_idcorrespondencia` INT)
    NO SQL
BEGIN
	Select idcorrespondencia,idusuario,idpessoaremetente,idpessoadestinatario,
	       idenviotipo,idcorrespondenciatipo,dtenviado,
        case 
            when dtrecebido = "1969-12-31"
            then NULL
            else dtrecebido
        end as dtrecebido,
         idstatus,desobservacao,dtcadastro
	From tb_correspondencias
	Where idcorrespondencia = var_idcorrespondencia;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencia_list`()
    NO SQL
BEGIN
	Select 
        a.idcorrespondencia,h.idcorrespondenciadocumento,b.deslogin,a.idusuario,
		c_remetente.despessoa as despessoa_remetente,
        a.idpessoaremetente,d_destinatario.despessoa as despessoa_destinatario,a.idpessoadestinatario,
        e.desenviotipo,a.idenviotipo,f.descorrespondenciatipo,a.idcorrespondenciatipo,
        a.dtenviado,a.dtrecebido,a.idstatus,g.desstatus,h.desassunto,i.desdocumentotipo,h.dtdocumento,
        a.desobservacao,a.dtcadastro
	From sistemadedocumentos.tb_correspondencias a
  left join sistemadedocumentos.tb_usuarios b on 
    a.idusuario = b.idusuario
  left join sistemadedocumentos.tb_pessoas c_remetente on
    c_remetente.idpessoa = a.idpessoaremetente
  left join sistemadedocumentos.tb_pessoas d_destinatario on
    d_destinatario.idpessoa = a.idpessoadestinatario
  inner join sistemadedocumentos.tb_enviotipos e on
    e.idenviotipo = a.idenviotipo
  inner join sistemadedocumentos.tb_correspondenciatipos f on
    f.idcorrespondenciatipo = a.idcorrespondenciatipo
  inner join sistemadedocumentos.tb_status g on
    g.idstatus = a.idstatus
  left join sistemadedocumentos.tb_correspondencias_documentos h on
    h.idcorrespondencia = a.idcorrespondencia
  left join sistemadedocumentos.tb_documentotipos i on 
    i.iddocumentotipo = h.iddocumentotipo;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencia_remove`(IN `var_idcorrespondencia` INT)
    NO SQL
BEGIN
	Delete From tb_correspondencias
	Where idcorrespondencia = var_idcorrespondencia;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_correspondencia_save`(IN `var_idcorrespondencia` INT, IN `var_idusuario` INT, IN `var_idpessoaremetente` INT, IN `var_idpessoadestinatario` INT, IN `var_idenviotipo` INT, IN `var_idcorrespondenciatipo` INT, IN `var_dtenviado` DATE, IN `var_dtrecebido` DATE, IN `var_idstatus` INT, IN `var_desobservacao` VARCHAR(300))
    NO SQL
BEGIN

if var_dtrecebido = '0000-00-00' then
    set var_dtrecebido  = NULL; 
end if; 

if var_idcorrespondencia = 0 THEN
    INSERT INTO tb_correspondencias(
        	idusuario,idpessoaremetente,idpessoadestinatario,
        	idenviotipo,idcorrespondenciatipo,dtenviado,
        	dtrecebido,idstatus,desobservacao
        	
    )values(
        	var_idusuario,var_idpessoaremetente,var_idpessoadestinatario,
        	var_idenviotipo,var_idcorrespondenciatipo,var_dtenviado,
        	var_dtrecebido,var_idstatus,var_desobservacao
    );
    select last_insert_id() as idcorrespondencia;
ELSE
    UPDATE tb_correspondencias
        SET 
       idusuario = var_idusuario,
			idpessoaremetente = var_idpessoaremetente,
			idpessoadestinatario = var_idpessoadestinatario,
			idenviotipo = var_idenviotipo,
			idcorrespondenciatipo = var_idcorrespondenciatipo,
			dtenviado = var_dtenviado,
			dtrecebido = var_dtrecebido,
			desobservacao = var_desobservacao,
       idstatus = var_idstatus
    WHERE idcorrespondencia = var_idcorrespondencia;
	
	Select var_idcorrespondencia as idcorrespondencia;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documentotipos_check_constraints`(IN `var_iddocumentotipo` INT)
    NO SQL
BEGIN

declare var_qtde int;
set var_qtde = (
    Select count(idcorrespondencia) as qtde 
    from sistemadedocumentos.tb_correspondencias_documentos
    where iddocumentotipo = var_iddocumentotipo
    );

if var_qtde > 0 then
    Select 1 as isconstraints;
else
    Select 0 as isconstraints;
end if; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documentotipo_get`(in var_iddocumentotipo int)
BEGIN
    Select iddocumentotipo, idcategoria, desdocumentotipo, instatus, dtcadastro 
    from tb_documentotipos
    where iddocumentotipo = var_iddocumentotipo;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documentotipo_idcategora_list`(IN `var_idcategoria` INT, in var_instatus bit)
    NO SQL
BEGIN
	Select 
		iddocumentotipo,idcategoria,desdocumentotipo,
		instatus,dtcadastro
	From tb_documentotipos
	where 
        idcategoria = var_idcategoria and
        ((var_instatus = 0) or(instatus = var_instatus));
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documentotipo_remove`(IN `var_iddocumentotipo` INT)
    NO SQL
Begin
	Delete from tb_documentotipos
	where iddocumentotipo = var_iddocumentotipo;
End$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documentotipo_save`(IN `var_iddocumentotipo` INT, IN `var_desdocumentotipo` VARCHAR(200), IN `var_instatus` BIT, IN `var_idcategoria` INT)
    NO SQL
BEGIN

if var_iddocumentotipo = 0 THEN
    INSERT INTO tb_documentotipos(
        	desdocumentotipo,idcategoria
    )values(
        	var_desdocumentotipo,var_idcategoria
    );
    
    Select last_insert_id() as iddocumentotipo;
ELSE
    UPDATE tb_documentotipos
        SET 
            desdocumentotipo = var_desdocumentotipo,
			instatus = var_instatus,
			idcategoria = var_idcategoria
    WHERE iddocumentotipo = var_iddocumentotipo;
    
    Select var_iddocumentotipo as iddocumentotipo;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_get`(IN `var_iddocumento` INT)
    NO SQL
BEGIN
	Select iddocumento,desdocumento,iddocumentotipo,dtcadastro
	From tb_documentos
	where iddocumento = var_iddocumento;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_iddocumentotipo_list`(IN `var_iddocumentotipo` INT)
    NO SQL
BEGIN
	Select iddocumento,desdocumento,iddocumentotipo,dtcadastro
	FROM tb_documentos
	WHERE iddocumentotipo = var_iddocumentotipo;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_idpessoa_get`(IN `var_idpessoa` INT)
    NO SQL
BEGIN
	Select 
    a.iddocumento, a.idpessoa, a.desdocumento,a.iddocumentotipo,
    b.desdocumentotipo,a.dtcadastro
	From tb_documentos a
  inner join tb_documentotipos b
  on a.iddocumentotipo = b.iddocumentotipo
	Where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_idpessoa_remove`(in `var_idpessoa` INT)
    NO SQL
BEGIN

Delete from tb_documentos
where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_list`()
    NO SQL
BEGIN
	Select iddocumento,desdocumento,iddocumentotipo,dtcadastro
	from tb_documentos;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_remove`(IN `var_iddocumento` INT)
    NO SQL
BEGIN
	Delete from tb_documentos
	where iddocumento = var_iddocumento;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_documento_save`(IN `var_iddocumento` INT, IN var_idpessoa int ,IN `var_desdocumento` VARCHAR(200), IN `var_iddocumentotipo` INT)
    NO SQL
BEGIN

if var_iddocumento = 0 THEN
    INSERT INTO tb_documentos(
        	desdocumento,iddocumentotipo,idpessoa
    )values(
        	var_desdocumento,
        	var_iddocumentotipo,
         var_idpessoa
    );
    
    Select last_insert_id() as iddocumento;
ELSE
    UPDATE tb_documentos
        SET 
       desdocumento = var_desdocumento,
			iddocumentotipo = var_iddocumentotipo,
       idpessoa = var_idpessoa
    WHERE iddocumento = var_iddocumento;
    
    Select var_iddocumento as iddocumento;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_endereco_get`(IN `var_idendereco` INT)
    NO SQL
BEGIN
	Select 
		idendereco,idpessoa,descep,desendereco,
		desnumero,descomplemento,desbairro,descidade,
		idestado,desreferencia,inprincipal,dtcadastro
	From tb_enderecos
	Where idendereco = var_idendereco;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_endereco_idpessoa_get`(IN `var_idpessoa` INT)
    NO SQL
BEGIN
	Select 
		a.idendereco,a.idpessoa,a.descep,a.desendereco,
		a.desnumero,a.descomplemento,a.desbairro,a.descidade,
		a.idestado,b.desestado,b.desuf,a.desreferencia,a.inprincipal,a.dtcadastro
	From tb_enderecos a
  inner join tb_estados b
  on a.idestado = b.idestado
	Where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_endereco_idpessoa_remove`(in `var_idpessoa` INT)
    NO SQL
BEGIN

Delete from tb_enderecos
where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_endereco_remove`(in var_idendereco int)
BEGIN
    Delete from tb_enderecos
    where idendereco = var_idendereco;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_endereco_save`(IN `var_idendereco` INT, IN `var_idpessoa` INT, IN `var_descep` VARCHAR(8), IN `var_desendereco` VARCHAR(400), IN `var_desnumero` VARCHAR(20), IN `var_descomplemento` VARCHAR(50), IN `var_desbairro` VARCHAR(100),IN `var_descidade` VARCHAR(200), IN `var_idestado` INT, IN `var_desreferencia` VARCHAR(100),in var_inprincipal bit)
    NO SQL
BEGIN

if(var_inprincipal = 1) then
    call sp_pessoa_endereco_principal_edit(var_idpessoa);
end if;

if var_idendereco = 0 THEN
    INSERT INTO tb_enderecos
	(
        idpessoa,descep,desendereco,desnumero,
        descomplemento,desbairro,descidade,idestado,desreferencia,
        inprincipal
    )
values
	(
        var_idpessoa,var_descep,var_desendereco,
        var_desnumero,var_descomplemento,var_desbairro,
        var_descidade,var_idestado,var_desreferencia,
        var_inprincipal
    );
    
    Select last_insert_id() as idenderco;
ELSE
    UPDATE tb_enderecos
        SET 
       idpessoa = var_idpessoa,
       descep = var_descep,
			desendereco = var_desendereco,
			desnumero = var_desnumero,
			descomplemento = var_descomplemento,
			desbairro = var_desbairro,
       descidade = var_descidade,
			idestado = var_idestado,
			desreferencia = var_desreferencia,
       inprincipal = var_inprincipal
        
    WHERE idendereco = var_idendereco;
    
    Select var_idendereco as idendereco;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_enviotipos_check_constraints`(IN `var_idenviotipo` INT)
    NO SQL
BEGIN

declare var_qtde int;
set var_qtde = (
    Select count(idcorrespondencia) as qtde 
    from sistemadedocumentos.tb_correspondencias
    where  idenviotipo = var_idenviotipo
    );

if var_qtde > 0 then
    Select 1 as isconstraints;
else
    Select 0 as isconstraints;
end if; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_enviotipo_get`(IN `var_idenviotipo` INT)
    NO SQL
BEGIN
	Select idenviotipo, desenviotipo,instatus,dtcadastro
	From tb_enviotipos
	Where idenviotipo = var_idenviotipo;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_enviotipo_list`(in var_instatus bit)
    NO SQL
BEGIN
	Select idenviotipo,desenviotipo,instatus,dtcadastro
	From tb_enviotipos
  where (var_instatus = 0) or(instatus = var_instatus);
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_enviotipo_remove`(in var_idenviotipo int)
BEGIN
    Delete from tb_enviotipos
    where idenviotipo = var_idenviotipo;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_enviotipo_save`(IN `var_idenviotipo` INT, IN `var_desenviotipo` VARCHAR(200),in `var_instatus` bit)
    NO SQL
BEGIN
if var_idenviotipo = 0 THEN

    INSERT INTO tb_enviotipos
	(
        desenviotipo,instatus
    )
values
	(
        var_idenviotipo,var_desenviotipo,var_instatus
     );

	select last_insert_id() as idenviotipo;
ELSE
    UPDATE tb_enviotipos
        SET 
			desenviotipo = var_desenviotipo,
       instatus = var_instatus
    WHERE idenviotipo = var_idenviotipo;
	
	Select idenviotipo as var_idenviotipo;
END IF;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_estado_list`()
    NO SQL
BEGIN
	Select idestado,desestado,desuf
	from tb_estados;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_menus_list`()
BEGIN
    Select idmenu, desmenu, desicone, nrordem, idmenupai, deshandler, dtcadastro 
    from tb_menus
    order by nrordem;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_permissao_menus_save`(
    in var_idpermissaomenu int, in var_idmenu int, in var_idusuario int,
    in var_instatus bit
)
BEGIN
    if var_idpermissaomenu = 0 then
    
        INSERT INTO tb_permissao_menus(
            idmenu, idusuario, instatus
        )values(
            var_idmenu, var_idusuario, var_instatus
        );
        
        Select last_insert_id() as idpermissaomenu;
    else
        Update tb_permissao_menus
        set 
            idusuario = var_idusuario,
            instatus = var_instatus,
            idmenu = var_idmenu,
            dtalteracao = CURRENT_TIMESTAMP()
        where 
            idpermissaomenu = var_idpermissaomenu;
            
        Select var_idpermissaomenu as idpermissaomenu;
    end if ;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_permissao_menus_usuario_list`(in var_idusuario int)
BEGIN 

Select 
    a.idmenu,a.desmenu,a.desicone,a.nrordem,a.idmenupai,
    a.deshandler,a.dtcadastro,b.instatus,b.idpermissaomenu,
    var_idusuario as idusuario 
from tb_menus a
left join tb_permissao_menus b
on a.idmenu = b.idmenu and b.idusuario = var_idusuario
order by nrordem;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoafisica_remove`(in `var_idpessoa` INT)
    NO SQL
BEGIN

Delete from tb_pessoafisica
where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoafisica_save`(IN `var_idpessoafisica` INT,IN `var_idpessoa` INT, IN `var_dtnascimento` DATE, IN `var_dessexo` CHAR(1))
    NO SQL
BEGIN

if var_dessexo = '' then
    set var_dessexo = 'F';
end if; 

if var_idpessoafisica = 0 THEN
    INSERT INTO tb_pessoafisica(
        dessexo,
        idpessoa,
        dtnascimento
    )values(
        	var_dessexo,
        	var_idpessoa,
        	var_dtnascimento
    );
    
    Select last_insert_id() as idpessoafisica;
ELSE
    UPDATE tb_pessoafisica
        SET 
			idpessoa = var_idpessoa,
			dessexo = var_dessexo,
			dtnascimento = var_dtnascimento
    WHERE idpessoafisica = var_idpessoafisica and idpessoa = var_idpessoa;
    
    Select var_idpessoafisica as idpessoafisica;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoajuridica_remove`(in `var_idpessoa` INT)
    NO SQL
BEGIN

Delete from tb_pessoajuridica
where idpessoa = var_idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoajuridica_save`(IN `var_idpessoajuridica` INT,IN `var_idpessoa` INT, IN `var_desramoatividade` VARCHAR(200))
    NO SQL
BEGIN

if var_idpessoajuridica = 0 THEN
    INSERT INTO tb_pessoajuridica(
        idpessoa,
        desramoatividade
    )values(
        	var_idpessoa,
        	var_desramoatividade
    );
    
    Select last_insert_id() as idpessoajuridica;
ELSE
    UPDATE tb_pessoajuridica
        SET 
			idpessoa = var_idpessoa,
			desramoatividade = var_desramoatividade
    WHERE idpessoajuridica = var_idpessoajuridica and idpessoa = var_idpessoa;
    
    Select var_idpessoajuridica as idpessoajuridica;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoas_check_constraints`(IN `var_idpessoa` INT)
    NO SQL
BEGIN

declare var_qtde int;
set var_qtde = (
    Select count(idcorrespondencia) as qtde 
    from sistemadedocumentos.tb_correspondencias
    where idpessoaremetente = var_idpessoa or idpessoadestinatario = var_idpessoa
    );

if var_qtde > 0 then
    Select 1 as isconstraints;
else
    Select 0 as isconstraints;
end if; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoatipos_list`()
    NO SQL
BEGIN
	Select idpessoatipo,despessoatipo
	from tb_pessoastipos;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_contato_principal_edit`(in var_idpessoa int, in var_idcontatotipo int)
BEGIN
    if(
        Select count(idcontato) 
        from tb_contatos 
        where
            idpessoa = var_idpessoa and 
            idcontatotipo = var_idcontatotipo and 
            inprincipal = 1
    ) >= 1 then
        Update tb_contatos
        set inprincipal = 0
        where idpessoa = var_idpessoa and 
        idcontatotipo = var_idcontatotipo;
    end if; 
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_empresa_edit`(
    in var_idpessoa int, in var_idpessoapai int
)
BEGIN
    -- Altera o pai da pessoa, caso o idpessoa, corresponder ao cadastro fisico
    Update tb_pessoafisica
    set idpessoapai = var_idpessoapai
    where idpessoa = var_idpessoa;
    
    -- Altera o pai da pessoa, caso o idpessoa, corresponder ao cadastro fisico
    Update tb_pessoajuridica
    set idpessoapai = var_idpessoapai 
    where idpessoa = var_idpessoa;
    
    Select var_idpessoapai as idpessoa, var_idpessoapai as idpessoapai; 
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_empresa_remove`(
    in var_idpessoa int, 
    in var_idpessoapai int
)
BEGIN
    -- Remove o idpessoapai da tabela pessoafisica, passando pelo idpessoa especifico
    Update tb_pessoafisica
    set idpessoapai = NULL
    where idpessoa = var_idpessoa and idpessoapai = var_idpessoapai;
    
    -- Remove o idpessoapai da tabela pessoajuridica, passando pelo idpessoa especifico
    Update tb_pessoajuridica
    set idpessoapai = NULL
    where idpessoa = var_idpessoa and idpessoapai = var_idpessoapai;
    
    Select var_idpessoa as idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_endereco_principal_edit`(in var_idpessoa int)
BEGIN
    if(
        Select count(idendereco) 
        from tb_enderecos 
        where
            idpessoa = var_idpessoa and 
            inprincipal = 1
    ) >= 1 then
        Update tb_enderecos
        set inprincipal = 0
        where idpessoa = var_idpessoa;
    end if; 
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_fisica_juridica_get`(IN `var_idpessoa` INT)
BEGIN   
    SELECT 
        a.idpessoa,b.idpessoafisica,c.idpessoajuridica,
        a.despessoa,a.idpessoatipo,d.despessoatipo,
        a.instatus,a.dtcadastro,
        b.dessexo,
        case 
            when b.dtnascimento = "0000-00-00" 
            then NULL 
            else b.dtnascimento 
        end as dtnascimento,
        c.desramoatividade,c.idpessoapai,
        pessoa_empresa.idpessoa as idpessoaempresa,
        pessoa_empresa.despessoa as despessoaempresa
    FROM tb_pessoas a
    
    left join tb_pessoafisica b
    on a.idpessoa = b.idpessoa 
    
    left join tb_pessoajuridica c
    on a.idpessoa = c.idpessoa
    
    inner join tb_pessoatipos d
    on a.idpessoatipo = d.idpessoatipo
    
    left join tb_pessoas pessoa_empresa
    on pessoa_empresa.idpessoa = IFNULL(c.idpessoapai,b.idpessoapai)
    
    WHERE a.idpessoa = var_idpessoa;  
    
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_get`(IN `var_idpessoa` INT)
BEGIN
    SELECT idpessoa,despessoa,idpessoatipo,a.instatus,dtcadastro 
    FROM tb_pessoas a
    WHERE idpessoa = var_idpessoa;
    
    
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_getByDespessoa`(IN `var_despessoa` VARCHAR(300))
    NO SQL
BEGIN
	Select idpessoa,despessoa,idpessoatipo,instatus,dtcadastro
	from tb_pessoas
	where despessoa = var_despessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_list`(in var_idpessoatipo int)
BEGIN
    IF var_idpessoatipo IS NOT NULL THEN
        Select 
            a.idpessoa,a.despessoa,a.idpessoatipo,c.despessoatipo,
            b_tel.descontato as descontato_telefone,
            c_email.descontato as descontato_email,
            d.descep, d.desendereco,d.desnumero,d.descomplemento,
            d.desbairro,f.desestado,a.instatus,a.dtcadastro
        FROM sistemadedocumentos.tb_pessoas a
        
        left join sistemadedocumentos.tb_contatos b_tel on 
            a.idpessoa = b_tel.idpessoa and b_tel.idcontatotipo = 1 and b_tel.inprincipal = 1
        
        left join sistemadedocumentos.tb_contatos c_email on 
            a.idpessoa = c_email.idpessoa and c_email.idcontatotipo = 2 and c_email.inprincipal = 1
            
        left join sistemadedocumentos.tb_enderecos d on
            a.idpessoa = d.idpessoa and d.inprincipal = 1
        
        inner join sistemadedocumentos.tb_pessoatipos c on 
            c.idpessoatipo = a.idpessoatipo

        left join sistemadedocumentos.tb_estados f on
            f.idestado = d.idestado
        where c.idpessoatipo = var_idpessoatipo;
    else
        Select 
            a.idpessoa,a.despessoa,a.idpessoatipo,c.despessoatipo,
            b_tel.descontato as descontato_telefone,
            c_email.descontato as descontato_email,
            d.descep, d.desendereco,d.desnumero,d.descomplemento,
            d.desbairro,f.desestado,a.instatus,a.dtcadastro
        FROM sistemadedocumentos.tb_pessoas a
        
        left join sistemadedocumentos.tb_contatos b_tel on 
            a.idpessoa = b_tel.idpessoa and b_tel.idcontatotipo = 1 and b_tel.inprincipal = 1
        
        left join sistemadedocumentos.tb_contatos c_email on 
            a.idpessoa = c_email.idpessoa and c_email.idcontatotipo = 2 and c_email.inprincipal = 1
            
        left join sistemadedocumentos.tb_enderecos d on
            a.idpessoa = d.idpessoa and d.inprincipal = 1
        
        inner join sistemadedocumentos.tb_pessoatipos c on 
            c.idpessoatipo = a.idpessoatipo
        
        left join sistemadedocumentos.tb_estados f on
            f.idestado = d.idestado;
        END IF ; 
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_remove`(IN `var_idpessoa` INT)
BEGIN

DELETE from tb_pessoas where idpessoa = var_idpessoa;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_save`(IN `var_idpessoa` INT, IN `var_despessoa` VARCHAR(100), IN `var_idpessoatipo` INT, IN `var_instatus` BIT)
BEGIN

if var_idpessoa = 0 THEN
    INSERT INTO tb_pessoas(
        despessoa,idpessoatipo
    )
    values
    (
        var_despessoa,var_idpessoatipo
    );
    
    Select last_insert_id() as idpessoa;
ELSE
    UPDATE tb_pessoas
        SET 
            despessoa = var_despessoa,
            idpessoatipo = var_idpessoatipo,
			instatus = var_instatus
        
    WHERE idpessoa = var_idpessoa;
    
    Select var_idpessoa as idpessoa;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_vinculada_edit`(
    in var_idpessoa int, var_idpessoapai int
)
BEGIN
    Update tb_pessoafisica
    set idpessoapai = var_idpessoa
    where idpessoa = var_idpessoapai;
    
    Update tb_pessoajuridica
    set idpessoapai = var_idpessoa
    where idpessoa = var_idpessoapai;
    
    Select var_idpessoa as idpessoa, var_idpessoapai as idpessoa;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_vinculada_idpessoa_get`(in var_idpessoa int)
BEGIN
    SELECT 
        a.idpessoa,a.despessoa,a.idpessoatipo,a.instatus,a.dtcadastro,
        b.idpessoapai as idpessoapaifisica,c.idpessoapai as idpessoapaijuridica 
    FROM tb_pessoas a
    LEFT JOIN tb_pessoafisica b
        on a.idpessoa = b.idpessoa
    LEFT JOIN tb_pessoajuridica c
        on a.idpessoa = c.idpessoa
    Where c.idpessoapai = var_idpessoa or b.idpessoapai = var_idpessoa ;    
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_pessoa_vinculada_remove`(
    in var_idpessoa int, 
    in var_idpessoapai int
)
BEGIN
    Update tb_pessoafisica
    set idpessoapai = NULL
    where idpessoa = var_idpessoa and idpessoapai = var_idpessoapai;
    
    Update tb_pessoajuridica
    set idpessoapai = NULL
    where idpessoa = var_idpessoa and idpessoapai = var_idpessoapai;
    
    Select var_idpessoa as idpessoa, var_idpessoapai as idpessoapai;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_status_get`(IN `var_idstatus` INT)
    NO SQL
BEGIN
	Select idstatus,desstatus
	From tb_status
	WHERE idstatus = var_idstatus;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_status_list`()
    NO SQL
BEGIN
	Select idstatus,desstatus
	FROM tb_status;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_status_save`(IN `var_idstatus` INT, IN `var_desstatus` VARCHAR(50))
    NO SQL
BEGIN

if var_idstatus = 0 THEN
    INSERT INTO tb_status(desstatus)values(var_desstatus);
ELSE
    UPDATE tb_status
        SET 
            desstatus = var_desstatus
        
    WHERE idstatus = var_idstatus;
END IF ; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_usuariologin_get`(IN `var_deslogin` VARCHAR(20), IN `var_dessenha` VARCHAR(20))
    NO SQL
BEGIN
  Select idusuario,deslogin,desnome,instatus,dtcadastro
	FROM tb_usuarios
	WHERE deslogin = var_deslogin and dessenha = var_dessenha and instatus = 1;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_usuarios_check_constraints`(IN `var_idusuario` INT)
    NO SQL
BEGIN

declare var_qtde int;
set var_qtde = (
    Select count(idcorrespondencia) as qtde 
    from sistemadedocumentos.tb_correspondencias
    where idusuario = var_idusuario
    );

if var_qtde > 0 then
    Select 1 as isconstraints;
else
    Select 0 as isconstraints;
end if; 

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_usuario_get`(IN `var_idusuario` INT)
    NO SQL
BEGIN

Select idusuario,deslogin,desnome,dessenha,instatus,dtcadastro
FROM tb_usuarios
WHERE idusuario = var_idusuario;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_usuario_list`(in var_instatus bit)
    NO SQL
BEGIN
    Select idusuario,desnome,deslogin,idpessoa,
            instatus,dtcadastro
    from tb_usuarios
    where 
        (var_instatus = 0) or 
        (var_instatus <> 0 and instatus = var_instatus);
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_usuario_remove`(IN `var_idusuario` INT)
    NO SQL
BEGIN
	Delete from tb_usuarios
	where idusuario = var_idusuario;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `sp_usuario_save`(IN `var_idusuario` INT, IN `var_desnome` VARCHAR(300), IN `var_deslogin` VARCHAR(20), IN `var_dessenha` VARCHAR(20), IN `var_instatus` BIT, IN `var_idpessoa` INT)
    NO SQL
BEGIN

SET var_idpessoa = NULL;

if var_idusuario = 0 or var_idusuario is null THEN

    INSERT INTO tb_usuarios(
        	desnome,deslogin,dessenha,idpessoa,instatus
    )values(
        	var_desnome,
        	var_deslogin,
        	var_dessenha,
        	var_idpessoa,
        	var_instatus
    );
    select last_insert_id() as idusuario;
ELSE
    UPDATE tb_usuarios
        SET 
        desnome = var_desnome,
			deslogin = var_deslogin,
			dessenha = var_dessenha,
			instatus = var_instatus,
			idpessoa = var_idpessoa
    WHERE idusuario = var_idusuario;

	Select var_idusuario as idusuario;

END IF ; 
	
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_arquivos`
--

CREATE TABLE IF NOT EXISTS `tb_arquivos` (
  `idarquivo` int(11) NOT NULL AUTO_INCREMENT,
  `desarquivo` varchar(200) DEFAULT NULL,
  `descaminho` varchar(300) DEFAULT NULL,
  `nrtamanho` int(11) DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idarquivo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='				' AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `tb_arquivos`
--

INSERT INTO `tb_arquivos` (`idarquivo`, `desarquivo`, `descaminho`, `nrtamanho`, `dtcadastro`) VALUES
(10, 'sdfsfsdfsdfsdf2.doc', '/res/arquivos/49/2014/12/14/', 26112, '2014-12-14 12:23:13'),
(11, 'qwdjhgdhad.rtf', '/res/arquivos/49/2014/12/14/', 30932, '2014-12-14 12:25:36'),
(12, 'teste.xls', '/res/arquivos/49/2014/12/14/', 16896, '2014-12-14 12:28:23'),
(13, 'teste.xlsx', '/res/arquivos/49/2014/12/14/', 8286, '2014-12-14 12:29:09'),
(14, 'lista de notebook.xls', '/res/arquivos/74/2015/01/27/', 348759, '2015-01-28 01:09:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoria`
--

CREATE TABLE IF NOT EXISTS `tb_categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `descategoria` varchar(200) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tb_categoria`
--

INSERT INTO `tb_categoria` (`idcategoria`, `descategoria`) VALUES
(1, 'Correspondencia'),
(2, 'Pessoas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_contatos`
--

CREATE TABLE IF NOT EXISTS `tb_contatos` (
  `idcontato` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `descontato` varchar(200) NOT NULL,
  `idcontatotipo` int(11) NOT NULL,
  `inprincipal` bit(1) NOT NULL DEFAULT b'0',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontato`),
  KEY `idpessoa` (`idpessoa`),
  KEY `idcontatotipo` (`idcontatotipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Extraindo dados da tabela `tb_contatos`
--

INSERT INTO `tb_contatos` (`idcontato`, `idpessoa`, `descontato`, `idcontatotipo`, `inprincipal`, `dtcadastro`) VALUES
(45, 33, 'fernandoaramires@gmail.com', 2, b'1', '2015-01-24 19:10:25'),
(46, 34, '11974266594', 1, b'1', '2015-01-24 19:11:35'),
(47, 34, 'jaquepa@hotmail.com', 2, b'1', '2015-01-24 19:11:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_contatotipos`
--

CREATE TABLE IF NOT EXISTS `tb_contatotipos` (
  `idcontatotipo` int(11) NOT NULL AUTO_INCREMENT,
  `descontatotipo` varchar(200) NOT NULL,
  PRIMARY KEY (`idcontatotipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `tb_contatotipos`
--

INSERT INTO `tb_contatotipos` (`idcontatotipo`, `descontatotipo`) VALUES
(1, 'Telefone'),
(2, 'E-mail'),
(3, 'Fax'),
(5, 'Celular');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_correspondencias`
--

CREATE TABLE IF NOT EXISTS `tb_correspondencias` (
  `idcorrespondencia` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) DEFAULT NULL COMMENT 'idusuario: usuario que cadastrou a correspondencia',
  `idpessoaremetente` int(11) DEFAULT NULL,
  `idpessoadestinatario` int(11) DEFAULT NULL,
  `desremetente` varchar(200) DEFAULT NULL,
  `desdestinatario` varchar(200) DEFAULT NULL,
  `idenviotipo` int(11) DEFAULT NULL,
  `idcorrespondenciatipo` int(11) DEFAULT NULL,
  `dtenviado` date DEFAULT NULL,
  `dtrecebido` date DEFAULT NULL,
  `idstatus` int(11) DEFAULT NULL,
  `desobservacao` varchar(200) DEFAULT NULL COMMENT 'desobservacao: campo para uma informa��o mais detalhada do que se trata essa correspondencia',
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcorrespondencia`),
  KEY `fk_correspondencias_status_idstatus` (`idstatus`),
  KEY `idusuario` (`idusuario`),
  KEY `idcorrespondenciatipo` (`idcorrespondenciatipo`),
  KEY `idenviotipo` (`idenviotipo`),
  KEY `idpessoadestinatario` (`idpessoadestinatario`),
  KEY `idpessoaremetente` (`idpessoaremetente`),
  KEY `fk_correspodencias_pessoas_idpessoremetente_idpessoa` (`idpessoaremetente`),
  KEY `fk_correspondencia_usuario_idusuario` (`idusuario`),
  KEY `fk_correspondencias_pessoas_idpessoaremetente` (`idpessoaremetente`),
  KEY `fk_correspondencias_pessoas_idpessadestinatario` (`idpessoadestinatario`),
  KEY `fk_correspondencias_enviotipos_idenviotipo` (`idenviotipo`),
  KEY `fk_correspondencias_correspondenciatipos_idcorrespondenciatipo` (`idcorrespondenciatipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Extraindo dados da tabela `tb_correspondencias`
--

INSERT INTO `tb_correspondencias` (`idcorrespondencia`, `idusuario`, `idpessoaremetente`, `idpessoadestinatario`, `desremetente`, `desdestinatario`, `idenviotipo`, `idcorrespondenciatipo`, `dtenviado`, `dtrecebido`, `idstatus`, `desobservacao`, `dtcadastro`) VALUES
(73, 36, 34, 33, NULL, NULL, 1, 1, '2015-01-24', '0000-00-00', 1, 'sadf', '2015-01-24 19:53:14'),
(74, 36, 34, 33, NULL, NULL, 3, 2, '2015-01-25', '0000-00-00', 2, '', '2015-01-25 19:54:45'),
(75, 36, 34, 33, NULL, NULL, 3, 1, '2015-01-01', NULL, 2, '', '2015-01-25 22:54:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_correspondencias_arquivos`
--

CREATE TABLE IF NOT EXISTS `tb_correspondencias_arquivos` (
  `idcorrespondencia` int(11) NOT NULL,
  `idarquivo` int(11) NOT NULL,
  PRIMARY KEY (`idcorrespondencia`,`idarquivo`),
  KEY `fk_correspondencias_arquivos` (`idarquivo`),
  KEY `fk_correspondencia_idcorrespondencia` (`idcorrespondencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_correspondencias_arquivos`
--

INSERT INTO `tb_correspondencias_arquivos` (`idcorrespondencia`, `idarquivo`) VALUES
(74, 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_correspondencias_documentos`
--

CREATE TABLE IF NOT EXISTS `tb_correspondencias_documentos` (
  `idcorrespondenciadocumento` int(11) NOT NULL AUTO_INCREMENT,
  `idcorrespondencia` int(11) NOT NULL,
  `iddocumentotipo` int(11) NOT NULL,
  `desassunto` varchar(200) NOT NULL,
  `dtdocumento` date NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcorrespondenciadocumento`),
  UNIQUE KEY `UQ_correspondencia_documentos_idcorrespondencia` (`idcorrespondencia`),
  KEY `idcorrespondencia` (`idcorrespondencia`),
  KEY `idcorrespondencia_2` (`idcorrespondencia`),
  KEY `iddocumentotipo` (`iddocumentotipo`),
  KEY `idcorrespondencia_3` (`idcorrespondencia`),
  KEY `idcorrespondencia_4` (`idcorrespondencia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Extraindo dados da tabela `tb_correspondencias_documentos`
--

INSERT INTO `tb_correspondencias_documentos` (`idcorrespondenciadocumento`, `idcorrespondencia`, `iddocumentotipo`, `desassunto`, `dtdocumento`, `dtcadastro`) VALUES
(60, 73, 1, 'adf', '2015-01-24', '2015-01-24 19:53:15'),
(61, 74, 1, 'assdasdsa', '2015-01-25', '2015-01-25 19:54:46'),
(62, 75, 1, 'asdasdasd', '2015-01-25', '2015-01-25 22:54:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_correspondenciashistoricos`
--

CREATE TABLE IF NOT EXISTS `tb_correspondenciashistoricos` (
  `idcorrespondenciahistorico` int(11) NOT NULL AUTO_INCREMENT,
  `descorrespondenciahistorico` varchar(500) DEFAULT NULL,
  `idcorrespondencia` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcorrespondenciahistorico`),
  KEY `fk_correspondenciashistoricos_correspondencia_idcorrespondencia` (`idcorrespondencia`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Extraindo dados da tabela `tb_correspondenciashistoricos`
--

INSERT INTO `tb_correspondenciashistoricos` (`idcorrespondenciahistorico`, `descorrespondenciahistorico`, `idcorrespondencia`, `idusuario`, `dtcadastro`) VALUES
(44, '						asdfasasffasfasfasf							', 74, 36, '2015-01-28 01:09:41'),
(45, '							asdasdasd						', 75, 36, '2015-01-28 01:11:10'),
(46, 'asddddddddddddddddddddddddd', 75, 36, '2015-01-28 01:11:27'),
(47, 'ASas', 75, 36, '2015-01-28 01:11:35'),
(48, 'AS', 75, 36, '2015-01-28 01:11:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_correspondenciatipos`
--

CREATE TABLE IF NOT EXISTS `tb_correspondenciatipos` (
  `idcorrespondenciatipo` int(11) NOT NULL AUTO_INCREMENT,
  `descorrespondenciatipo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idcorrespondenciatipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tb_correspondenciatipos`
--

INSERT INTO `tb_correspondenciatipos` (`idcorrespondenciatipo`, `descorrespondenciatipo`) VALUES
(1, 'Enviados'),
(2, 'Recebidos'),
(3, 'Interno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_documentos`
--

CREATE TABLE IF NOT EXISTS `tb_documentos` (
  `iddocumento` int(11) NOT NULL AUTO_INCREMENT,
  `iddocumentotipo` int(11) NOT NULL,
  `idpessoa` int(11) DEFAULT NULL,
  `desdocumento` varchar(200) DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddocumento`),
  KEY `iddocumentotipo` (`iddocumentotipo`),
  KEY `fk_documentos_pessoas_idpessoa` (`idpessoa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `tb_documentos`
--

INSERT INTO `tb_documentos` (`iddocumento`, `iddocumentotipo`, `idpessoa`, `desdocumento`, `dtcadastro`) VALUES
(13, 7, 33, '32668532884', '2015-01-26 01:47:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_documentotipos`
--

CREATE TABLE IF NOT EXISTS `tb_documentotipos` (
  `iddocumentotipo` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) DEFAULT NULL,
  `desdocumentotipo` varchar(100) DEFAULT NULL,
  `instatus` bit(1) DEFAULT b'1',
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddocumentotipo`),
  KEY `idcategoria` (`idcategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `tb_documentotipos`
--

INSERT INTO `tb_documentotipos` (`iddocumentotipo`, `idcategoria`, `desdocumentotipo`, `instatus`, `dtcadastro`) VALUES
(1, 1, 'Carta', b'1', '2014-10-30 05:37:35'),
(2, 1, 'Proposta Comercial', b'1', '2014-09-29 05:39:52'),
(3, 1, 'Procura��o', b'0', '2014-09-29 05:40:15'),
(4, 1, 'Atestado M�dico', b'1', '2014-09-29 05:40:39'),
(5, 1, 'Fatura', b'1', '2014-09-29 05:40:50'),
(6, 2, 'RG', b'1', '2014-09-29 05:42:47'),
(7, 2, 'CPF', b'1', '2014-09-29 05:43:12'),
(8, 2, 'CNH', b'1', '2014-09-29 05:43:22'),
(9, 2, 'CNPJ', b'1', '2014-09-29 05:43:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_enderecos`
--

CREATE TABLE IF NOT EXISTS `tb_enderecos` (
  `idendereco` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `descep` char(8) NOT NULL,
  `desendereco` varchar(300) DEFAULT NULL,
  `desnumero` varchar(20) DEFAULT NULL,
  `descomplemento` varchar(100) DEFAULT NULL,
  `desbairro` varchar(200) DEFAULT NULL,
  `descidade` varchar(200) DEFAULT NULL,
  `idestado` int(11) DEFAULT NULL,
  `desreferencia` varchar(500) DEFAULT NULL,
  `inprincipal` bit(1) DEFAULT b'0',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idendereco`),
  KEY `fk_enderecos_pessoas_idpessoa` (`idpessoa`),
  KEY `idestado` (`idestado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `tb_enderecos`
--

INSERT INTO `tb_enderecos` (`idendereco`, `idpessoa`, `descep`, `desendereco`, `desnumero`, `descomplemento`, `desbairro`, `descidade`, `idestado`, `desreferencia`, `inprincipal`, `dtcadastro`) VALUES
(17, 33, '01316000', 'Rua Francisca Miquelina', '23423', '', 'Bela Vista', 'S�o Paulo', 25, '', b'1', '2015-01-26 00:47:48'),
(19, 33, '01316000', 'Rua Francisca Miquelina', 'zsgfsddfdsf', '', 'Bela Vista', 'S�o Paulo', 25, '', b'0', '2015-01-26 03:14:21'),
(24, 34, '02442020', 'Rua Liestal', 'dsfdsfsdf', 'dsfsdfsdf', 'Lauzane Paulista', 'S�o Paulo', 25, '', b'0', '2015-01-26 04:37:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_enviotipos`
--

CREATE TABLE IF NOT EXISTS `tb_enviotipos` (
  `idenviotipo` int(11) NOT NULL AUTO_INCREMENT,
  `desenviotipo` varchar(100) DEFAULT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idenviotipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `tb_enviotipos`
--

INSERT INTO `tb_enviotipos` (`idenviotipo`, `desenviotipo`, `instatus`, `dtcadastro`) VALUES
(1, 'Normal', b'1', '2014-09-30 03:22:36'),
(2, 'Registrado', b'1', '2014-09-30 03:22:36'),
(3, 'Eletronico', b'1', '2014-09-30 03:22:42'),
(9, 'Sedex', b'0', '2014-10-09 17:53:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_estados`
--

CREATE TABLE IF NOT EXISTS `tb_estados` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `desestado` varchar(100) NOT NULL,
  `desuf` char(2) NOT NULL,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Extraindo dados da tabela `tb_estados`
--

INSERT INTO `tb_estados` (`idestado`, `desestado`, `desuf`) VALUES
(1, 'Acre', 'AC'),
(2, 'Alagoas', 'AL'),
(3, 'Amap�', 'AP'),
(4, 'Amazonas', 'AM'),
(5, 'Bahia', 'BA'),
(6, 'Cear�', 'CE'),
(7, 'Distrito Federal', 'DF'),
(8, 'Esp�rito Santo', 'ES'),
(9, 'Goi�s', 'GO'),
(10, 'Maranh�o', 'MA'),
(11, 'Mato Grosso', 'MT'),
(12, 'Mato Grosso do Sul', 'MS'),
(13, 'Minas Gerais', 'MG'),
(14, 'Par�', 'PA'),
(15, 'Para�ba', 'PB'),
(16, 'Paran�', 'PR'),
(17, 'Pernambuco', 'PE'),
(18, 'Piau�', 'PI'),
(19, 'Rio de Janeiro', 'RJ'),
(20, 'Rio Grande do Norte', 'RN'),
(21, 'Rio Grande do Sul', 'RS'),
(22, 'Rond�nia', 'RD'),
(23, 'Roraima', 'RR'),
(24, 'Santa Catarina', 'SC'),
(25, 'S�o Paulo', 'SP'),
(26, 'Sergipe', 'SE'),
(27, 'Tocantins', 'TO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_menus`
--

CREATE TABLE IF NOT EXISTS `tb_menus` (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `desmenu` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `desicone` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `nrordem` int(11) DEFAULT NULL,
  `idmenupai` int(11) DEFAULT NULL,
  `deshandler` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `tb_menus`
--

INSERT INTO `tb_menus` (`idmenu`, `desmenu`, `desicone`, `nrordem`, `idmenupai`, `deshandler`, `dtcadastro`) VALUES
(1, 'Sistema de Gerenciamento de Correspodencia', 'fa fa-home', 0, NULL, NULL, '2015-01-22 01:07:27'),
(2, 'Home', 'fa fa-home', 1, 1, NULL, '2015-01-22 01:08:05'),
(3, 'Correspondencias', 'fa fa-envelope', 3, 1, 'correspondencias.php', '2015-01-22 01:08:35'),
(4, 'Cal�ndario', 'fa fa-calendar', 2, 1, 'calendario.php', '2015-01-22 01:08:54'),
(5, 'Tipo de Envios', 'fa fa-send-o', 4, 1, 'enviotipo.php', '2015-01-22 01:09:07'),
(6, 'Tipo de Documentos', 'fa fa-folder-open-o', 5, 1, 'documentotipos.php', '2015-01-22 01:09:19'),
(7, 'Pessoas', 'fa fa-users', 6, 1, 'pessoas.php', '2015-01-22 01:09:30'),
(8, 'Usuarios', 'fa fa-user', 7, 1, 'usuarios.php', '2015-01-22 01:09:39'),
(9, 'Permiss�es', 'fa fa-tasks', 8, 1, 'permissoes.php', '2015-01-22 01:09:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_permissao_menus`
--

CREATE TABLE IF NOT EXISTS `tb_permissao_menus` (
  `idpermissaomenu` int(11) NOT NULL AUTO_INCREMENT,
  `idmenu` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `instatus` bit(1) DEFAULT b'1',
  `dtalteracao` datetime DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpermissaomenu`),
  UNIQUE KEY `uq_permissao_menus_idusuario_idmenu` (`idmenu`,`idusuario`),
  KEY `fk_permissao_menus_usuarios_idusuario` (`idusuario`),
  KEY `fk_permissao_menus_menus_idmenu` (`idmenu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Extraindo dados da tabela `tb_permissao_menus`
--

INSERT INTO `tb_permissao_menus` (`idpermissaomenu`, `idmenu`, `idusuario`, `instatus`, `dtalteracao`, `dtcadastro`) VALUES
(1, 1, 36, b'1', NULL, '2015-01-23 00:36:47'),
(9, 2, 36, b'1', NULL, '2015-01-23 00:38:41'),
(10, 3, 36, b'1', NULL, '2015-01-23 00:38:42'),
(11, 4, 36, b'1', NULL, '2015-01-23 00:38:42'),
(12, 5, 36, b'1', NULL, '2015-01-23 00:38:42'),
(13, 6, 36, b'1', NULL, '2015-01-23 00:38:43'),
(14, 7, 36, b'1', NULL, '2015-01-23 00:38:43'),
(15, 8, 36, b'1', NULL, '2015-01-23 00:38:43'),
(16, 9, 36, b'1', NULL, '2015-01-23 00:40:17'),
(17, 9, 60, b'0', '2015-01-24 13:11:25', '2015-01-24 13:09:50'),
(18, 8, 60, b'0', '2015-01-24 13:11:30', '2015-01-24 13:09:51'),
(19, 6, 60, b'1', NULL, '2015-01-24 13:09:52'),
(20, 7, 60, b'1', NULL, '2015-01-24 13:09:53'),
(23, 4, 60, b'1', NULL, '2015-01-24 13:10:11'),
(24, 3, 60, b'1', NULL, '2015-01-24 13:10:12'),
(25, 1, 65, b'1', NULL, '2015-01-25 18:40:48'),
(26, 2, 65, b'1', NULL, '2015-01-25 18:40:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pessoafisica`
--

CREATE TABLE IF NOT EXISTS `tb_pessoafisica` (
  `idpessoafisica` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `dtnascimento` date DEFAULT NULL,
  `dessexo` char(1) DEFAULT 'F',
  `idpessoapai` int(11) DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpessoafisica`),
  KEY `fk_pessoafisica_pessoa_idpessoa` (`idpessoa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Extraindo dados da tabela `tb_pessoafisica`
--

INSERT INTO `tb_pessoafisica` (`idpessoafisica`, `idpessoa`, `dtnascimento`, `dessexo`, `idpessoapai`, `dtcadastro`) VALUES
(30, 33, '0000-00-00', 'F', NULL, '2015-01-24 19:10:23'),
(31, 34, '0000-00-00', 'F', 76, '2015-01-24 19:11:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pessoajuridica`
--

CREATE TABLE IF NOT EXISTS `tb_pessoajuridica` (
  `idpessoajuridica` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `desramoatividade` varchar(200) DEFAULT NULL,
  `idpessoapai` int(11) DEFAULT NULL,
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpessoajuridica`),
  KEY `fk_pessoajujridica_pessoa_idpessoa` (`idpessoa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pessoas`
--

CREATE TABLE IF NOT EXISTS `tb_pessoas` (
  `idpessoa` int(11) NOT NULL AUTO_INCREMENT,
  `despessoa` varchar(100) DEFAULT NULL,
  `idpessoatipo` int(11) DEFAULT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpessoa`),
  KEY `idpessoatipo` (`idpessoatipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Extraindo dados da tabela `tb_pessoas`
--

INSERT INTO `tb_pessoas` (`idpessoa`, `despessoa`, `idpessoatipo`, `instatus`, `dtcadastro`) VALUES
(33, 'Fernando de Azevedo Ramires', 1, b'1', '2015-01-24 19:10:22'),
(34, 'Jaqueline Paola Abdal Ramires', 1, b'1', '2015-01-24 19:11:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pessoas_arquivos`
--

CREATE TABLE IF NOT EXISTS `tb_pessoas_arquivos` (
  `idpessoaarquivo` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `idarquivo` int(11) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpessoaarquivo`),
  KEY `fk_pessoas_arquivos_pessoas_idpessoa` (`idpessoa`),
  KEY `fk_pessoas_arquivos_arquivos_idarquivo` (`idarquivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pessoatipos`
--

CREATE TABLE IF NOT EXISTS `tb_pessoatipos` (
  `idpessoatipo` int(11) NOT NULL AUTO_INCREMENT,
  `despessoatipo` varchar(200) NOT NULL,
  PRIMARY KEY (`idpessoatipo`),
  KEY `idpessoatipo` (`idpessoatipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tb_pessoatipos`
--

INSERT INTO `tb_pessoatipos` (`idpessoatipo`, `despessoatipo`) VALUES
(1, 'Pessoa F�sica'),
(2, 'Pessoa Juridica');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_status`
--

CREATE TABLE IF NOT EXISTS `tb_status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `desstatus` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tb_status`
--

INSERT INTO `tb_status` (`idstatus`, `desstatus`) VALUES
(1, 'Recebido'),
(2, 'Pendente'),
(3, 'Enviado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuarios`
--

CREATE TABLE IF NOT EXISTS `tb_usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) DEFAULT NULL,
  `desnome` varchar(200) NOT NULL,
  `deslogin` varchar(100) DEFAULT NULL,
  `dessenha` varchar(20) DEFAULT NULL,
  `instatus` bit(1) NOT NULL DEFAULT b'1',
  `dtcadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idusuario`),
  KEY `idpessoa` (`idpessoa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Extraindo dados da tabela `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`idusuario`, `idpessoa`, `desnome`, `deslogin`, `dessenha`, `instatus`, `dtcadastro`) VALUES
(36, NULL, 'Teste', 'teste', '123456', b'1', '2014-10-01 18:38:10'),
(53, NULL, 'Fernando de Azevedo Ramires', 'framires', '123456', b'1', '2014-10-16 04:58:05'),
(60, NULL, 'Bruno Bove Barbosa', 'bruno', '123456', b'1', '2014-11-04 23:12:57'),
(65, NULL, 'sdsdfsdf', 'sfsdfsdfsdfsd', 'sdfsdfsd', b'0', '2015-01-25 18:40:47');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_pessoa_enderecos_one`
--
CREATE TABLE IF NOT EXISTS `v_pessoa_enderecos_one` (
`idendereco` int(11)
,`idpessoa` int(11)
);
-- --------------------------------------------------------

--
-- Structure for view `v_pessoa_enderecos_one`
--
DROP TABLE IF EXISTS `v_pessoa_enderecos_one`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_pessoa_enderecos_one` AS select min(`tb_enderecos`.`idendereco`) AS `idendereco`,`tb_enderecos`.`idpessoa` AS `idpessoa` from `tb_enderecos` group by `tb_enderecos`.`idpessoa`;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_contatos`
--
ALTER TABLE `tb_contatos`
  ADD CONSTRAINT `fk_contatos_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contatos_contatotipos_idcontatotipo` FOREIGN KEY (`idcontatotipo`) REFERENCES `tb_contatotipos` (`idcontatotipo`);

--
-- Limitadores para a tabela `tb_correspondencias`
--
ALTER TABLE `tb_correspondencias`
  ADD CONSTRAINT `fk_correspondencias_correspondenciatipos_idcorrespondenciatipo` FOREIGN KEY (`idcorrespondenciatipo`) REFERENCES `tb_correspondenciatipos` (`idcorrespondenciatipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondencias_enviotipos_idenviotipo` FOREIGN KEY (`idenviotipo`) REFERENCES `tb_enviotipos` (`idenviotipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondencias_pessoas_idpessadestinatario` FOREIGN KEY (`idpessoadestinatario`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondencias_pessoas_idpessoaremetente` FOREIGN KEY (`idpessoaremetente`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondencias_status_idstatus` FOREIGN KEY (`idstatus`) REFERENCES `tb_status` (`idstatus`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondencias_usuario_idusuario` FOREIGN KEY (`idusuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_correspondencias_arquivos`
--
ALTER TABLE `tb_correspondencias_arquivos`
  ADD CONSTRAINT `fk_correspondencia_idcorrespondencia` FOREIGN KEY (`idcorrespondencia`) REFERENCES `tb_correspondencias` (`idcorrespondencia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondencias_arquivos` FOREIGN KEY (`idarquivo`) REFERENCES `tb_arquivos` (`idarquivo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_correspondencias_documentos`
--
ALTER TABLE `tb_correspondencias_documentos`
  ADD CONSTRAINT `fk_correspondenciadocumento_correspondencia_idcorrespondencia` FOREIGN KEY (`idcorrespondencia`) REFERENCES `tb_correspondencias` (`idcorrespondencia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_correspondenciadocumentos_iddocumentotipo` FOREIGN KEY (`iddocumentotipo`) REFERENCES `tb_documentotipos` (`iddocumentotipo`);

--
-- Limitadores para a tabela `tb_correspondenciashistoricos`
--
ALTER TABLE `tb_correspondenciashistoricos`
  ADD CONSTRAINT `fk_correspondenciashistoricos_correspondencia_idcorrespondencia` FOREIGN KEY (`idcorrespondencia`) REFERENCES `tb_correspondencias` (`idcorrespondencia`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_documentos`
--
ALTER TABLE `tb_documentos`
  ADD CONSTRAINT `fk_documentos_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_documentos_documentotipos_iddocumentotipo` FOREIGN KEY (`iddocumentotipo`) REFERENCES `tb_documentotipos` (`iddocumentotipo`);

--
-- Limitadores para a tabela `tb_documentotipos`
--
ALTER TABLE `tb_documentotipos`
  ADD CONSTRAINT `fk_documentotipos_categoria_idcategoria` FOREIGN KEY (`idcategoria`) REFERENCES `tb_categoria` (`idcategoria`);

--
-- Limitadores para a tabela `tb_enderecos`
--
ALTER TABLE `tb_enderecos`
  ADD CONSTRAINT `fk_enderecos_estados_idestado` FOREIGN KEY (`idestado`) REFERENCES `tb_estados` (`idestado`),
  ADD CONSTRAINT `fk_enderecos_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_permissao_menus`
--
ALTER TABLE `tb_permissao_menus`
  ADD CONSTRAINT `fk_permissao_menus_menus_idmenu` FOREIGN KEY (`idmenu`) REFERENCES `tb_menus` (`idmenu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_permissao_menus_usuarios_idusuario` FOREIGN KEY (`idusuario`) REFERENCES `tb_usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_pessoafisica`
--
ALTER TABLE `tb_pessoafisica`
  ADD CONSTRAINT `fk_pessoafisica_pessoa_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_pessoajuridica`
--
ALTER TABLE `tb_pessoajuridica`
  ADD CONSTRAINT `fk_pessoajujridica_pessoa_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_pessoas`
--
ALTER TABLE `tb_pessoas`
  ADD CONSTRAINT `fk_pessoas_pessoatipos_idpessoatipo` FOREIGN KEY (`idpessoatipo`) REFERENCES `tb_pessoatipos` (`idpessoatipo`);

--
-- Limitadores para a tabela `tb_pessoas_arquivos`
--
ALTER TABLE `tb_pessoas_arquivos`
  ADD CONSTRAINT `fk_pessoas_arquivos_arquivos_idarquivo` FOREIGN KEY (`idarquivo`) REFERENCES `tb_arquivos` (`idarquivo`),
  ADD CONSTRAINT `fk_pessoas_arquivos_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`);

--
-- Limitadores para a tabela `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD CONSTRAINT `fk_usuario_pessoas_idpessoa` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
