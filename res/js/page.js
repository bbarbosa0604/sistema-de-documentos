var page = {

    table_datable:{
        "table_data_correspondencia":true,
        "table_data_usuarios":true,
        "table_data_enviotipos":true,
        "table_data_pessoas":true,
        "table_data_documentotipos":true,
        "table_data_vincularpessoa":false // Valor falso, porque a estrutura da tabela é criada dinamincamente.

    },
    actions:{},

    estados:{
        "estados":['AC','AL','AP','AM','BA','CE','DF','ES',
                   'GO','MA','MT','MS','MG','PA','PB','PR','PE',
                   'PI','RJ','RN','RS','RD','RR','SC','SP','SE','TO'
            ]
    },
    
    week_day:['Domingo','Segunda - Feira','Terça - Feira','Quarta - Feira','Quinta - Feira','Sexta - Feira', 'Sábado'],
    month:['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],

    changeSerialize:false,

    validarCNPJ:function(cnpj) {     
        cnpj = cnpj.replace(/[^\d]+/g,'');
     
        if(cnpj == '') return false;
         
        if (cnpj.length != 14)
            return false;
     
        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" || 
            cnpj == "11111111111111" || 
            cnpj == "22222222222222" || 
            cnpj == "33333333333333" || 
            cnpj == "44444444444444" ||                 
            cnpj == "55555555555555" || 
            cnpj == "66666666666666" || 
            cnpj == "77777777777777" || 
            cnpj == "88888888888888" || 
            cnpj == "99999999999999")
            return false;
             
        // Valida DVs
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
             
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
              return false;
               
        return true;
        
    },

    validarCpf:function(strCPF) {
        strCPF = strCPF.replace(/[^\d]+/g,'');
        var Soma;
        var Resto;
        Soma = 0;   
        //strCPF  = RetiraCaracteresInvalidos(strCPF,11);
        if (strCPF == "00000000000")
        return false;
        for (i=1; i<=9; i++)
        Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i); 
        Resto = (Soma * 10) % 11;
        if ((Resto == 10) || (Resto == 11)) 
        Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10)) )
        return false;
        Soma = 0;
        for (i = 1; i <= 10; i++)
           Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
        if ((Resto == 10) || (Resto == 11)) 
        Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11) ) )
            return false;
        return true;
    },

    validarEmail:function(email){
        var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return (filtro.test(email))?true:false;

    },

    setMask:function(value){
        $("[name*=descontato]").off();
        $("[name*=descontato]").unbind("blur");
        $("[name*=email]").unbind("blur");
        $("[name*=desdocumento]").unbind("blur");
        $("[type=submit]").prop("disabled",false);

        switch(value.toUpperCase()){
            case "CEP":
                $(".cep, .CEP").mask("99999999");
            break;
            case"CPF":
                $(".cpf, .CPF").mask("99999999999");
                $("[name*=desdocumento]").bind("blur",function(){

                        $("[type=submit]").prop("disabled",true);

                        if(page.validarCpf($(this).val())){

                            $("[type=submit]").prop("disabled",false);

                        }else{
                            toastr.error("CPF inválido!","CPF");
                        }

                    return true; 

                });
            break;
            case "CNPJ":
                $(".cnpj, .CNPJ").mask("99999999999999");

                $("[name*=desdocumento]").bind("blur",function(){

                        $("[type=submit]").prop("disabled",true);

                        if(page.validarCNPJ($(this).val())){

                            $("[type=submit]").prop("disabled",false);

                        }else{
                            toastr.error("CNPJ inválido!","CNPJ");
                        }

                    return true;
                });
            break;
            case "TELEFONE":
                $(".Telefone, .Celular, .Fax").mask("9999999999?9");
            break;
            case "CELULAR":
                $(".Telefone, .Celular, .Fax").mask("9999999999?9");
            break;
            case "FAX":
                $(".Telefone, .Celular, .Fax").mask("9999999999?9");
            break;            
            case "E-MAIL":
                $("[name*=descontato]").bind("blur",function(){

                        $("[type=submit]").prop("disabled",true);

                        if(page.validarEmail($(this).val())){

                            $("[type=submit]").prop("disabled",false);

                        }else{
                            toastr.error("E-mail Inválido!","E-mail");
                        }

                    return true 
                });
                $("[name*=email]").bind("blur",function(){

                        $("[type=submit]").prop("disabled",true);

                        if(page.validarEmail($(this).val())){

                            $("[type=submit]").prop("disabled",false);

                        }else{
                            toastr.error("E-mail Inválido!","E-mail");
                        }

                    return true 
                });
            break;
            default:
                $("[name*=descontato]").unmask("");
                $("[name*=desdocumento]").unmask("");
        }
    },

    setDataTable:function(){
       $.each(page.table_datable,function(key,value,c){
                if(value){
                    if(key=="table_data_correspondencia"){
                        $("#" + key + "").dataTable({    
                            "aoColumnDefs": [
                                {"aTargets":[ 5 ], 
                                "bSortable": true, 
                                "sType": "date-uk-pre"}
                                    ]
                                });                        
                    }else{
                        $("#" + key + "").dataTable();
                    }
                }
        });
    },

    /*
        Utilizada para Resetar os valores em um formulario
    */
    reset:function(form){

        $(form)[0].reset();
        $(form).find("[class*=checked]").removeClass("checked");
        $(form).find("[type*=hidden]").val("");


        $('.modal').on('hidden.bs.modal', function (e) {

        if($(this).find("form")[0] != undefined){
            page.reset($(this).find("form"));
        }

        disableFieldsCorrespondencia(false);
         
       if( $('#full-calendar')[0] != undefined ){
            $('#full-calendar').fullCalendar( 'refetchEvents' );
        }

        if( $("#form-fileupload")[0] != undefined ){
            $("#show_upload_file").addClass("no-display");
            $('.files .name').html("");
            $('.files .size').html("");
            $('.progress-bar').css("width","0%").html("");
            $("#form-fileupload")[0].reset();

        }

       if($(this).find("[class*=correspondencia]")[0]!=undefined){
            $("#tab_correspondencia li").removeClass("active");
            $("#tab_correspondencia li:first-child a").click();
        }
    });

    },
    /*
        UTILIZADO PARA CARREGAR QUALQUER FORMULARIO
        - params é o objeto, chave e valor = {chave:valor}
        - form é o id do formulario que será carregado
    */
    configUiNormal:{
            overlayCSS: {
                backgroundColor: '#fff'
            },
            message: '<i class="fa fa-spinner fa-spin loading_personal_normal"></i>',
            css: {
                border: 'none',
                color: '#333',
                background: 'none'
            }
    },
    configUi:{
            overlayCSS: {
                backgroundColor: '#fff'
            },
            message: '<i class="fa fa-spinner fa-spin loading_personal"></i>',
            css: {
                border: 'none',
                color: '#333',
                background: 'none'
            }
    },
    configUiPage:{
            overlayCSS: {
                backgroundColor: '#000'
            },
            message: '<h1 class="text-bold">Carregando</h1>'+
            '<i class="fa fa-cog fa-spin loading_personal_page"></i>',
            css: {
                border: 'none',
                color: '#fff',
                background: 'none'
            }
    },
    load:function(params,form){
        page.reset(form);
        $.ajax({
              dataType: "json",
              url: this.actions.url_load,
              data: params,
              beforeSend:function(){
                $(".loadmask").block(page.configUi);
             },
              success: function(json){
                $(form).loadData(json);                
                
              },
            error:function(xhr,textStatus,errorThrown){
                toastr.error(errorThrown,"ERROR");
                console.dir(textStatus);
                console.dir(errorThrown);

            }
            }).done(function(){
                $(".loadmask").unblock();

            }).always(function(){
                $(".loadmask").unblock();
            });

    },
    save:function(form,modal,options,callback){
        var params = page.changeSerialize?form.serializeArray2():form.serializeArray();
        
        options = options || null;

       $.ajax({
            method:"POST",
            dataType:'json',
            url:this.actions.url_save,
            data:params,
            success:function(json){
                page.reset(form);
                modal.modal('hide');

                if(json.success){
                    toastr.success(json.msg,json.titlemsg);
                    options?page.reload():"";
                    callback?callback(json.success):"";

                }else{
                    toastr.error(json.msg,json.titlemsg);
                    callback?callback(json.success):"";
                }
            },
            beforeSend:function(){
                $("[type=submit]").button("loading");
                $(".loadmask").block(page.configUiNormal);
            },
            error:function(xhr,textStatus,errorThrown){
                toastr.error(errorThrown,"ERROR");
                console.dir(textStatus);
                console.dir(errorThrown);

            }
        }).done(function(){
            $(".loadmask").unblock();

        }).always(function(){
            $("[type=submit]").button("reset");
            $(".loadmask").unblock();
        });
   },
    save_no_modal:function(form){
        $.ajax({
            method:"POST",
            dataType:'json',
            url:this.actions.url_save,
            data:this.actions.form_params,
            success:function(json){
                if(json.success){
                    toastr.success(json.msg,json.titlemsg);
                }else{
                    toastr.error(json.msg,json.titlemsg);
                }
            },
            beforeSend:function(){
                $(".loadmask").block(page.configUiNormal);
            },
            error:function(xhr,textStatus,errorThrown){
                toastr.error(errorThrown,"ERROR");
                console.dir(textStatus);
                console.dir(errorThrown);
            }
        }).done(function(){
            $(".loadmask").unblock();

        }).always(function(){
            $(".loadmask").unblock();
        });
   },
    delete:function(options,callback){
    
    options.reload = options.reload || null;

    $.ajax({
        method:"POST",
        dataType:'json',
        url:options.url_delete,
        data:options.form_params,

        success:function(json){
            if(json.success){
                toastr.info(json.msg,json.titlemsg);
                options.reload?page.reload():"";
                callback?callback(json.success):"";
        }else{
            toastr.error(json.msg,json.titlemsg);
            callback?callback(json.success):"";
            }
        },
        beforeSend:function(){
            $(".fullsizePage ").removeClass("loadmask").addClass("loadmask");
            $(".loadmask").block(page.configUiNormal);
        },
        error:function(xhr,textStatus,errorThrown){
            toastr.error(errorThrown,"ERROR");
            console.dir(textStatus);
            console.dir(errorThrown);
        }
    }).done(function(){
        $(".loadmask").unblock();
        $(".fullsizePage ").removeClass("loadmask");

    }).always(function(){
        $("[type=submit]").button("reset");
        $(".loadmask").unblock();
    });

   },
    reload:function(){
        window.location.reload();
   },
   reload_content:function(options){

    $(".panel-body").removeClass("animated").removeClass("fadeInDown");
    $(".panel-body").load(options.url_reload,options.params,function(){
        $(".panel-body").addClass("animated fadeInDown");

        $.getScript(PATH_URL+options.js);
        
        });

    return true;
   },
    load_ajax_content:function(options){
        
        $.ajax({
            url:options.url_load,
            method:"POST",
            dataType:options.tipo,
            data:options.form_params,

            success:function(json){
                if(typeof options.callback == 'function') options.callback(json);
            },
            error:function(){
                toastr.error("","Ocorreu um erro ao Carregar os dados");
            }

        });
    }

}
    

    ////////////////////////////////////////////////////////////////////////////////////////
        
    $('.modal').on('hidden.bs.modal', function (e) {

        if($(this).find("form")[0] != undefined){
            page.reset($(this).find("form"));
        }

        disableFieldsCorrespondencia(false);
         
       if( $('#full-calendar')[0] != undefined ){
            $('#full-calendar').fullCalendar( 'refetchEvents' );
        }

        if( $("#form-fileupload")[0] != undefined ){
            $("#show_upload_file").addClass("no-display");
            $('.files .name').html("");
            $('.files .size').html("");
            $('.progress-bar').css("width","0%").html("");
            $("#form-fileupload")[0].reset();

        }

       if($(this).find("[class*=correspondencia]")[0]!=undefined){
            $("#tab_correspondencia li").removeClass("active");
            $("#tab_correspondencia li:first-child a").click();
        }
    });

/*    $(".dropdown.current-user li a").on("click",function(){
        
        
        window.location = $(this).data("url");
    });*/

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-uk-pre": function ( a ) {
        var ukDatea = a.split('/');
        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    },

    "date-uk-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-uk-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
    } );

    
   /////////////////////////////////////////////////////////////////////////////////////


    $(".dropdown-menu.dropdown-dark a").on("click", function(e){        
        if ($(this).data("url")){
            e.preventDefault();
            loadPage($(this));

        }

    });
    $(".main-navigation-menu a").on("click",function(){
        loadPage($(this));
    });
   /////////////////////////////////////////////////////////////////////////////////////
    /*
        Desabilita a insercão de dados no Input
        que estiver com a Classe field_readonly
    */

    function fieldsReadOnly(){
        $(".field_readonly").keydown(function(e){
            e.preventDefault();
        });
    }

    fieldsReadOnly();


    function loadPage(t){

        if($(".close-subviews"))$(".close-subviews").click();

        $(".main-content").addClass("loadmask").removeClass("fadeIn");
        $(".loadmask").block(page.configUiPage);


        if(t[0].href != "javascript:void(0);") return false;

        $.ajax({
            url:t.data("url"),
            dataType:"html",
            success:function(data){
                $(".main-content").html(data);
            },
            error:function(){
                toastr.error("Erro ao Carregar a Pagina");
                $(".main-content").removeClass("loadmask").addClass("fadeIn");
                $(".loadmask").block(page.configUiPage);
            },
            complete:function(data){
                $(".loadmask").unblock();
                $(".main-content").removeClass("loadmask").addClass("fadeIn");
            }

        });
    }

   /////////////////////////////////////////////////////////////////////////////////////

   function disableFieldsCorrespondencia(v,status){
        
        status = status || 0; 

        $("[id*=select]:not('#select_idstatus')").prop("disabled",v);
        $("[id*=button_]").prop("disabled",v);
        $("textarea[name*='desobservacao'], input:not([type='hidden']):not([class*='setup']):not([name='email']):not([name='assunto']):not([name='dtrecebido'])").prop("disabled",v);

            if (status == 1) {
                $("[name='dtrecebido']").prop("disabled",true);
           }else{
                $("[name='dtrecebido']").prop("disabled",false);
            }

    }


//////////////////////FUNCTION PARA CARREGAR JSON NOS FORMULARIOS///////////////////
(function($){
    $.fn.loadData = function(data){
        // Interando sobre o Object passado e setando os valores nos campos
        for(var campo in data){
            // Recuperando o elemento do campo
            var el = $(this).find('[name="'+ campo +'"]');

            // Verificando se foi resgatado mais de um Objeto
            if(el.length > 1){
                // LOOP para verificar se deve ser setado o valor
                $(el).each(function(i,v){
                    // Setando o valor no Elemento
                    $(this).setValue(data[campo]);
                });
            } else{
                // Setando o valor no Elemento
                $(el).setValue(data[campo]);
            }
        
            if($(this)[0].id == "form_correspondencia" && (data["idstatus"] == 1 || data["idstatus"] == 3)){
                
                disableFieldsCorrespondencia(true,data["idstatus"]);
                page.changeSerialize = true;
            }else{
                disableFieldsCorrespondencia(false,data["idstatus"]);
                page.changeSerialize = false;
            }
        } // Fim da interação no Objeto com os dados

         
        return this;
    }
////////////////////////////////////////////////////////////////////////////////////

/////////////////SETA OS VALORES DO RETORNO DO JSON/////////////////////////////
    $.fn.setValue = function(value){
        // Campo "checkbox" ou "radio"
        if($(this).is('input[type=checkbox]') || $(this).is('input[type=radio]')){
            // Verificando se o valor do elemento é igual ao passado no Object
            if($(this).is('input[value='+ value +']')){
                $(this).attr('checked', true);
                $(this).parents().find("[class*=icheckbox]").addClass('checked');
                //$(this).parent().parent().find("[class*=icheckbox]").addClass('checked');
            }
        } else { // Demais campos
            $(this).val(value);
        }
 
        // Retornando o Elemento referênciado
        return this;
    }
    $.fn.serializeArray2 = function(){

        $values = $(this).serializeArray();
        $.each($(this).find("[disabled]"),function(index,el){

            b = {"name":el.name,"value":el.value};
            $values.push(b);
        });
    return $values;
    }
})(jQuery);
////////////////////////////////////////////////////////////////////////////////////
$(".main-navigation-menu li").on("click",function(){

    $(".main-navigation-menu li").removeClass("active");
    $(".main-navigation-menu li").removeClass("open");

    $(this).addClass("active");
    
});



