(function($){

	FormElements.init();
	UITreeview.init();
	
	$.jstree.defaults.checkbox.tie_selection = false;

    $(".search-select").on("change",function(e){        

    	$("#content_tree").html("<div id='tree_permissoes' class='tree-demo usuarios'></div>");

        UITreeview.reload(e.val);

        $("#tree_permissoes").on("changed.jstree",function(node,object,selected,event){
	        if(object.node){ 
	            params = {
	                idusuario:object.node.original.idusuario,
	                idmenu:object.node.original.idmenu,
	                instatus:object.node.state.selected?1:0,
	                idpermissaomenu:object.node.original.idpermissaomenu
	            }
	           $.ajax({
	                url:"actions/save.permissao_menu.php",
	                data:params,
	                method:"post",
	                dataType:"JSON",
	                success:function(json){
	                    toastr.success(json.msg,json.titlemsg);
	                }

	            });

        	}

    	});

    	$.getJSON("actions/list.menus.php", {
    		idusuario: e.val
    	}, function(data){
    		$("#menus").html("<option value=''></option>");
    		$("#menus").data("idmenu", e.val);
    		$(".options").addClass('hide');

    		$.each(data, function(a,b){
    			if(b.state.selected == true){
    				$("#menus").append("<option value='"+b.idmenu+"'>"+b.text+"</option>");
    			}
    		});
    	});

    	

    });

	$("#menus").on('change', function() {
		$(".options").removeClass('hide');
		$("#check1").prop('checked', false);
		$("#check2").prop('checked', false);
		$("#check3").prop('checked', false);
		$.getJSON("actions/list.menus.acoes.php", {
    		idusuario: $("#select-usuarios").val(),
    		idmenu: $(this).val()
    	}, function(data){
    		if(data.length > 0){
    			$.each(data, function(a,b){
    				if(b.idacao == 1) $("#check1").prop('checked', true);
    				if(b.idacao == 2) $("#check2").prop('checked', true);
    				if(b.idacao == 3) $("#check3").prop('checked', true);
    			})
    		}
    	});
	});

	$(".p2").on('click', 'input[name=desexcluir]', function(event) {
		var state = $(this).context.checked;
		var idmenu = $("#menus").val();
		var idusuario = $("#select-usuarios").val();
		var idacao = $(this).val();
		//
		$.post('actions/save.permissao.acao.php', {
			state: state,
			idmenu: idmenu,
			idusuario: idusuario,
			idacao: idacao
		}, function(data, textStatus, xhr) {
			
		});
	});

})(jQuery);