(function($){

        page.setDataTable();
        
    //////////////////////////RESET EVENT IN ELEMENT////////////////////////
        $(document).find("[data-target='.removedocumentotipo']").unbind('click');
        $("#form_documentotipo").unbind('submit');
    /////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
        $("#form_documentotipo").submit(function(event){
            event.preventDefault();

            page.save($("#form_documentotipo"),$(".documentotipo"),false,function(d){
                if (d) {
                    page.reload_content(
                        {
                            url_reload:"actions/get.documentotipos.load.php",
                            js:"/res/js/documentotipos/documentotipos.js",
                            params:''
                        }
                    );
                }
            });
        });

    ////////////////////////////PARAMETRIZACAO///////////////////////////////////////
        $(document).find("[data-target='.documentotipo']").on('click', function(){

            $(".documentotipo").find(".modal-title").html($(this).data("titlemodal"));
            $("#form_documentotipo").find("[name*=idcategoria]").val($(this).data("form-params1"));
            
            page.actions = {
                    "url_save":   $(this).data("form-url-save"),
                    "url_load":   $(this).data("form-url-load"),
                    "url_delete": $(this).data("form-url-delete"),
                    "form_params":{
                            "iddocumentotipo": $(this).data("form-params"),

                        }
            }
            if (page.actions.url_load != "" && page.actions.url_load != undefined){
                page.load(page.actions.form_params,("#form_documentotipo"));
            }

        });
    ////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////DELECAO/////////////////////////////////////////


        $(document).find("[data-target='.removedocumentotipo']").on('click', function(){

            page.actions = {
                    "url_delete":  $(this).data("form-url-delete"),
                    "form_params": {
                        "iddocumentotipo": $(this).data('form-params')
                    }
            }
            bootbox.confirm("<h1>Deseja excluir esse Tipo de Documento ?</h1>",function(result){
                if(result)page.delete(page.actions,function(d){
                    if (d) {
                            page.reload_content(
                            {
                                url_reload:"actions/get.documentotipos.load.php",
                                js:"/res/js/documentotipos/documentotipos.js",
                                params:''
                            }
                        );
                    }
                });
            });

        });


})(jQuery);