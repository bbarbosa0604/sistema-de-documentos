var Calendar = function () {
	"use strict";
	var dateToShow, calendar, demoCalendar, eventClass, eventCategory, subViewElement, subViewContent, $eventDetail;
	var defaultRange = new Object;
	defaultRange.start = moment();
	defaultRange.end = moment().add('days', 1);
	//Calendar
	var setFullCalendarEvents = function() {
		var date = new Date();
		dateToShow = date;
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

	};
    //function to initiate Full Calendar
    var runFullCalendar = function () {
    	$(".add-event").off().on("click", function() {
			subViewElement = $(this);
			subViewContent = subViewElement.attr('href');
    		$.subview({
					content : subViewContent,
					onShow : function() {
						editFullEvent();
					},
					onHide : function() {
						hideEditEvent();
					}
				});
    	});
    	
        $('#event-categories div.event-category').each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 50 //  original position after the drag
            });
        });
        /* initialize the calendar
		-----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var form = '';
        $('#full-calendar').fullCalendar({
            buttonText: {
                prev: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>'
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events:'actions/correspondencia_situacao_count.php',
            //events:'gestaodocumentos/test_documento_list.php',
            editable: true,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                
                var $categoryClass = $(this).attr('data-class');
                var $category = $categoryClass.replace("event-", "").toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
                // we need to copy it, so that multiple events don't have a reference to the same object

                
                
                var newEvent = new Object;
				newEvent.title = originalEventObject.title, newEvent.start = new Date(date), newEvent.end =  moment(new Date(date)).add('hours', 1), newEvent.allDay = true, newEvent.className = $categoryClass, newEvent.category = $category, newEvent.content = "";

                
                demoCalendar.push(newEvent);
                $('#full-calendar').fullCalendar( 'refetchEvents' );
               
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            selectable: false,
            selectHelper: false,
            select: function (start, end, allDay) {
            	defaultRange.start = moment(start);
				defaultRange.end = moment(start).add('hours', 1);
				$.subview({
					content : "#newFullEvent",
					onShow : function() {
						editFullEvent();
					},
					onHide : function() {
						hideEditEvent();
					}
				});
            },
            eventClick: function (calEvent, jsEvent, view) {
            	dateToShow = calEvent.start;

				$.subview({
					content : "#readFullEvent",
					startFrom : "right",
					onShow : function() {
						readFullEvents(calEvent._id,calEvent);
					}
				});
                
            }
        });
    };
   
    var readFullEvents = function(el,calEvent) {
    	
		$(".edit-event").off().on("click", function() {
			subViewElement = $(this);
			subViewContent = subViewElement.attr('href');
			$.subview({
				content : subViewContent,
				onShow : function() {
					editFullEvent(el);
				},
				onHide : function() {
					hideEditEvent();
				}
			});
		});


		//EFEITOS TRANSITION
		$("#sandbox_apllyEfects-recebidos").removeClass().
		addClass('fadeInLeftBig animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass();
		});


		$("#sandbox_apllyEfects-enviados").removeClass().
		addClass('fadeInRightBig animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass();
		});


		$("#sandbox_apllyEfects-internos").removeClass().
		addClass('fadeInDownBig animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$(this).removeClass();
		});

		$.ajax({
			url : 'actions/correspondencia_situacao_date_get.php',
			type:"POST",
			data:{"dt_search":$.fullCalendar.formatDate(calEvent.start,"yyyy-MM-dd")},
			dataType : 'json',
			success : function(json) {
				$("ul").find(".event-quantity").html("<h3>0</h3>");
				$("ul").find(".plan-name").html("<h3></h3>");
				$.each(json,function(obj,v,index){
					/*if(v.start == $.fullCalendar.formatDate(calEvent.start,"yyyy-MM-dd")){*/
						$(".pricing-table").attr("data-select",$.fullCalendar.formatDate(calEvent.start,"yyyy-MM-dd"));
						switch (parseInt(v.idsituacao)) {
							case 1:
								$("ul.enviados").find(".event-quantity").html("<h3>"+v.qtde+"</h3>");
								$("ul.enviados").find(".plan-name").html("<h3>"+v.category+"</h3>");
								break;
							case 2:
								$("ul.recebidos").find(".event-quantity").html("<h3>"+v.qtde+"</h3>")
								$("ul.recebidos").find(".plan-name").html("<h3>"+v.category+"</h3>");
								break;
							case 3:
								$("ul.internos").find(".event-quantity").html("<h3>"+v.qtde+"</h3>");
								$("ul.internos").find(".plan-name").html("<h3>"+v.category+"</h3>");
								break;
						}
					//}


				});

				$(".event-day").html("<h2>"+$.fullCalendar.formatDate(calEvent.start,"dd")+"</h2>");
				$(".event-date").html("<h3>"+$.fullCalendar.formatDate(calEvent.start,"dddd")+"</h3>"+
					$.fullCalendar.formatDate(calEvent.start,"MMMM yyyy"));
			}
		});

	};
	
	var runFullCalendarValidation = function(el) {
		
		var formEvent = $('.form-full-event');
		var errorHandler2 = $('.errorHandler', formEvent);
		var successHandler2 = $('.successHandler', formEvent);

		formEvent.validate({
			errorElement : "span", // contain the error msg in a span tag
			errorClass : 'help-block',
			errorPlacement : function(error, element) {// render error placement for each input type
				if (element.attr("type") == "radio" || element.attr("type") == "checkbox") {// for chosen elements, need to insert the error after the chosen container
					error.insertAfter($(element).closest('.form-group').children('div').children().last());
				} else if (element.parent().hasClass("input-icon")) {

					error.insertAfter($(element).parent());
				} else {
					error.insertAfter(element);
					// for other inputs, just perform default behavior
				}
			},
			ignore : "",
			rules : {
				eventName : {
					minlength : 2,
					required : true
				},
				eventStartDate : {
					required : true,
					date : true
				},
				eventEndDate : {
					required : true,
					date : true
				}
			},
			messages : {
				eventName : "* Please specify your first name"

			},
			invalidHandler : function(event, validator) {//display error alert on form submit
				successHandler2.hide();
				errorHandler2.show();
			},
			highlight : function(element) {
				$(element).closest('.help-block').removeClass('valid');
				// display OK icon
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
				// add the Bootstrap error class to the control group
			},
			unhighlight : function(element) {// revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error');
				// set error class to the control group
			},
			success : function(label, element) {
				label.addClass('help-block valid');
				// mark the current input as valid and display OK icon
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
			},
			submitHandler : function(form) {
				successHandler2.show();
				errorHandler2.hide();
				var newEvent = new Object;

				newEvent.title = $(".form-full-event .event-name ").val(), newEvent.start = new Date($('.form-full-event .event-start-date').val()), newEvent.end = new Date($('.form-full-event .event-end-date').val()), newEvent.allDay = $(".form-full-event .all-day").bootstrapSwitch('state'), newEvent.className = $(".form-full-event .event-categories option:checked").val(), newEvent.category = $(".form-full-event .event-categories option:checked").text(), newEvent.content = $eventDetail.code();

				$.blockUI({
					message : '<i class="fa fa-spinner fa-spin"></i> Do some ajax to sync with backend...'
				});
				
				if ($(".form-full-event .event-id").val() !== "") {
				el = $(".form-full-event .event-id").val();

					for ( var i = 0; i < demoCalendar.length; i++) {

						if (demoCalendar[i]._id == el) {
							newEvent._id = el;
							var eventIndex = i;
						}

					}
					//mockjax simulates an ajax call
					$.mockjax({
					url : '/event/edit/webservice',
					dataType : 'json',
					responseTime : 1000,
					responseText : {
						say : 'ok'
					}
				});

				$.ajax({
					url : '/event/edit/webservice',
					dataType : 'json',
					success : function(json) {
						$.unblockUI();
						if (json.say == "ok") {
							demoCalendar[eventIndex] = newEvent;
							$('#full-calendar').fullCalendar( 'refetchEvents' );
							$.hideSubview();
							toastr.success('The event has been successfully modified!');
						}
					}
				});

				} else {
				//mockjax simulates an ajax call
				$.mockjax({
					url : '/event/new/webservice',
					dataType : 'json',
					responseTime : 1000,
					responseText : {
						say : 'ok'
					}
				});

				$.ajax({
					url : '/event/new/webservice',
					dataType : 'json',
					success : function(json) {
						$.unblockUI();
						if (json.say == "ok") {
							demoCalendar.push(newEvent);
							$('#full-calendar').fullCalendar( 'refetchEvents' );
							$.hideSubview();
							toastr.success('A new event has been added to your calendar!');
						}
					}
				});
					
				}



			}
		});
	};
	// on hide event's form destroy summernote and bootstrapSwitch plugins
	var hideEditEvent = function() {
		$.hideSubview();
		$('.form-full-event .summernote').destroy();
		$(".form-full-event .all-day").bootstrapSwitch('destroy');
		
	};
    return {
        init: function () {
        	setFullCalendarEvents();
            runFullCalendar();
            runFullCalendarValidation();
        }
    };
}();