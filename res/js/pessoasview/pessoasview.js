(function($){

    $(document).find("[data-target='.remove-pessoa-vinculo']").unbind("click");
    $(document).find("[data-target='.pessoaview']").unbind("click");
    $(document).find("[data-target='.removeendereco']").unbind("click");
    $(document).find("[data-target='.pessoa_enderecos']").unbind("click");
    $(document).find("[data-target='.removecontato']").unbind("click");
    $(document).find("[data-target='.pessoa_contatos']").unbind("click");
    $(document).find("[data-target='.removedocumento']").unbind('click');
    $(document).find("[data-target='.pessoa_documentos']").unbind("click");
    $("#form_pessoa_dadospessoais_fisica").unbind("submit");
    $(document).find("[data-target='.pessoa_dadospessoais_fisica']").unbind("click");
    $("#form_pessoa_dadospessoais_juridica").unbind("submit");
    $("a[data-target*='.vincularpessoaempresa']").unbind("click");
    $("#table_data_vincularpessoaempresa a").unbind("click");
    $("a[data-target*='.removerpessoaempresa']").unbind("click");
    $("button[data-target*='.vincularpessoa']").unbind("click");
    $("#table_data_vincularpessoa a").unbind("click");
    $("a[data-target*='.relacionando_pessoas']").unbind("click");
    $("#table_data_relacionar_pessoas a").unbind("click");
    $(".dados_pessoas thead").find("[class*=dados_pessoas_subtitulo] a").unbind("click");
    $("#form_pessoa_contatos").unbind("submit");
    $("#form_pessoa_documentos").unbind("submit");
    $("#form_pessoa_endereco").unbind("submit");
    $("#descep").unbind("blur");
    $("[name*=idcontatotipo]").unbind("change");
	$("[name*=iddocumentotipo]").unbind("change");

    Main.init();
    FormElements.init();

    setTimeout(function(){

        //Carregando outros
        $.getJSON("actions/get.pessoas.outros.php", {
            idpessoa : $("input[name=idpessoa-outros]").val()
        } ,function(data){
            $.each(data, function(a, b){
                if(b.idcampotipo == 5){
                	$("select[name=campo-"+b.idcampo+"]").val(b.desvalue);
                	$("select[name=campo-"+b.idcampo+"]").attr("idpessoaoutros", b.idpessoaoutros);
                } else {
                	$("input[name=campo-"+b.idcampo+"]").val(b.desvalue);
                	$("input[name=campo-"+b.idcampo+"]").attr("idpessoaoutros", b.idpessoaoutros);
                }
                
            });
        });

    }, 500);

	var pessoasviews = {
		options_pessoas_views:{},

	    load_tbody_dados_pessoais:function(){

	        $("#tbody_dados_pessoais").load("actions/get.pessoas.dadospessoais.php",page.actions.form_params);
	        
	    },
	    load_tbody_pessoas_e_empresas_relacionadas:function(force_load){

	        //force_load = force_load || null;
	        //if(!$("#"+page.options_pessoas_views.id_element+"").children().length || force_load){}
	        $("#"+pessoasviews.options_pessoas_views.id_element+"").load(
	        pessoasviews.options_pessoas_views.url_load, pessoasviews.options_pessoas_views.form_params,function(){

	            $(document).find("[data-target='.remove-pessoa-vinculo']").on('click', function(){

	                page.actions = {
	                    url_save:$(this).data("form-url-remove"),
	                    form_params:{
	                        "idpessoa":$(this).data("form-params"),
	                        "idpessoapai":$(this).data("form-params-idpessoapai")
	                    }
	                }

	                bootbox.confirm("<h1>Deseja retirar o vinculo com essa pessoa ?</h1>",function(result){
	                    if(result){
	                    	$(".main-content").addClass("loadmask");
	                        page.save_no_modal();
	                        pessoasviews.load_tbody_pessoas_e_empresas_relacionadas();
	                    }

	                });

	            });

	            $('.tooltips').tooltip();
	            pessoasviews.loadEventPessoaView();
	        });
	    
	        
	    },
	    load_tbody_documentos:function(force_load){    
	        //force_load = force_load || null;

	        //if(!$("#"+page.options_pessoas_views.id_element+"").children().length || force_load){}
	        $("#"+pessoasviews.options_pessoas_views.id_element+"").load(
	        pessoasviews.options_pessoas_views.url_load, pessoasviews.options_pessoas_views.form_params,function(){



	            $('.tooltips').tooltip();
	            pessoasviews.loadEventDocumento();
	        });
	    },
	    load_tbody_contatos:function(force_load){
	    
	        //force_load = force_load || null;

	        //if(!$("#"+page.options_pessoas_views.id_element+"").children().length || force_load){}
	        $("#"+pessoasviews.options_pessoas_views.id_element+"").load(
	        pessoasviews.options_pessoas_views.url_load, pessoasviews.options_pessoas_views.form_params,function(){

	            pessoasviews.loadEventContato();
	            $('.tooltips').tooltip();

	        });
	    },
	    load_tbody_enderecos:function(force_load){
	    
	        //force_load = force_load || null;

	        //if(!$("#"+page.options_pessoas_views.id_element+"").children().length || force_load){}
	        $("#"+pessoasviews.options_pessoas_views.id_element+"").load(
	        pessoasviews.options_pessoas_views.url_load, pessoasviews.options_pessoas_views.form_params,function(){

	            pessoasviews.loadEventEndereco();

	            $('.tooltips').tooltip();
	            
	        });
	    },
	    loadEventPessoaView:function(){
		
			$(document).find("[data-target='.pessoaview']").on('click', function(e){

			    page.actions = {
			            "url_save":   $(this).data("form-url-save"),
			            "url_load":   $(this).data("form-url-load"),
			            "url_delete": $(this).data("form-url-delete"),
			            "form_params":{
			                    "idpessoa": $(this).data("form-params")
			                }
			    }
				
			    $(".main-content").addClass("loadmask");
			    $(".loadmask").block(page.configUiPage);

			    $.ajax({
			        url:$(this).data("url"),
			        data:page.actions.form_params,
			        dataType:"html",
			        method:"POST",
			        success:function(data){
			            $(".main-content").html(data);
			        },
			        error:function(){
			            toastr.error("Erro ao Carregar a Pagina");
			        },
			        complete:function(data){
			            $(".loadmask").unblock();
			            $(".main-content").removeClass("loadmask");
			        }

			    });
			    

			    if (page.actions.url_load != "" && page.actions.url_load != undefined){
			        page.load(page.actions.form_params,("#form_pessoa"));
			    }

			});
		},
		loadEventEndereco:function(){    

		    $(document).find("[data-target='.removeendereco']").on('click', function(){
		        page.actions = {
		                "url_delete":  $(this).data("form-url-delete"),
		                "form_params": {
		                    "idendereco": $(this).data('form-params')
		                }
		        }
		        bootbox.confirm("<h1>Deseja excluir esse Endereço ?</h1>",function(result){
		            if(result){
		            	page.delete(page.actions,function(){
		            		pessoasviews.load_tbody_enderecos();
		            	});
		            };
		        });

		    });


		    ////////////////////////////////PARAMETRIZACAO/////////////////////////////////
		    $(document).find("[data-target='.pessoa_enderecos']").on('click', function(){
		        
		        $(".pessoa_enderecos").find(".modal-title").html($(this).data("titlemodal"));

		        page.actions = {
		                "url_save":   $(this).data("form-url-save"),
		                "url_load":   $(this).data("form-url-load"),
		                "url_delete": $(this).data("form-url-delete"),
		                "form_params":{
		                        "idendereco": $(this).data("form-params"),
		                        "idpessoa": $(this).data("form-params-idpessoa"),
		                        


		                    }
		        }
		        if (page.actions.url_load != "" && page.actions.url_load != undefined){
		            page.load(page.actions.form_params,("#form_pessoa_endereco"));
		        }

		    });
		},
		loadEventContato:function(){
		    $(document).find("[data-target='.removecontato']").on('click', function(){
		        page.actions = {
		                "url_delete":  $(this).data("form-url-delete"),
		                "form_params": {
		                    "idcontato": $(this).data('form-params')
		                }
		        }
		        bootbox.confirm("<h1>Deseja excluir esse Contato ?</h1>",function(result){
		            if(result){
		            	page.delete(page.actions,function(){
		            		pessoasviews.load_tbody_contatos();
		            		
		            	});
		            }
		        });

		    });

		    ////////////////////////////////////PARAMETRIZACAO////////////////////////////

		    $(document).find("[data-target='.pessoa_contatos']").on('click', function(){

		    	$(this).closest("tr").data("mask")?
		    	page.setMask($(this).closest("tr").data("mask")):"";

		        $(".pessoa_contatos").find(".modal-title").html($(this).data("titlemodal"));
		        
		        page.actions = {
		                "url_save":   $(this).data("form-url-save"),
		                "url_load":   $(this).data("form-url-load"),
		                "url_delete": $(this).data("form-url-delete"),
		                "form_params":{
		                        "idcontato": $(this).data("form-params"),
								"idpessoa": $(this).data("form-params-idpessoa"),
		                    }
		        }
		        if (page.actions.url_load != "" && page.actions.url_load != undefined){
		            page.load(page.actions.form_params,("#form_pessoa_contatos"));
		        }

		    });
		},
		loadEventDocumento:function(){

		    $(document).find("[data-target='.removedocumento']").on('click', function(){

		        page.actions = {
		                "url_delete":  $(this).data("form-url-delete"),
		                "form_params": {
		                    "iddocumento": $(this).data('form-params')
		                }
		        }
		        bootbox.confirm("<h1>Deseja excluir esse Documento ?</h1>",function(result){
		            if(result){
		            	page.delete(page.actions,function(){
		            		pessoasviews.load_tbody_documentos();
		            		
		            	});

		            }
		        });

		    });


		    ////////////////////////////////////////////////////////////////////////////////////
		    //////////////////////////////////PARAMETRIZACAO///////////////////////////////////
		    $(document).find("[data-target='.pessoa_documentos']").on('click', function(){
		        
		    	$(this).closest("tr").data("mask")?
		    	page.setMask($(this).closest("tr").data("mask")):"";

		        $(".pessoa_documentos").find(".modal-title").html($(this).data("titlemodal"));

		        page.actions = {
		                "url_save":   $(this).data("form-url-save"),
		                "url_load":   $(this).data("form-url-load"),
		                "form_params":{
		                        "iddocumento": $(this).data("form-params"),
		                        "idpessoa":$(this).data("form-params-idpessoa")

		                    }
		        }
		        if (page.actions.url_load != "" && page.actions.url_load != undefined){
		            page.load(page.actions.form_params,("#form_pessoa_documentos"));
		        }

		    });
		    
		},
		buscaCep:function(t){
		    $("#msg_error_cep").removeClass("no-display").addClass("no-display");
		    $.ajax({
		      dataType: "json",
		      url: "http://viacep.com.br/ws/"+ t.val() + "/json",
		      success: function(json){
		        if (json.erro){
		            $("#msg_error_cep").html("Cep digitado não encontrado, para cadastrar o endereco, preencha todos os campos abaixo").removeClass("no-display");

		            $.each($("#form_pessoa_endereco").find("[disabled=true]"),function(a,b,c){
		                b.disabled = false;
		            });
		        }else{
		            $("[name = descel]").val(json.cep.replace(/[^\d]+/g,''));
		            $("[name = desendereco]").val(json.logradouro);
		            $("[name = descidade]").val(json.localidade);
		            $("[name = desbairro]").val(json.bairro);
		            $("[name = desendereco]").val(json.logradouro);
		            $("[name = idestado]").val(page.estados.estados.indexOf(json.uf) + 1);
		        }
		        
		      },
		        beforeSend:function(){
		            $(".loadmask").block(page.configUiNormal);

		        }
		    }).done(function(){
		        $(".loadmask").unblock();
		     });

		}


	} 

	$("#form_pessoa_dadospessoais_fisica").submit(function(event){
	    event.preventDefault();

	    page.save(
	        $("#form_pessoa_dadospessoais_fisica"),
	        $(".pessoa_dadospessoais_fisica"),
	        false
	    );
	    load_tbody_dados_pessoais();

	});
///////////////////////////////////////EDICAO//////////////////////////////////////////
	$(document).find("[data-target='.pessoa_dadospessoais_fisica']").on('click', function(){

	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "url_delete": $(this).data("form-url-delete"),
	            "form_params":{
	                    "idpessoa": $(this).data("form-params")
	                }
	    }
	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        page.load(page.actions.form_params,("#form_pessoa_dadospessoais_fisica"));
	    }

	    

	});
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////PESSOA JURIDICA//////////////////////////////
/////////////////////////////CRIACAO E EDICAO///////////////////////////////////
	$("#form_pessoa_dadospessoais_juridica").submit(function(event){
	    event.preventDefault();

	    /*
	    FUNCTION PARA SALVAR O FORMULARIO, PASSANDO COMO PARAMETRO
	     - o id do formulario=form_correspondencia
	     - o id do modal que tem o formulario = form_pessoas_dadospessoais_juridica

	    */

	    page.save(
	        $("#form_pessoa_dadospessoais_juridica"),
	        $(".pessoa_dadospessoais_juridica"),
	        false);
	    load_tbody_dados_pessoais();

	});

///////////////////////////////////PARAMETRIZACAO///////////////////////////////////////
	$(document).find("[data-target='.pessoa_dadospessoais_juridica']").on('click', function(){

	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "url_delete": $(this).data("form-url-delete"),
	            "form_params":{
	                    "idpessoa": $(this).data("form-params")
	                }
	    }
	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        page.load(page.actions.form_params,("#form_pessoa_dadospessoais_juridica"));
	    }

	});

	$("a[data-target*='.vincularpessoaempresa']").on("click", function(e) {
	    e.preventDefault();

	    var actionUrl = $(".vincularpessoaempresa").data("url");
	    var form_url_edit = $(this).data("form-url-edit");
	    var idpessoa = $(this).data("form-params");

        $.ajax({
            method:"POST",
            dataType:'json',
            data:{idpessoatipo:2},
            url:actionUrl,
            success: function(json){
                var innerHtml = "";                
                $(".modal.vincularpessoaempresa div.modal-body").html("");

                innerHtml +=
                    "<table class='table table-striped table-hover display' id='table_data_vincularpessoaempresa'>"+
                        "<thead>"+
                            "<tr>"+
                                "<th>#</th>"+
                                "<th>Nome</th>"+
                                "<th>Telefone</th>"+
                                "<th>E-mail</th>"+
                                "<th>Tipo</th>"+
                            "</tr>"+
                        "</thead>"+
                        "<tbody>";

                $.each(json,function(i,x){

                    innerHtml +=
                        "<tr>"+
                            "<td class='center'>"+
                                "<div class='visible-md visible-lg hidden-sm hidden-xs'>"+
                                    "<a href='javascript:void(0)' class='btn btn-xs btn-blue tooltips' data-idpessoa='"+x.idpessoa+"'"+
                                    "data-despessoa='"+x.despessoa+"'data-placement='top' data-original-title='Selecionar'>"+
                                        "<i class='fa fa-arrow-circle-left' data-size='m'></i>"+
                                    "</a>"+
                                "</div>"+
                            "</td>"+
                            "<td>"+x.despessoa+"</td>"+
                            "<td>"+x.descontato_telefone+
                            "</td><td>"+x.descontato_email+"</td>"+
                            "<td>"+x.despessoatipo+"</td>"+
                        "</tr>";

                });

                innerHtml+="</tbody></table>";
                $(".modal.vincularpessoaempresa div.modal-body").html(innerHtml);

            },
            beforeSend:function(){
                $(".loadmask_pessoavincula").block(page.configUi);

            }
        }).done(function(){

            $("#table_data_vincularpessoaempresa").DataTable({
                bDestroy:true,
                bRetriving:true,
                "lengthMenu": [[5, 10], [5, 10]]
            });

            $("#table_data_vincularpessoaempresa a").on('click',function(){

                page.actions = {
                    url_save:form_url_edit,
                    form_params:{
                        "idpessoa":idpessoa,
                        "idpessoapai":$(this).data("idpessoa")
                    }
                }

                page.save_no_modal();           
                var htmlItem =
                "<tr>"+
                    "<td>"+
                        "<h4>Nome:</h4>"+
                    "</td>"+
                    "<td>"+
                        "<h4>"+
                            "<a class='tooltips' data-target='.pessoaview' data-original-title='Clique para acessar o Cadastro' href='javascript:void(0)' data-url='pessoas.view.php' data-form-params='"+$(this).data("idpessoa")+"'><span class='text-bold'>"+$(this).data("despessoa")+"</span>"+
                            "</a>"+
                        "</h4>"+
                    "</td>"+
                    "<td>"+
                    "</td>"+
                    "<td>"+
                        "<h2>"+
                            "<a href='javascript:void(0)' class='tooltips' data-form-url-remove='actions/remove.pessoa.empresa.php' data-target='.removerpessoaempresa' data-form-params='"+idpessoa+"' data-form-params-idpessoapai='"+$(this).data("idpessoa")+"' data-original-title='Remover Empresa'><i class='fa fa-times remove-pessoa-vinculo-empresa'></i></a>"+
                        "</h2>"+
                    "</td>"+
                "</tr>";

                $("#tbody_empresa").html(htmlItem);
                $(".vincularpessoaempresa").modal('hide');
                $('.tooltips').tooltip();
                pessoasviews.loadEventPessoaView();  
            });

            $(".loadmask_pessoavincula").unblock();
        });
	});

	$("a[data-target*='.removerpessoaempresa']").on("click", function(e) {

	        page.actions = {
	            url_save:$(this).data("form-url-remove"),
	            form_params:{
	                "idpessoa":$(this).data("form-params"),
	                "idpessoapai":$(this).data("form-params-idpessoapai")
	            }
	        }

	        bootbox.confirm("<h1>Deseja remover essa Empresa ?</h1>",function(result){
	        if(result){
	            page.save_no_modal();

	            $("#tbody_empresa").html(""+
	                "<tr>"+
	                "<td></td>"+
	                "<td><h4><div class='text-red text-bold'>Nenhuma Empresa Cadastrada</div></h4><h4></h4></td>"+
	                "<td></td>"+
	                "<td></td>"+
	            "</tr>");
				pessoasviews.loadEventPessoaView();
	        }
	    });
	});

	/////////////////////////////VINCULAR PESSOA//////////////////////////////////
	$("button[data-target*='.vincularpessoa']").on("click", function(e) {
	        e.preventDefault();

	        var type = $(this).data("type");
	        if (type != undefined) $(".vincularpessoa h4").html("Vincular o " + type.toString());

	        var actionUrl = $(".vincularpessoa").data("url");

	        $.ajax({
	            method:"POST",
	            dataType:'json',
	            url:actionUrl,
	            success: function(json){
	                var innerHtml = "";                
	                $(".modal.vincularpessoa div.modal-body").html("");

	                innerHtml +=
	                    "<table class='table table-striped table-hover display' id='table_data_vincularpessoa'>"+
	                        "<thead>"+
	                            "<tr>"+
	                                "<th>#</th>"+
	                                "<th>Nome</th>"+
	                                "<th>Telefone</th>"+
	                                "<th>E-mail</th>"+
	                                "<th>Tipo</th>"+
	                            "</tr>"+
	                        "</thead>"+
	                        "<tbody>";

	                $.each(json,function(i,x){
	                    innerHtml +=
	                        "<tr>"+
	                            "<td class='center'>"+
	                                "<div class='visible-md visible-lg hidden-sm hidden-xs'>"+
	                                    "<a href='#' class='btn btn-xs btn-blue tooltips' data-idpessoa='"+x.idpessoa+"'"+
	                                    "data-despessoa='"+x.despessoa+"'data-placement='top' data-original-title='Selecionar'>"+
	                                        "<i class='fa fa-arrow-circle-left' data-size='m'></i>"+
	                                    "</a>"+
	                                "</div>"+
	                            "</td>"+
	                            "<td>"+x.despessoa+"</td>"+
	                            "<td>"+x.descontato_telefone+
	                            "</td><td>"+x.descontato_email+"</td>"+
	                            "<td>"+x.despessoatipo+"</td>"+
	                        "</tr>";

	                });

	                innerHtml+="</tbody></table>";
	                $(".modal.vincularpessoa div.modal-body").html(innerHtml);

	            },
	            beforeSend:function(){
	                $(".loadmask_pessoavincula").block(page.configUi);
	            }
	            }).done(function(){
	                $("#table_data_vincularpessoa").DataTable({
	                    "bDestroy":true,
	                    "bRetriving":true,
	                    "lengthMenu": [[5, 10], [5, 10]],
	                });
	                if(type =="Remetente"){
	                    $("#table_data_vincularpessoa a").on('click',function(){
	                        $("[name = idpessoaremetente].setup").val($(this).data("idpessoa"));
	                        $("[name = despessoa_remetente].setup").val($(this).data("despessoa"));

	                        $(".vincularpessoa").modal('hide');    
	                    }); 

	                }else{

	                    $("#table_data_vincularpessoa a").on('click',function(){
	                        $("[name = idpessoadestinatario].setup").val($(this).data("idpessoa"));
	                        $("[name = despessoa_destinatario].setup").val($(this).data("despessoa"));

	                        $(".vincularpessoa").modal('hide');                     
	                    });
	                }
	                $(".loadmask_pessoavincula").unblock();
	            });

	});

	$("a[data-target*='.relacionando_pessoas']").on("click", function(e) {
	    e.preventDefault();

	        var idpessoa = $(this).data("form-params");
	        var actionUrl = $(this).data("form-url-load");
	        var form_url_edit = $(this).data("form-url-edit");
	        var form_url_remove = $(this).data("form-url-remove");


	       $.ajax({
	            method:"POST",
	            dataType:'json',
	            url:actionUrl,
	            success: function(json){
	                var innerHtml = "";                
	                $(".modal.relacionando_pessoas div.modal-body").html("");

	                innerHtml +=
	                    "<table class='table table-striped table-hover display' id='table_data_relacionar_pessoas'>"+
	                        "<thead>"+
	                            "<tr>"+
	                                "<th>#</th>"+
	                                "<th>Nome</th>"+
	                                "<th>Telefone</th>"+
	                                "<th>E-mail</th>"+
	                                "<th>Tipo</th>"+
	                            "</tr>"+
	                        "</thead>"+
	                        "<tbody>";

	                $.each(json,function(i,x){

	                    innerHtml +=
	                        "<tr>"+
	                            "<td class='center'>"+
	                                "<div class='visible-md visible-lg hidden-sm hidden-xs'>"+
	                                    "<a href='javascript:void(0)' class='btn btn-xs btn-blue tooltips' data-idpessoa='"+x.idpessoa+"'"+
	                                    "data-despessoa='"+x.despessoa+"'data-placement='top' data-original-title='Selecionar'>"+
	                                        "<i class='fa fa-arrow-circle-left' data-size='m'></i>"+
	                                    "</a>"+
	                                "</div>"+
	                            "</td>"+
	                            "<td>"+x.despessoa+"</td>"+
	                            "<td>"+x.descontato_telefone+
	                            "</td><td>"+x.descontato_email+"</td>"+
	                            "<td>"+x.despessoatipo+"</td>"+
	                        "</tr>";

	                });

	                innerHtml+="</tbody></table>";
	                $(".modal.relacionando_pessoas div.modal-body").html(innerHtml);
	            },
	            beforeSend:function(){
	                $(".loadmask_pessoavincula").block(page.configUi);

	            }
	            }).done(function(){

	                $("#table_data_relacionar_pessoas a").on("click", function(){

	                    page.actions = {
	                        url_save:form_url_edit,
	                        form_params:{
	                            "idpessoa":idpessoa,
	                            "idpessoapai":$(this).data("idpessoa")
	                        }
	                    }
	                    page.save_no_modal();           

	                    var htmlItem ="";

	                    pessoasviews.load_tbody_pessoas_e_empresas_relacionadas();
	                    $(".relacionando_pessoas").modal('hide');
	                    $('.tooltips').tooltip();
	                    pessoasviews.loadEventPessoaView();  
	                });

	                $("#table_data_relacionar_pessoas").DataTable({
	                    bDestroy:true,
	                    bRetriving:true,
	                    "lengthMenu": [[5, 10], [5, 10]]
	                });


	                $(".loadmask_pessoavincula").unblock();

	        });


	});

    $(".dados_pessoas thead").find("[class*=dados_pessoas_subtitulo] a").on("click",function(event,index){

       var array_tbody_pessoas = [
            "tbody_dados_pessoais","tbody_pessoas_e_empresas_relacionadas",
            "tbody_documentos","tbody_contatos","tbody_enderecos"
        ];

        var name_id = $(this).find("span").data("tbody") - 1;

        $(this).find("span").is(".fa-minus")?
        $(this).find("span").removeClass().addClass("fa fa-plus"):
        $(this).find("span").removeClass().addClass("fa fa-minus");

        $("#"+array_tbody_pessoas[name_id]+"").toggleClass(function(){
            if(!$(this).is("no-display")){

                pessoasviews.options_pessoas_views = {
                    "form_params":{idpessoa:$(this).data('form-params')},
                    "url_load":$(this).data("form-url-load"),
                    "tipo":'html',
                    "id_element":array_tbody_pessoas[name_id]
                };
                //func = new Function("load_"+array_tbody_pessoas[name_id]+"();");
                //func();
                eval("pessoasviews.load_"+array_tbody_pessoas[name_id]+"();");
                return "no-display";
            }
        });
       
    });

	$("#form_pessoa_contatos").submit(function(event){
	    event.preventDefault();


	    /*
	    FUNCTION PARA SALVAR O FORMULARIO, PASSANDO COMO PARAMETRO
	     - o id do formulario=form_correspondencia
	     - o id do modal que tem o formulario = contatos

	    */
	    $(this).find("[name*=idpessoa]").val(page.actions.form_params.idpessoa);
	    page.save($("#form_pessoa_contatos"),$(".pessoa_contatos"),false,function(){
	    	pessoasviews.load_tbody_contatos();
	    });

	});

	$("#form_pessoa_documentos").submit(function(event){
	    event.preventDefault();

	    $(this).find("[name*=idpessoa]").val(page.actions.form_params.idpessoa);
	    page.save($("#form_pessoa_documentos"),$(".pessoa_documentos"),false,function(){
	    	pessoasviews.load_tbody_documentos();	    	
	    });


	});

	$("#form_pessoa_endereco").submit(function(event){
	    event.preventDefault();

	    page.changeSerialize = true;
		$(this).find("[name*=idpessoa]").val(page.actions.form_params.idpessoa);
	    page.save($("#form_pessoa_endereco"),$(".pessoa_enderecos"),false,function(){
	    	pessoasviews.load_tbody_enderecos();
	    	
	    });

	});

	pessoasviews.loadEventPessoaView();	
	pessoasviews.loadEventContato();
	pessoasviews.loadEventDocumento();
	pessoasviews.loadEventEndereco();


    $("#descep").blur(function(){        
        if($(this).val().replace(/[^\d]+/g,'').length == 8){
            pessoasviews.buscaCep($(this));
        }else{
            $("#msg_error_cep").html("Cep Inválido, o cep deve conveter 8 caracteres").removeClass("no-display");
        }
    });


	$(".cep, .CEP").mask("99999999");
    $(".cpf, .CPF").mask("99999999999");
    $(".cnpj, .CNPJ").mask("99999999999999");
    $(".Telefone, .Celular, .Fax").mask("9999999999?9");




    $("[name*=idcontatotipo]").on("change",function(){
    	$("[name*=descontato]").removeClass().addClass("form-control").addClass(($(this)[0].selectedOptions[0].innerText));
			page.setMask(($(this)[0].selectedOptions[0].innerText));
    });
  

    $("[name*=iddocumentotipo]").on("change",function(){
        $("[name*=desdocumento]").removeClass().addClass("form-control").addClass(($(this)[0].selectedOptions[0].innerText));
        	page.setMask(($(this)[0].selectedOptions[0].innerText));
    });

    $("#btn-modal-outros").on('click', function() {
    	$("#modal-outros").modal("show");
    });

    $(".btn-save-outros").on('click', function() {
    	if($("#form_outros input").length > 0){
            
            $.each($("#form_outros input"), function(a, b){
                
                var campo = {};

                campo.idcampo = $(b).attr("name").replace("campo-","");
                campo.desvalue = $(b).val();
                campo.valid = $(b).val() == "" ? false : true;
                campo.idpessoaoutros = $(b).attr("idpessoaoutros");
                campo.idpessoa = $("input[name=idpessoa]")[2].value;

                if(campo.valid){
                    $.post("actions/add.pessoa.outros.php", campo , function( data ) {
                        var jsonReturn = $.parseJSON(data);
                        if(jsonReturn.success === false){
                            alert(data.msg);
                        }
                    });
                }
            });

            $.each($("#form_outros select"), function(a, b){
                
                var campo = {};

                campo.idcampo = $(b).attr("name").replace("campo-","");
                campo.desvalue = $(b).val();
                campo.valid = $(b).val() == "" ? false : true;
                campo.idpessoaoutros = $(b).attr("idpessoaoutros");
                campo.idpessoa = $("input[name=idpessoa]")[2].value;

                if(campo.valid){
                    $.post("actions/add.pessoa.outros.php", campo , function( data ) {
                        var jsonReturn = $.parseJSON(data);
                        if(jsonReturn.success === false){
                            alert(data.msg);
                        }
                    });
                }
            });
        }    	
    });

})(jQuery);
