(function($){

	Main.init();
	FormElements.init();
	page.setDataTable();

    ///////////////////////////////////////DISABLE EVENT ELEMENT///////////////////////////////////////////
    $(".send-email").unbind("click");
    $("#form_send_email").unbind("submit");
    $("#tab_correspondencia_historicos .addhistorico").unbind("click");
    $("li.tab_correspondencia_historicos a").unbind("click");
    //$("#table_data_vincularpessoa a").unbind("click");
    $("a.arquivos-remove").unbind("click");
    $("a.arquivos-download").unbind("click");
    $("button[data-target*='.vincularpessoa']").unbind("click");
    $(document).find("[data-target='.removecorrespondencia']").unbind("click");
    $(document).find("[data-target='.correspondencia']").unbind("click");
    $("#form_correspondencia").unbind("submit");
    $(document).find("[data-target='.uploadfile']").unbind("click");
    $("#form-fileupload").unbind("submit");
    $("li.tab_correspondencia_arquivos a").unbind("click");

    //////////////////////////////////////////////////////////////////////////////////////////////////////

	var arquivos = {
        obj:{
            idarquivo:"",
            desarquivo:"",
            descaminho:"",
            nrtamanho:"",
            idcorrespondencia:""
        },
        fileid:[],
        fn_createIndexFile:function(el){           
            var fileid = this.fileid;
            $.each(el,function(index,element){
                var value;
                if($(element).data("checkboxValue")!=undefined){
                	value = $(element).data("checkboxValue");
                    if(element.checked){
                        if(fileid.indexOf(value)==-1)fileid.push(value);
                    }else{
                        if(fileid.indexOf(value)!=-1)fileid.splice(fileid.indexOf(value),1);

                    }
                }

            });

            return fileid;
        },
        load:function(){
            $.ajax({
                url:"actions/list.arquivos.idcorrespondencia.php",
                data:this.obj,
                dataType:'json',
                method:"POST",
                success:function(json){
                    var content="";
                    $.each(json,function(key,value,c){
                        content +=''+
                            '<tr data-idarquivo="'+value.idarquivo+'">'+
                                '<td class="left hidden-xs">'+
                                    '<div class="checkbox-table">'+
                                        '<label>'+
                                            '<input type="checkbox" data-checkbox-value='+value.idarquivo+
                                            ' class="flat-grey foocheck">'+
                                        '</label>'+
                                    '</div>'+
                                '</td>'+
                                '<td>'+value.desarquivo+'</td>'+
                                '<td>'+arquivos.formatFileSize(parseInt(value.nrtamanho))+'</td>'+
                                '<td>'+
                                    '<div class="btn-group">'+
                                        '<button type="button" class="btn btn-azure dropdown-toggle" data-toggle="dropdown">'+
                                            'Acões '+
                                            '<span class="caret"></span>'+
                                        '</button>'+
                                        '<ul class="dropdown-menu pull-right" role="menu">'+
                                            '<li>'+
                                                '<a class="arquivos-download" href="#">'+
                                                    '<i class="fa fa-download"></i> Download'+
                                                '</a>'+
                                            '</li>'+
                                            '<li>'+
                                                '<a class="arquivos-remove" href="#">'+
                                                    '<i class="fa fa-trash-o"></i> Excluir'+
                                                '</a>'+
                                            '</li>'+
                                        '</ul>'+
                                    '</div>'+
                                '<td>'+
                                    '<div class="btn-group"></div>'+
                                '</td>'+
                            '</tr>';
                    });

                    $("#tab_correspondencia_arquivos tbody").html(content);

                        $("a.arquivos-remove").on("click",function(event){
                            event.preventDefault();
                            var $this = $(this);
                            arquivos.obj.idarquivo = $(this).parents("tr").data("idarquivo");
			                arquivos.remove(function(){
			                	arquivos.removeArquivoList($this);
			                });
                        });

                        $("a.arquivos-download").on("click",function(event){
                            event.preventDefault();
                            arquivos.obj.idarquivo = $(this).parents("tr").data("idarquivo");
                            arquivos.download();
                        });

                },
                beforeSend:function(){
                     $(".loadmask").block(page.configUiNormal);
                }
            }).done(function(){
                $(".loadmask").unblock();
                Main.init();

                $('#tab_correspondencia_arquivos input[type="checkbox"]').on('ifToggled', function(event,v){
                    arquivos.fn_createIndexFile($(event.currentTarget));
                });
            });
        },
        addArquivoList:function(value){
            var content = ''+
                '<tr data-idarquivo="'+value.idarquivo+'">'+
                    '<td class="left hidden-xs">'+
                        '<div class="checkbox-table">'+
                            '<label>'+
                                '<input type="checkbox" data-checkbox-value='+value.idarquivo+
                                ' class="flat-grey foocheck">'+
                            '</label>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+value.desarquivo+'</td>'+
                    '<td>'+arquivos.formatFileSize(parseInt(value.nrtamanho))+'</td>'+
                    '<td>'+
                        '<div class="btn-group">'+
                            '<button type="button" class="btn btn-azure dropdown-toggle" data-toggle="dropdown"> Acões'+
                                '<span class="caret"></span>'+
                            '</button>'+
                            '<ul class="dropdown-menu pull-right" role="menu">'+
                                '<li>'+
                                    '<a class="arquivos-download" href="#">'+
                                        '<i class="fa fa-download"></i> Download'+
                                    '</a>'+
                                '</li>'+
                                '<li>'+
                                    '<a class="arquivos-remove" href="#">'+
                                        '<i class="fa fa-trash-o"></i> Excluir'+
                                    '</a>'+
                                '</li>'+
                            '</ul>'+
                        '</div>'+
                    '<td>'+
                        '<div class="btn-group"></div>'+
                    '</td>'+
                '</tr>';

            $("#tab_correspondencia_arquivos tbody").append(content);

            $("#tab_correspondencia_arquivos tr:last-child() a.arquivos-download").on("click",function(){
                event.preventDefault();
                arquivos.obj.idarquivo = $(this).parents("tr").data("idarquivo");
                arquivos.download();
            });


            $("#tab_correspondencia_arquivos tr:last-child() a.arquivos-remove").on("click",function(){
                event.preventDefault();
                var $this = $(this);
                arquivos.obj.idarquivo = $this.parents("tr").data("idarquivo");
                arquivos.remove(function(){
                	arquivos.removeArquivoList($this);
                });
                
            });

            Main.init();
            $('#tab_correspondencia_arquivos input[type="checkbox"]').on('ifToggled', function(event,v){
                arquivos.fn_createIndexFile($(event.currentTarget));
            });

        },
        removeArquivoList:function(x){
            $(x).parents("tr").remove();

        },
        save:function(){
            $.ajax({
                beforeSend:function(){
                    $(".loadmask").block(page.configUiNormal);
                }
            }).done(function(){
                $(".loadmask").unblock();
            });
        },
        remove:function(callback){
            $.ajax({
                url:"actions/remove.arquivo.php",
                data:this.obj,
                dataType:'json',
                method:"POST",
                success:function(json){
                    if(json.success){
                        toastr.success(json.msg,json.titlemsg);
                        callback();

                    }else{
                        toastr.error(json.msg,json.titlemsg);
                    }
                },
                beforeSend:function(){
                    $(".loadmask").block(page.configUiNormal);
                }
            }).done(function(){
                $(".loadmask").unblock();
            });
            
        },
        download:function(){
			$.ajax({
				url:"actions/check.arquivo.php",
				data:this.obj,
				method:"POST",
				dataType:"json",
				success:function(json){
				    if(json.success){
				        toastr.success(json.msg,json.titlemsg);
				        window.open("actions/download.arquivo.php?idarquivo="+arquivos.obj.idarquivo+"");
				    }else{
				        toastr.error(json.msg,json.titlemsg);
				    }			    
				},
				beforeSend:function(){
				    $(".loadmask").block(page.configUiNormal);
				}

			}).done(function(){
				$(".loadmask").unblock();
			});  
        },
        upload:function(form){
	        var data = new FormData();
	        $.each($(form).find("[type*=file]")[0].files, function(i, file) {
	            data.append(i, file);
                data.append(i,$("#form-fileupload").serializeArray()[0].value);
	        });

	        $.ajax({  
	            type: "POST", 
	            contentType:false,
	            cache: false,
	            processData: false,
	            dataType:"json",
	            url: "actions/send.fileupload.php",  
	            data: data,
	            xhr:function(){
	                var myXhr = $.ajaxSettings.xhr();
	                $('.progress-bar').css("width","0%");
	                if(myXhr.upload){ // Check if upload property exists
	                    myXhr.upload.addEventListener('progress',arquivos.progressHandlingFunction, false);
	                }
	                return myXhr;
	            },  
	            success: function(json){
	                if(json.success){
	                    toastr.info(json.msg);                    
	                    arquivos.addArquivoList(json);
	                    setTimeout(function(){
	                        $(".uploadfile").modal("hide");
	                    },2000);
	                }else{                    
	                    toastr.error(json.msg);
	                    setTimeout(function(){
	                        $('.progress-bar').css("width","0%");
	                    },1500);
	                }

	                //$(".files").html("<td>" + data.desarquivo + "</td><td>" + data.sizeformated + "</td><td>" + data.type + "</td>");
	            },
	            beforeSend:function(){
	                $(form).find("button[type*='submit']").prop("disabled",true);
	            },
	            complete:function(){
	                $(form).find("button[type*='submit']").prop("disabled",false);
	            }

	        });  
              
        },
        progressHandlingFunction:function(e){
	        if(e.lengthComputable){
	            percent = parseFloat((e.loaded * 100 ) / e.total)+"%";
	            $('.progress-bar').css("width",percent);
	            $('.progress-bar').html(percent==100?"Completo!":percent);
	        }
    	},
	   formatFileSize:function(bytes) {
            if (typeof bytes !== 'number') {
                return '';
            }
            if (bytes >= 1000000000) {
                return (bytes / 1000000000).toFixed(2) + ' GB';
            }
            if (bytes >= 1000000) {
                return (bytes / 1000000).toFixed(2) + ' MB';
            }
            return (bytes / 1000).toFixed(2) + ' KB';
        }

    },
    historico = {
        obj:{
            idcorrespondenciahistorico:"",
            descorrespondenciahistorico:"",
            idcorrespondencia:"",
            idusuario:"",
            dtcadastro:""
        },
        save:function(){
            $.ajax({
                url:"actions/add.correspondenciahistorico.php",
                data:this.obj,
                dataType:'json',
                method:"POST",
                success:function(json){
                    var value = json.result;
                    historico.addhistorico(value);
                    toastr.info(json.msg,json.titlemsg);

                    $(".correspondencia-historico-input textarea").val("");
                },
                beforeSend:function(){
                    $(".loadmask").block(page.configUiNormal);
                }
            }).done(function(){
                $(".loadmask").unblock();
            });
        },
        addhistorico:function(obj){
            content ='<div class="item-historico"><div class="item-historico-date"><div><div class="inline-block"><span class="day text-bold">'+obj.day+'</span></div><div class="inline-block" style="margin-left: 10px;"><span class="block week-day text-extra-large"> '+ page.week_day[obj.idday_weak] +' </span><span class="block month text-large text-red"> '+page.month[obj.idmonth - 1]+' '+obj.year+'</span></div><div class="inline-block pull-right"><span class="block time bold-text text-small"><i class="fa fa-clock-o"></i> '+obj.hour+'</span></div><div class="inline-block pull-right"><span class="block user text-blue bold-text text-small"><i class="fa fa-user"></i> '+obj.deslogin+' </span></div></div></div><div class="content-historico" style="color:#7f8c97"><p>'+obj.descorrespondenciahistorico+'</p></div></div>';

                $(".correspondencia_historico").prepend(content);

        },
        load:function(){
            $.ajax({
                url:"actions/list.correspondenciahistorico.php",
                data:this.obj,
                dataType:'json',
                method:"POST",
                success:function(json){
                    var content="";
                    $.each(json,function(key,value,c){
                        content+=
                            '<div class="item-historico">'+
                                '<div class="item-historico-date">'+
                                    '<div class="inline-block">'+
                                        '<span class="day text-bold">'+value.day+'</span>'+
                                    '</div>'+
                                    '<div class="inline-block" style="margin-left: 10px;">'+
                                        '<span class="block week-day text-extra-large"> '+ page.week_day[value.idday_weak] +
                                        '</span>'+
                                        '<span class="block month text-large text-red"> '+page.month[value.idmonth - 1]+' '+value.year+
                                        '</span>'+
                                    '</div>'+
                                    '<div class="inline-block pull-right">'+
                                        '<span class="block time bold-text text-small">'+
                                            '<i class="fa fa-clock-o"></i> '+value.hour+
                                        '</span>'+
                                    '</div>'+
                                    '<div class="inline-block pull-right">'+
                                        '<span class="block user text-blue bold-text text-small">'+
                                            '<i class="fa fa-user"></i> '+value.deslogin+
                                        ' </span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="content-historico" style="color:#7f8c97">'+
                                    '<p>'+value.descorrespondenciahistorico+'</p>'+
                                '</div>'+
                            '</div>';
                    
                    });
                    $(".correspondencia_historico").html(content);
                },
                beforeSend:function(){
                    $(".loadmask").block(page.configUiNormal);
                }
            }).done(function(){
                $(".loadmask").unblock();
            });     
        }
    },       
    email = {
        sendemail:function(form,params){
            $.ajax({
                url:"actions/send.email.php",
                type:"POST",
                dataType:"JSON",
                data:params,
                success:function(json){
                    if(json.success){                        
                        toastr.success(json.msg,json.msgtitle);
                        form[0].reset();

                    }else{

                        toastr.error(json.msg,json.msgtitle);
                    }
                },
                beforeSend:function(){
                    $(".loadmask").block(page.configUiNormal);
                }
            }).done(function(){
                $(".loadmask").unblock();
            });
        }
    }
    /////////////////////////CORRESPONDENCIA ARQUIVOS//////////////////////////////////////////////
    if($("[type*=file]").length > 0){

        $("[type*=file]")[0].onchange = function(){
            arquivo = $('[type*=file]')[0].files;
            file = arquivo[0];
            $('.files .name').html(file.name);
            $('.files .size').html(arquivos.formatFileSize(file.size));
            $('#show_upload_file').removeClass('no-display');
    	}
	}

    //$("#form-fileupload").onReset = function(){};

    $("li.tab_correspondencia_arquivos a").on("click",function(){

        if(arquivos.obj.idcorrespondencia == page.actions.form_params.idcorrespondencia){
            if(!$("#tab_correspondencia_arquivos table > tbody").children().length > 0 && page.actions.form_params.idcorrespondencia !='undefined'){
                arquivos.obj.idcorrespondencia = page.actions.form_params.idcorrespondencia;
                arquivos.load();
            }
        }else{
            arquivos.obj.idcorrespondencia = page.actions.form_params.idcorrespondencia;
            arquivos.load();
        }

    });

    $(document).find("[data-target='.uploadfile']").on('click', function(event){
        $(".uploadfile form").find("[name*=idcorrespondencia]").val(page.actions.form_params.idcorrespondencia);
       
    }); 

    $("#form-fileupload").submit(function(event) {
        event.preventDefault();

        if($("[type*=file]")[0].files.length > 0){
            arquivos.upload($(this));
        }
        
        $("#form-fileupload")[0].reset();

        return false;   
    });
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////CORRESPONDENCIA////////////////////////////////////////

    /////////////////////////////CRIACAO E EDICAO///////////////////////////////////

	$("#form_correspondencia").submit(function(event){
	    event.preventDefault();
        //Salvando Campo outros
        if($("#form_outros input").length > 0){
            
            $.each($("#form_outros input"), function(a, b){
                
                var campo = {};

                campo.idcorrespondenciaoutros = 0;
                campo.idcampo = $(b).attr("name").replace("campo-","");
                campo.desvalue = $(b).val();
                campo.valid = $(b).val() == "" ? false : true;
                campo.idcorrespondenciaoutros = $(b).attr("idcorrespondenciaoutros");
                campo.idcorrespondencia = $("input[name=idcorrespondencia]")[2].value;

                if(campo.valid){
                    $.post("actions/add.correpondencia.outros.php", campo , function( data ) {
                        var jsonReturn = $.parseJSON(data);
                        if(jsonReturn.success === false){
                            alert(data.msg);
                        }
                    });
                }
            });

            $.each($("#form_outros select"), function(a, b){
                
                var campo = {};

                campo.idcorrespondenciaoutros = 0;
                campo.idcampo = $(b).attr("name").replace("campo-","");
                campo.desvalue = $(b).val();
                campo.valid = $(b).val() == "" ? false : true;
                campo.idcorrespondenciaoutros = $(b).attr("idcorrespondenciaoutros");
                campo.idcorrespondencia = $("input[name=idcorrespondencia]")[2].value;

                if(campo.valid){
                    $.post("actions/add.correpondencia.outros.php", campo , function( data ) {
                        var jsonReturn = $.parseJSON(data);
                        if(jsonReturn.success === false){
                            alert(data.msg);
                        }
                    });
                }
            });
        }
        // -------------------------------------------------------------------------------------- //
        page.save($("#form_correspondencia"),$(".correspondencia"),false,function(d){    
            if(d){
                page.reload_content(
                    {
                        url_reload:"actions/get.correspondencias.load.php",
                        js:"/res/js/correspondencia/correspondencias.js",
                        params:''
                    }
                );

                
            }
        });


	});
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////PARAMETRIZACAO/////////////////////////////////////
	$(document).find("[data-target='.correspondencia']").on('click', function(){

        console.log("uhuu");

	    $(".correspondencia").find(".modal-title").html($(this).data("titlemodal"));

        if($(this).data("enable-all-tabs")){
            $("#tab_correspondencia").find(".disabledTab").removeClass("disabledTab");
        }else{
            $("#tab_correspondencia li:not(:first-child)").addClass("disabledTab");
        }

	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "url_delete": $(this).data("form-url-delete"),
	            "form_params":{
	                    "idcorrespondencia": $(this).data("form-params")
	                }
	    }
	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        var a = page.load(page.actions.form_params,("#form_correspondencia"));
	    }

        setTimeout(function(){

            //Carregando outros
            $.getJSON("actions/get.correspondencia.outros.php", {
                idcorrespondencia : $("input[name=idcorrespondencia]")[2].value
            } ,function(data){
                $.each(data, function(a, b){
                    if(b.idcampotipo==5){
                        $("select[name=campo-"+b.idcampo+"]").val(b.desvalue);
                        $("select[name=campo-"+b.idcampo+"]").attr("idcorrespondenciaoutros", b.idcorrespondenciaoutros);
                    } else {
                        $("input[name=campo-"+b.idcampo+"]").val(b.desvalue);
                        $("input[name=campo-"+b.idcampo+"]").attr("idcorrespondenciaoutros", b.idcorrespondenciaoutros);
                    }
                    
                });
            });

        }, 500);

	});
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////DELEÇÃO//////////////////////////////////////////////

	$(document).find("[data-target='.removecorrespondencia']").on('click', function(event){
	    event.preventDefault();

	    page.actions = {
	            "url_delete":  $(this).data("form-url-delete"),
	            "form_params": {
	                "idcorrespondencia": $(this).data('form-params'),
	                "idcorrespondenciadocumento": $(this).data('form-params1')
	            }
	    }
	    bootbox.confirm("<h1>Deseja excluir a correspondencia?</h1>",function(result){
	        if(result)page.delete(page.actions,function(d){
                if(d){
                    page.reload_content(
                        {
                            url_reload:"actions/get.correspondencias.load.php",
                            js:"/res/js/correspondencia/correspondencias.js",
                            params:''
                        }
                    );
                }

            });
	    });

	});
	/////////////////////////////VINCULAR PESSOA//////////////////////////////////////////
	$("button[data-target*='.vincularpessoa']").on("click", function(e){
	    e.preventDefault();

	    var type = $(this).data("type");
	    if (type != undefined) $(".vincularpessoa h4").html("Vincular o " + type.toString());

	    var actionUrl = $(".vincularpessoa").data("url");

	    $.ajax({
	        method:"POST",
	        dataType:'json',
	        url:actionUrl,
	        success: function(json){
	            var innerHtml = "";                
	            $(".modal.vincularpessoa div.modal-body").html("");

	            innerHtml +=
	                "<table class='table table-striped table-hover display' id='table_data_vincularpessoa'>"+
	                    "<thead>"+
	                        "<tr>"+
	                            "<th>#</th>"+
	                            "<th>Nome</th>"+
	                            "<th>Telefone</th>"+
	                            "<th>E-mail</th>"+
	                            "<th>Tipo</th>"+
	                        "</tr>"+
	                    "</thead>"+
	                    "<tbody>";

	            $.each(json,function(i,x){
	                innerHtml +=
	                    "<tr>"+
	                        "<td class='center'>"+
	                            "<div class='visible-md visible-lg hidden-sm hidden-xs'>"+
	                                "<a href='#' class='btn btn-xs btn-blue tooltips' data-idpessoa='"+x.idpessoa+"'"+
	                                "data-despessoa='"+x.despessoa+"'data-placement='top' data-original-title='Selecionar'>"+
	                                    "<i class='fa fa-arrow-circle-left' data-size='m'></i>"+
	                                "</a>"+
	                            "</div>"+
	                        "</td>"+
	                        "<td>"+x.despessoa+"</td>"+
	                        "<td>"+x.descontato_telefone+
	                        "</td><td>"+x.descontato_email+"</td>"+
	                        "<td>"+x.despessoatipo+"</td>"+
	                    "</tr>";

	            });

	            innerHtml+="</tbody></table>";
	            $(".modal.vincularpessoa div.modal-body").html(innerHtml);

	        },
	        beforeSend:function(){
	            $(".loadmask_pessoavincula").block(page.configUi);
	        }
	        }).done(function(){
	            $("#table_data_vincularpessoa").DataTable({
	                "bDestroy":true,
	                "bRetriving":true,
	                "lengthMenu": [[5, 10], [5, 10]],
	            });
	            if(type =="Remetente"){
	                $("#table_data_vincularpessoa a").on('click',function(){
	                    $("[name = idpessoaremetente].setup").val($(this).data("idpessoa"));
	                    $("[name = despessoa_remetente].setup").val($(this).data("despessoa"));

	                    $(".vincularpessoa").modal('hide');    
	                }); 

	            }else{

	                $("#table_data_vincularpessoa a").on('click',function(){
	                    $("[name = idpessoadestinatario].setup").val($(this).data("idpessoa"));
	                    $("[name = despessoa_destinatario].setup").val($(this).data("despessoa"));

	                    $(".vincularpessoa").modal('hide');                     
	                });
	            }
	            $(".loadmask_pessoavincula").unblock();
	        });


	});
	////////////////////////////////////////////////////////////////////////////////////////////// 
	//////////////////////////////////CORRESPONDENCIA HISTORICO/////////////////////////////////// 
	$("li.tab_correspondencia_historicos a").on("click",function(){

        if(historico.obj.idcorrespondencia == page.actions.form_params.idcorrespondencia){
            if(!$(".correspondencia_historico").children().length > 0 && page.actions.form_params.idcorrespondencia !='undefined'){
                    historico.obj.idcorrespondencia = page.actions.form_params.idcorrespondencia;
                    historico.load();
            }
        }else{
            historico.obj.idcorrespondencia = page.actions.form_params.idcorrespondencia;
            historico.load();
        }
    });


    $("#tab_correspondencia_historicos .addhistorico").on("click",function(){
        if ($(".correspondencia-historico-input textarea").val().length > 0){

            var txthistorico = $(".correspondencia-historico-input textarea").val();
            
            historico.obj.descorrespondenciahistorico = txthistorico;
            historico.save();
        }
    });

    $("#form_send_email").submit(function(e){
        
        e.preventDefault();
        
        $(this).find("input[name*=idarquivo]").val(arquivos.fileid.join(";"))
        
        var params = $(this).serializeArray2();
        
        email.sendemail($(this),params);


        $('#tab_correspondencia_arquivos input').iCheck('uncheck');

    });

    $(".send-email").on("click",function(e){
        e.preventDefault();
        $(".correspondenciaemail").modal();
    });



    if($("#form-fileupload")[0] != undefined ){

        $("#show_upload_file").addClass("no-display");
        $('.files .name').html("");
        $('.files .size').html("");
        $('.progress-bar').css("width","0%").html("");
        $("#form-fileupload")[0].reset();

    }

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-uk-pre": function ( a ) {
        var ukDatea = a.split('/');
        return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
    },

    "date-uk-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-uk-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
    });

    $("#select_idstatus").on("change",function(el){

        if($(this).val()==1){
            $("[name='dtrecebido']").prop("required","true");
        }else{
            $("[name='dtrecebido']").removeAttr("required");
        }
    });

    //Define o limite de definição da Data do recebimento
    $("[name='dtrecebido']").attr("max",moment().format("YYYY-MM-DD"));

	////////////////////////////////////////////////////////////////////////////////////////////// 
})(jQuery);