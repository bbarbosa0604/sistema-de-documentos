(function($){

	$(".btn-add-field").on('click', function() {
		$("input[name=idcampo]").val(0);
		$("input[name=idcampo]").val("");
		$("input[name=desnomeexibicao]").val("");
		$("select[name=idcampotipo]").val("");
		$("#formulariocampos").modal("show");
		return false;
	});

	$(document).find("[data-target='.formulario']").on('click', function(){
		$("input[name=idformulario]").val($(this).data("id"));		
		$("#table_campos tbody").html("");
		$.getJSON('actions/get.formulario.campo.php', {idformulario: $(this).data("id")}, function(json, textStatus) {
			$.each(json, function(fields, obj){
				$tpl = "<tr>";
				$tpl += "<td>"+obj.desnomeexibicao+"</td>";
				$tpl += '<td><a href="#" class="btn btn-sm btn-red tooltips btn-remove" data-id="'+obj.idcampo+'"><i class="fa fa-times"></i></a></td>';
				$tpl += '<td><a href="#" class="btn btn-sm btn-blue tooltips btn-edit" data-id="'+obj.idcampo+'"><i class="fa fa-pencil"></i></a></td>';
				$tpl += "</tr>";
				$("#table_campos tbody").append($tpl);
			});
		});
	});

	$("#idcampotipo").on('change', function() {
		if($(this).val()==5){
			$("#options").removeClass("hide");
			$(".opts").html('');
		} else{
			$("#options").addClass("hide");
			$(".opts").html('');
		}
	});

	$(".btn-add-options").on('click', function() {

		$tpl = '<div class="input-group" style="padding:5px 5px;">'+
	      	'<input type="text" class="form-control" name="opts" data-id="0">'+
	      	'<div class="input-group-addon btn btn-red btn-remove-option" data-id="0"><i class="fa fa-times"></i></div>'+
    	'</div>';
		$(".opts").append($tpl);
		return false;
	});

	$(".btn-save-campos").on('click', function() {
		var idformulario = $("input[name=idformulario]").val();
		var idcampo = $("input[name=idcampo]").val();
		var desnomeexibicao = $("input[name=desnomeexibicao]").val();
		var idcampotipo = $("select[name=idcampotipo]").val();

		if(desnomeexibicao == ""){
			alert("Digite um nome de exibição");
			return false;
		}

		if(idcampotipo == ""){
			alert("Escolha o tipo do campo");
			return false;
		}

		if(idcampotipo == 5 && $(".opts input").length == 0){
			alert("Foi escolhido opção SELECT e não inserido nenhum Option");
			return false;
		}


		var options = [];
		var valid = true;
		$.each($(".opts input"), function(a, b){
			if($(b).val() == ""){
				valid = false;	
			}
			options.push({
				desvalue:$(b).val(),
				idcamposelect:$(b).data("id")
			})
		});

		if(valid == false){
			alert("Os OPTIONS precisam ter valor");
			return false;
		}

		$.post("actions/add.campo.php", {
			idformulario: idformulario,
			desnomeexibicao:desnomeexibicao,
			idcampotipo:idcampotipo,
			idcampo:idcampo,
			options:options,
		},function( data ) {
			var jsonReturn = $.parseJSON(data);
			if(jsonReturn.success === false){
				alert(data.msg);
			} else {          
				alert("Campo salvo com sucesso!");
			}
		});

	});

	$("#table_campos tbody").on('click', '.btn-remove', function(event) {
		$obj = $(this);
		$.post("actions/remove.campo.php", {
			idcampo: $(this).data("id"),
		},function( data ) {
			var jsonReturn = $.parseJSON(data);
			if(jsonReturn.success === false){
				alert(data.msg);
			} else {          
				alert("Campo removido com sucesso!");
				$obj.closest('tr').remove();
			}
		});
	});

	$("#table_campos tbody").on('click', '.btn-edit', function(event) {
		$.getJSON('actions/get.campo.php', {idcampo: $(this).data("id")}, function(json, textStatus) {
			$("input[name=idcampo]").val(json.idcampo);
			$("input[name=desnomeexibicao]").val(json.desnomeexibicao);
			$("select[name=idcampotipo]").val(json.idcampotipo);
			
			if(json.idcampotipo == 5){
				$("#options").removeClass("hide");
				$(".opts").html('');
				$.getJSON('actions/get.camposelect.php',{
					idcampo: json.idcampo
				}, function(json, textStatus){
					$.each(json, function(a, b){
						$tpl = '<div class="input-group" style="padding:5px 5px;">'+
					      	'<input type="text" class="form-control" name="opts" value="'+b.desvalue+'" data-id="'+b.idcamposelect+'">'+
					      	'<div class="input-group-addon btn btn-red btn-remove-option" data-id="'+b.idcamposelect+'"><i class="fa fa-times"></i></div>'+
				    	'</div>';
						$(".opts").append($tpl);
					});
				});
			} else {
				$("#options").addClass("hide");
				$(".opts").html('');
			}
		});
		$("#formulariocampos").modal("show");
	});

	$('.opts').on('click', '.btn-remove-option', function(event) {
		$obj = $(this);
		$.post("actions/remove.camposelect.php", {
			idcamposelect: $(this).data("id"),
		},function( data ) {
			var jsonReturn = $.parseJSON(data);
			if(jsonReturn.success === false){
				alert(data.msg);
			} else {          
				alert("Campo removido com sucesso!");
				$obj.closest('.input-group').remove();
			}
		});
		return false;
	});

})(jQuery);