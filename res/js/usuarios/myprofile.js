(function($){ 

    Main.init();

    $("#form_myprofile_usuario").unbind("submit");
       
    $("#form_myprofile_usuario").on("submit",function(event){
        event.preventDefault();
        var params = $(this).serializeArray();

        if($(this).find("[id*=dessenha]").val().length < 6){
            toastr.error("A senha deve ter no minimo 6 caracteres","Definição de senha inválida!");
            return false;
        }

        page.actions = {
            "form_params":params,
            "url_save":"actions/save.usuario.php"
        }

        page.save_no_modal($(this));

    });
})(jQuery);