(function($){

	page.setDataTable();


	$("#form_usuario").unbind("submit");
	$(document).find("[data-target='.usuario']").unbind("click");
	$(document).find("[data-target='.removeusuario']").unbind("click");

	$("#form_usuario").submit(function(event){
	    event.preventDefault();

	    if($(this).find("[id*=dessenha]").val().length < 6){
	        toastr.error("A senha deve ter no minimo 6 caracteres","Definição de senha inválida!");
	        return false;
	    }
		page.save($("#form_usuario"),$(".usuario"),false,function(d){
			if (d) {
	            page.reload_content(
	                {
	                    url_reload:"actions/get.usuarios.load.php",
	                    js:"/res/js/usuarios/usuarios.js",
	                    params:''
	                }
	            );
	        }
		});

	});

	$(document).find("[data-target='.usuario']").on('click', function(){

	    $(".usuario").find(".modal-title").html($(this).data("titlemodal"));
	    
	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "form_params":{
	                    "idusuario": $(this).data("form-params")
	                }
	    }
	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        page.load(page.actions.form_params,("#form_usuario"));
	    }

	});

	$(document).find("[data-target='.removeusuario']").on('click', function(event){
	    event.preventDefault();

	    page.actions = {
	            "url_delete":  $(this).data("form-url-delete"),
	            "form_params": {
	                "idusuario": $(this).data('form-params')
	            }
	    }
	    bootbox.confirm("<h1>Deseja excluir o usuario?</h1>",function(result){
	        if(result)page.delete(page.actions,function(d){
	        	if (d) {
		            page.reload_content(
		                {
		                    url_reload:"actions/get.usuarios.load.php",
		                    js:"/res/js/usuarios/usuarios.js",
		                    params:''
		                }
		            );
		        }
	        });
	    });


	});

})(jQuery);