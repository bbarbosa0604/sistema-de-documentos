var Charts = function() {
	"use strict";


	var dashboard_chart = function() {

		d3.json('actions/dashboard.chart.php',function(data) {

			nv.addGraph(function() {
				var chart = nv.models.pieChart().x(function(d) {
					return d.label;
				}).y(function(d) {
					return d.value;
				}).showLabels(true)
				.labelType("percent")
				.color(data.colors);

				d3.select("#tipocorrespondencia-chart svg").datum(data.tipos).transition().duration(200).call(chart);


				return chart;
			});

		//Donut chart example
			nv.addGraph(function() {
				var chart = nv.models.pieChart().x(function(d) {
					return d.label;
				}).y(function(d) {
					return d.value;
				}).showLabels(true)//Display pie labels
				.labelThreshold(.05)//Configure the minimum slice size for labels to show up
				.labelType("percent")//Configure what type of data to show in the label. Can be "key", "value" or "percent"
				.donut(true)//Turn on Donut mode. Makes pie chart look tasty!
				.donutRatio(0.35)
				.color(data.colors)//Configure how big you want the donut hole size to be.
				;
				chart.pie.donutLabelsOutside(true).donut(true);
				d3.select("#statuscorrespondencia-chart svg").datum(data.status).transition().duration(200).call(chart);

				return chart;
			});

		});
	};	

	return {
		//main function to initiate template pages
		init : function() {
			dashboard_chart();
		}
	};
}();
