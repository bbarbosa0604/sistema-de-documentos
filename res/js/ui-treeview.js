var UITreeview = function() {
	"use strict";
	//function to initiate jquery.dynatree

	var runTreeView = function(idusuario) {

		function loadJstree(){

	        $('#tree_permissoes').jstree({
	            'plugins' : ["checkbox", "types","json_data","ui","types"], 
	            'core' : {
		            "themes" : {
		                "responsive" : false
	            },
	            data:{
	                'url' : 'actions/list.menus.php?idusuario='+idusuario,
	                'type': 'get',
	                'dataType': 'json',
	                'contentType':'application/json;'
	                }
	            }
	        });
	    }

	    loadJstree();

		var to = false;
		$('#tree_4_search').keyup(function() {
			if (to) {
				clearTimeout(to);
			}
			to = setTimeout(function() {
				var v = $('#tree_4_search').val();
				$('#tree_4').jstree(true).search(v);
			}, 250);
		});
	};
	return {
		//main function to initiate template pages
		init : function() {
			runTreeView(0);
		},
		reload:function(idusuario){
			runTreeView(idusuario);
		}
	};
}();
