(function($){

	$.getScript(PATH_URL+"/res/js/pages-calendar.js",function(){
		Calendar.init();		
	});

	$.getScript(PATH_URL+"/res/js/subview.js");

	Main.init();
	FormElements.init();

	$(document).find("[data-target='.correspondencia']").unbind('click');
	$("button[data-target*='.vincularpessoa']").unbind("click")
	$("#form_correspondencia").unbind("submit");

/////////////////////////////CORRESPONDENCIA////////////////////////////////////
/////////////////////////////CRIACAO E EDICAO///////////////////////////////////

	$("#form_correspondencia").submit(function(event){
	    event.preventDefault();
	    page.save($("#form_correspondencia"),$(".correspondencia"));

	});
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////PARAMETRIZACAO/////////////////////////////////////
	$(document).find("[data-target='.correspondencia']").on('click', function(){

	    $(".correspondencia").find(".modal-title").html($(this).data("titlemodal"));

	      if($(this).data("enable-all-tabs")){
	        $("#tab_correspondencia").find(".disabledTab").removeClass("disabledTab");
	      }else{
	        $("#tab_correspondencia li:not(:first-child)").addClass("disabledTab");
	      }

	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "url_delete": $(this).data("form-url-delete"),
	            "form_params":{
	                    "idcorrespondencia": $(this).data("form-params")
	                }
	    }
	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        page.load(page.actions.form_params,("#form_correspondencia"));

	    }

	});

	$("#modal-list-correspondecias table tbody").on('click', '.btn-view', function(event) {
		$(".correspondencia").find(".modal-title").html($(this).data("titlemodal"));

	      if($(this).data("enable-all-tabs")){
	        $("#tab_correspondencia").find(".disabledTab").removeClass("disabledTab");
	      }else{
	        $("#tab_correspondencia li:not(:first-child)").addClass("disabledTab");
	      }

	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "url_delete": $(this).data("form-url-delete"),
	            "form_params":{
	                    "idcorrespondencia": $(this).data("form-params")
	                }
	    }
	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        page.load(page.actions.form_params,("#form_correspondencia"));

	    }
	});

	$(".btn-list").on('click', function() {
		$dt = $(".pricing-table").data("select");
		$idstatus = $(this).data("id");
		$desstatus = "";
		switch($idstatus){
			case 1:
				$desstatus = "Recebido";
				break;
			case 2:
				$desstatus = "Pendente";
				break;
			case 3:
				$desstatus = "Enviado";
				break;
		}

		$("#modal-list-correspondecias").find("h4").html("Detalhamento");
		$("#modal-list-correspondecias").find("small").html("Lista de Correspondências do tipo "+$desstatus+" de "+$dt);

		$("#modal-list-correspondecias table tbody").html("");
		$tpl = "";
		$.getJSON("actions/get.correspondencia.php", {
			dt: $dt,
			idstatus: $idstatus,
		} ,function(data){
			$.each(data, function(a, b){
				if(b.idstatus == 1) $status = '<span class="label label-sm label-success">'+b.desstatus+'</span>';
				if(b.idstatus == 2) $status = '<span class="label label-sm label-danger">'+b.desstatus+'</span>';
				if(b.idstatus == 3) $status = '<span class="label label-sm label-primary">'+b.desstatus+'</span>';

				var d = new Date(b.dtcadastro);

				$tpl = "<tr>";
				$tpl += "<td>"+d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+"</td>";
				$tpl += "<td>"+b.desassunto+"</td>";
				$tpl += "<td>"+b.despessoa_remetente+"</td>";
				$tpl += "<td>"+b.despessoa_destinatario+"</td>";
				$tpl += "<td>"+b.desdocumentotipo+"</td>";
				$tpl += "<td>"+$status+"</td>";
				$tpl += '<td><td class="center"><div class="visible-md visible-lg hidden-sm hidden-xs"><a href="#" class="btn btn-xs btn-blue tooltips btn-view" data-target=".correspondencia" data-toggle="modal" data-placement="top" data-original-title="Visualizar" data-form-url-load="actions/get.correspondencia.php" data-enable-all-tabs=true data-titlemodal="Editando Correspondencia" data-form-url-save="actions/save.correspondencia.php" data-form-params="'+b.idcorrespondencia+'"><i class="fa fa-edit"></i></a></div></td></td>';
				$tpl += "</tr>";
				$("#modal-list-correspondecias table tbody").append($tpl);
			});
		});

		
		$("#modal-list-correspondecias").modal("show");


	});

	$("button[data-target*='.vincularpessoa']").on("click", function(e) {
	    e.preventDefault();

	    var type = $(this).data("type");
	    if (type != undefined) $(".vincularpessoa h4").html("Vincular o " + type.toString());

	    var actionUrl = $(".vincularpessoa").data("url");

	    $.ajax({
	        method:"POST",
	        dataType:'json',
	        url:actionUrl,
	        success: function(json){
	            var innerHtml = "";                
	            $(".modal.vincularpessoa div.modal-body").html("");

	            innerHtml +=
	                "<table class='table table-striped table-hover display' id='table_data_vincularpessoa'>"+
	                    "<thead>"+
	                        "<tr>"+
	                            "<th>#</th>"+
	                            "<th>Nome</th>"+
	                            "<th>Telefone</th>"+
	                            "<th>E-mail</th>"+
	                            "<th>Tipo</th>"+
	                        "</tr>"+
	                    "</thead>"+
	                    "<tbody>";

	            $.each(json,function(i,x){
	                innerHtml +=
	                    "<tr>"+
	                        "<td class='center'>"+
	                            "<div class='visible-md visible-lg hidden-sm hidden-xs'>"+
	                                "<a href='#' class='btn btn-xs btn-blue tooltips' data-idpessoa='"+x.idpessoa+"'"+
	                                "data-despessoa='"+x.despessoa+"'data-placement='top' data-original-title='Selecionar'>"+
	                                    "<i class='fa fa-arrow-circle-left' data-size='m'></i>"+
	                                "</a>"+
	                            "</div>"+
	                        "</td>"+
	                        "<td>"+x.despessoa+"</td>"+
	                        "<td>"+x.descontato_telefone+
	                        "</td><td>"+x.descontato_email+"</td>"+
	                        "<td>"+x.despessoatipo+"</td>"+
	                    "</tr>";

	            });

	            innerHtml+="</tbody></table>";
	            $(".modal.vincularpessoa div.modal-body").html(innerHtml);

	        },
	        beforeSend:function(){
	            $(".loadmask_pessoavincula").block(page.configUi);
	        }
	        }).done(function(){
	            $("#table_data_vincularpessoa").DataTable({
	                "bDestroy":true,
	                "bRetriving":true,
	                "lengthMenu": [[5, 10], [5, 10]],
	            });
	            if(type =="Remetente"){
	                $("#table_data_vincularpessoa a").on('click',function(){
	                    $("[name = idpessoaremetente].setup").val($(this).data("idpessoa"));
	                    $("[name = despessoa_remetente].setup").val($(this).data("despessoa"));

	                    $(".vincularpessoa").modal('hide');    
	                }); 

	            }else{

	                $("#table_data_vincularpessoa a").on('click',function(){
	                    $("[name = idpessoadestinatario].setup").val($(this).data("idpessoa"));
	                    $("[name = despessoa_destinatario].setup").val($(this).data("despessoa"));

	                    $(".vincularpessoa").modal('hide');                     
	                });
	            }
	            $(".loadmask_pessoavincula").unblock();
	        });


	});

    $("#select_idstatus").on("change",function(el){

        if($(this).val()==1){
            $("[name='dtrecebido']").prop("required","true");
        }else{
            $("[name='dtrecebido']").removeAttr("required");
        }
    });

    //Define o limite de definição da Data do recebimento
    $("[name='dtrecebido']").attr("max",moment().format("YYYY-MM-DD")); 

})(jQuery);