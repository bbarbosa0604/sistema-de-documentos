(function($){

	page.setDataTable();
	

	////////////////////////REMOVE EVENT ELEMENT/////////////////////////////////////
	$("#form_pessoa").unbind("submit");
	$(document).find("[data-target='.pessoa']").unbind("click");
	$(document).find("[data-target='.pessoaview']").unbind("click");
	$(document).find("[data-target='.removepessoa']").unbind("click");
	$("[name*=descontato-email]").unbind("blur");

	/////////////////////////////////////////////////////////////////////////////////

	$("#form_pessoa").submit(function(event){
	    event.preventDefault();

	    page.save($("#form_pessoa"),$(".pessoa"),false,function(d){
			if(d){
				page.reload_content(
		            {
		                url_reload:"actions/get.pessoas.load.php",
		                js:"/res/js/pessoas/pessoas.js",
		                params:''
		            }
		        );
		    }
	    });

	});

	$(document).find("[data-target='.pessoa']").on('click', function(e){

		    page.actions = {
		            "url_save":   $(this).data("form-url-save"),
		            "url_load":   $(this).data("form-url-load"),
		            "url_delete": $(this).data("form-url-delete"),
		            "form_params":{
		                    "idpessoa": $(this).data("form-params")
		                }
		    }
			
		    if (page.actions.url_load != "" && page.actions.url_load != undefined){
		        page.load(page.actions.form_params,("#form_pessoa"));
		    }

		});
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////PARAMETRIZACAO/////////////////////////////////////////

	$(document).find("[data-target='.pessoaview']").on('click', function(e){

	    page.actions = {
	            "url_save":   $(this).data("form-url-save"),
	            "url_load":   $(this).data("form-url-load"),
	            "url_delete": $(this).data("form-url-delete"),
	            "form_params":{
	                    "idpessoa": $(this).data("form-params")
	                }
	    }
		
	    $(".main-content").addClass("loadmask");
	    $(".loadmask").block(page.configUiPage);

	    $.ajax({
	        url:$(this).data("url"),
	        data:page.actions.form_params,
	        dataType:"html",
	        method:"POST",
	        success:function(data){
	            $(".main-content").html(data);
	        },
	        error:function(){
	            toastr.error("Erro ao Carregar a Pagina");
	        },
	        complete:function(data){
	            $(".loadmask").unblock();
	            $(".main-content").removeClass("loadmask");
	        }

	    });
	    

	    if (page.actions.url_load != "" && page.actions.url_load != undefined){
	        page.load(page.actions.form_params,("#form_pessoa"));
	    }

	});

	////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////DELEÇÃO//////////////////////////////////////////

	$(document).find("[data-target='.removepessoa']").on('click', function(){

	    page.actions = {
	            "url_delete":  $(this).data("form-url-delete"),
	            "form_params": {
	                "idpessoa": $(this).data('form-params')
	            }
	    }
	    bootbox.confirm("<h1>Deseja excluir a pessoa ?</h1>",function(result){
	        if(result)page.delete(page.actions,function(d){
				if (d) {
					page.reload_content(
		                {
		                    url_reload:"actions/get.pessoas.load.php",
		                    js:"/res/js/pessoas/pessoas.js",
		                    params:''
		                }
		            );
		        }

	        });
	    });

	});

    $("[name*=descontato-email]").bind("blur",function(){
    	if(!page.validarEmail($(this).val()) && $(this).val().length > 0){    		
    		
    		toastr.error("E-mail Inválido!","E-mail");

    		$("[type=submit]").prop("disabled",true);

    	}else{
    		
			$("[type=submit]").prop("disabled",false);
    		
    	}

        return true; 
    });

    $("[name*=descontato-telefone]").mask("9999999999?9");

})(jQuery);