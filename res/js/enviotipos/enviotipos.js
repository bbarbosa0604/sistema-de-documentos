(function($){

	page.setDataTable();

    ///////////////////////////////REMOVE EVENT ELEMENT//////////////////////////

    $("#form_enviotipo").unbind("submit");
    $(document).find("[data-target='.removeenviotipo']").unbind("click");
    $(document).find("[data-target='.enviotipo']").unbind("click");
    ///////////////////////////////////////////////////////////////////////////////


    $("#form_enviotipo").submit(function(event){
        event.preventDefault();

        page.save($("#form_enviotipo"),$(".enviotipo"),false,function(d){
            if (d) {
                page.reload_content(
                    {
                        url_reload:"actions/get.enviotipos.load.php",
                        js:"/res/js/enviotipos/enviotipos.js",
                        params:''
                    }
                );
            }
        });

    });

    ////////////////////////////////////////////////////////////////////////////////
    $(document).find("[data-target='.enviotipo']").on('click', function(){

        $(".enviotipo").find(".modal-title").html($(this).data("titlemodal"));
        
        page.actions = {
                "url_save":   $(this).data("form-url-save"),
                "url_load":   $(this).data("form-url-load"),
                "url_delete": $(this).data("form-url-delete"),
                "form_params":{
                        "idenviotipo": $(this).data("form-params")
                    }
        }
        if (page.actions.url_load != "" && page.actions.url_load != undefined){
            page.load(page.actions.form_params,("#form_enviotipo"));
        }

    });
    ////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////DELECAO/////////////////////////////////////////


    $(document).find("[data-target='.removeenviotipo']").on('click', function(){

        page.actions = {
                "url_delete":  $(this).data("form-url-delete"),
                "form_params": {
                    "idenviotipo": $(this).data('form-params')
                }
        }
        bootbox.confirm("<h1>Deseja excluir esse Tipo de Envio ?</h1>",function(result){
            if(result)page.delete(page.actions,function(d){
                if (d) {
                    page.reload_content(
                        {
                            url_reload:"actions/get.enviotipos.load.php",
                            js:"/res/js/enviotipos/enviotipos.js",
                            params:''
                        }
                    );
                }
            });
        });

    });
 
})(jQuery);