/**
 *
 * @name menu
 * @version 0.1
 * @requires jQuery v1.7+
 * @author João Rangel
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 *
 * For usage and examples, buy TopSundue:
 * 
 */
(function($){

 	$.fn.extend({ 
 		
		//pass the options variable to the function
 		menu: function(options) {

			//Set the default values, use comma to separate the settings, example:
			var defaults = {
				debug:false,
				idItemTpl:false,
				subitensname:"itens",
				data:[]
			};
				
			var o =  $.extend(defaults, options);

			if(o.debug === true) console.info(o.data);

			var renderMenuList = function(list, template){

				var menus = [];

				$.each(list, function(index, menu){

					var $menu = $(template(menu)),
						$sub = $menu.find("ul:first");

					if(menu[o.subitensname] instanceof Array && menu[o.subitensname].length){
						$sub.html(renderMenuList(menu[o.subitensname], template));
						$menu.find("a").append('<i class="icon-arrow"></i>');
					}else{
						$sub.remove();
					}

					menus.push($menu[0].outerHTML);

				});

				return menus.join("");

			};

    		return this.each(function() {
				
    			if(o.debug === true) console.info("options", o);

    			if(!o.idItemTpl){
    				throw new Error("Informe o ID do template na opção idItemTpl");
    				return false;
    			}

				var $el = $(this),
					template = Handlebars.compile($(o.idItemTpl).html());

				if(o.debug === true) console.info("element", this);
				if(o.debug === true) console.info("template", $(o.idItemTpl));

				$el.html(renderMenuList(o.data, template));
			
    		});
    	}
	});
	
})(jQuery);