/**
 *
 * @name picker-file
 * @version 0.1
 * @requires jQuery v1.7+
 * @author João Rangel
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 *
 * For usage and examples, buy TopSundue:
 * 
 */
(function($){

	function getPageSize(){

		var w = window,
		    d = document,
		    e = d.documentElement,
		    g = d.getElementsByTagName('body')[0],
		    x = w.innerWidth || e.clientWidth || g.clientWidth,
		    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

		    return {w:x, h:y};

	}

	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};

	function loadArquivosPessoa(callback){

		$.getJSON(PATH_URL+"/modules/files/actions/getArquivosByPessoa.php", function(r){

			if(r.success){

				if(typeof callback === "function") callback(r.data);

			}else{

				toastr.error(r.error || "Tente novamente mais tarde.");

			}

		});

	}

 	$.fn.extend({ 
 		
		//pass the options variable to the function
 		filepicker: function(options) {

 			var _this = this;

			//Set the default values, use comma to separate the settings, example:
			var defaults = {
				debug:false,
				multiple:false,
				accept:"",
				text:"Selecionar Arquivo...",
				icon:'<i class="fa fa-upload"></i>',
				callback:function(){},
				alertError:function(msg){

					if(typeof toastr === "object") toastr.error(msg);

				},
				alertSuccess:function(msg){

					if(typeof toastr === "object") toastr.success(msg);

				},
				alertInfo:function(msg){

					if(typeof toastr === "object") toastr.info(msg);

				},
				tplModal:'<div class="modal bs-example-modal-lg">	<div class="modal-dialog modal-lg">		<div class="modal-content">				<div class="modal-header panel-green" style="background-color:#22262F; color:white">					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:white">						&times;					</button>					<h4 class="modal-title">Selecionar Arquivos</h4>					<iframe name="filepickeriframe" src="about:blank" style="display:none"></iframe>				</div>				<div class="modal-body">					<div class="panel panel-white hide" id="panel-uploads">						<div class="panel-body">							<table class="table table-hover table-striped">								<thead>									<tr>										<th>Nome do Arquivo</th>										<th>Tipo</th>										<th>Tamanho</th>										<th>Status</th>										<th></th>									</tr>								</thead>								<tbody>																	</tbody>							</table>						</div>					</div>										<div class="tabbable tabs-left">						<ul id="myTab3" class="nav nav-tabs">							<li class="active">								<a href="#tab-upload" data-toggle="tab">									<i class="pink fa fa-desktop"></i> Upload								</a>							</li>						<li class="">								<a href="#tab-myfiles" data-toggle="tab">									<i class="blue fa fa-folder-open"></i> Meus Arquivos								</a>							</li>							<li class="">								<a href="#tab-url" data-toggle="tab">									<i class="fa fa-link"></i> URL								</a>							</li>						</ul>						<div class="tab-content" style="height:{{tabContentHeight}}px; width:{{tabContentWidth}}px;">							<div class="tab-pane fade in active" id="tab-upload">																<div class="tab-upload-content">									<h3>Arraste arquivos para cá</h3>									<h6>Ou, se preferir...</h6>									<button type="button" class="btn btn-gray btn-upload">Selecione arquivos do computador</button>								</div>							</div>							<div class="tab-pane fade" id="tab-myfiles">															<ul id="Grid" class="list-unstyled">																	</ul>							</div>							<div class="tab-pane fade" id="tab-url"><div class="form-group"><label class="control-label" for="desurl" style="font-weigth:bold">Cole um URL de imagem aqui</label><span class="input-icon"><input type="url" id="desurl" class="form-control"><i class="fa fa-link"></i></span></div><div class="row"><div class="col-md-12 hide" id="image-preview"></div><div class="col-md-12" id="text-instructions"><p style="margin-top:64px; text-align:center">Se seu URL estiver correto, uma visualização da imagem será exibida aqui. Imagens grandes poderão demorar alguns minutos para aparecer.</p><p style="text-align:center">Lembre-se de que usar as imagens de outras pessoas na web sem a permissão delas é falta de educação e, o que é pior, infringe leis de direitos autorais.</p></div></div>														</div>						</div>					</div>				</div>				<div class="modal-footer">					<button type="button" data-dismiss="modal" class="btn btn-light-grey btn-cancel">						Cancelar					</button>					<button type="submit" class="btn btn-green" disabled="disabled">						Inserir					</button>				</div>		</div>	</div></div>',
				tplUploadRow:'<tr data-name="{{name}}"><td style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{name}}</td><td>{{type}}</td><td>{{sizeFormat}}</td><td class="hidden-xs">{{status}}</td><td class="center"><i class="status fa fa-refresh fa-spin"></i></td></tr>',
				tplMyFilesItem:'<li data-idarquivo="{{idarquivo}}" class="col-md-3 col-sm-6 col-xs-12 mix category_1 gallery-img" data-cat="1" style="display: inline-block;">'+
									'<div class="portfolio-item">'+
										'<a class="thumb-info" href="#">'+
											'<img style="width:145px; height:145px;" src="'+PATH_URL+'/res/arquivos/{{desarquivo}}" class="img-responsive" alt="">'+
											'<span class="thumb-info-title"> {{desarquivotipo}} </span>'+
										'</a>'+
										'<div class="chkbox"></div>'+
									'</div>'+
								'</li>'
			};
				
			var o =  $.extend(defaults, options);

			_this.setEventsInThumbs = function($modal){

				$modal.find('.chkbox').off('click').on('click', function () {
		            if ($(this).parent().hasClass('selected')) {
		                $(this).parent().removeClass('selected').children('a').children('img').removeClass('selected');
		            } else {
		                $(this).parent().addClass('selected').children('a').children('img').addClass('selected');
		            }
		        });

			}

			$.event.trigger({
				type: "upload_error",
				time: new Date()
			});
			$.event.trigger({
				type: "upload_complete",
				time: new Date()
			});

    		return _this.each(function() {

    			var t = this;
    			t.selecteds = [];

				var 
					size = getPageSize(), 
					$element = $(this), 
					tplModal = Handlebars.compile(o.tplModal), 
					tplUploadRow = Handlebars.compile(o.tplUploadRow), 
					tplMyFilesItem = Handlebars.compile(o.tplMyFilesItem), 
					$modal = $(tplModal({
						width:size.w,
						height:size.h
					})),
					$iframe = $modal.find('iframe[name="filepickeriframe"]');

				if($element[0].tagName !== "BUTTON"){
					console.error("O elemento filepicker deve ser uma tag BUTTON ", $element);
					return false;
				}

				if($element[0].name){
					var name = $element[0].name+"-idarquivo[]";
				}else{
					var name = "idarquivo[]";
				}

				if(!$element.text()){

					$element.html(o.icon +" "+ o.text).addClass("btn-light-grey");

				}

				$element.wrap('<div class="filepicker-container"></div>');
				$element.closest(".filepicker-container").append('<p class="help-block"></p>');

				var $btnChangeFiles = $('<button type="button" class="btn btn-default btn-change hide"><i class="fa fa-refresh"></i> Alterar Arquivos Selecionados</button>');
				$element.closest(".filepicker-container").append($btnChangeFiles);

				$btnChangeFiles.on("click", function(){

					t.selecteds = [];
					setButtonStatus("");
					$btnChangeFiles.addClass("hide");
					$element.removeClass("hide");

					$modal.find("#panel-uploads table").find("tbody tr").remove();

					$modal.find("#panel-uploads table").closest(".panel").addClass("hide");
					$modal.find(".tabbable").removeClass("hide");

					$element.closest(".filepicker-container").find('[name="'+name+'"]').each(function(){
						$(this).remove();
					});

					$element.trigger("click");

				});

				for (var i = 0; i < $element[0].attributes.length; i++) {
					var attr = $element[0].attributes[i];
					if(attr.name === "multiple"){
						if(attr.value === "true"){
							o.multiple = true;
						}else{
							o.multiple = false;
						}
					}
				};

				if($element.attr("accept")) o.accept = $element.attr("accept");	

				$modal.find(".modal-body, .tabbable").css({
					"padding": 0,
					"margin": 0
				});

				$modal.find(".tab-content, .panel-body").css({
					"overflow": "auto",
					"margin": 0,
					"height":parseInt(size.h*.7)+"px"
				});

				$modal.find("#tab-upload").css({
					"text-align": "center",
					"border": "#DDD 4px dashed",
					"border-radius": "2px",
					"margin": "4px",
					"height":(parseInt(size.h*.7)-10)+"px"
				});

				$modal.find("#image-preview").css({
					"height":((parseInt(size.h*.7)-10)-57)+"px"
				});

				function selectedsToInput(to_remote){

					$element.addClass("hide");
					$element.closest(".filepicker-container").find(".btn-change").removeClass("hide");
					$element.closest(".filepicker-container").find(".help-block").text(t.selecteds.length+" arquivo(s) selecionado(s)").css({
						"float": "left",
						"margin-right": "8px",
						"line-height": "20px"
					});

					if(to_remote === true){

						$modal.block();
						
						$.post(PATH_URL+"/modules/files/actions/toRemoteServer.php", $.param({
							ids:t.selecteds.join(",")
						}), function(r){

							$modal.unblock();

							if(r.success){

								for (var i = t.selecteds.length - 1; i >= 0; i--) {
									var selected = t.selecteds[i];

									$element.closest(".filepicker-container").append('<input type="hidden" name="'+name+'" value="'+selected+'">');

								};

								$modal.find(".btn-cancel").trigger("click");

								$element.trigger("change", [t.selecteds]);

							}else{

								toastr.error(r.error || "Não foi possível salvar o arquivo.");

							}

						}).fail(function(){

							$modal.unblock();

							toastr.error("Não foi possível salvar o arquivo.");

						});

					}else{

						for (var i = t.selecteds.length - 1; i >= 0; i--) {
							var selected = t.selecteds[i];

							$element.closest(".filepicker-container").append('<input type="hidden" name="'+name+'" value="'+selected+'">');

						};

						$modal.find(".btn-cancel").trigger("click");

						$element.trigger("change", [t.selecteds]);

					}

				}

				function checkComplete(){

					var total = $modal.find("#panel-uploads table").find("tbody tr").length;
					var completos = 0;
					var success = 0;

					$modal.find("#panel-uploads table").find("tbody tr").each(function(){

						if($(this).hasClass("success") || $(this).hasClass("danger")){
							completos++;
						}
						if($(this).hasClass("success")){
							success++;
						}

					});

					if(completos >= total){

						if(t.selecteds.length > 0){

							selectedsToInput(false);

						}

					}

				}

				function clickUpload(){

					$modal.find('#filepickerform').find('[type="file"]').trigger("click");

				}

				function setButtonStatus(text){

					$element.closest(".filepicker-container").find(".help-block").text(text);

				}

				function addFileUpload(file){

					$modal.find(".modal-title").text("Por favor, aguarde...");
					setButtonStatus("Salvando arquivos no servidor...");

					var File = file;
					file.sizeFormat = bytesToSize(file.size);

					var $tr = $(tplUploadRow(file));
					$modal.find("#panel-uploads table").find("tbody").append($tr);

					$modal.find("#panel-uploads table").closest(".panel").removeClass("hide");
					$modal.find(".tabbable").addClass("hide");

					$tr
						.on("upload_error", function(){

							$(this).addClass("danger");
							$(this).find("i.status").attr("class", "fa fa-warning text-danger");

						})
						.on("upload_complete", function(){

							$(this).addClass("success");
							$(this).find("i.status").attr("class", "fa fa-check-circle text-success");

						});

					if(typeof FormData !== "function" || typeof XMLHttpRequest !== "function"){

						$tr.trigger("upload_error");
						toastr.error("Seu navegador não suporta uploads.");
						return false;

					}

					return $tr;

				}

				function callbackLoadMeusArquivos(data){

					var $grid = $modal.find("#Grid");

					if(data.length){

						$grid.html("");

						$.each(data, function(){
							var $li = $(tplMyFilesItem(this));
							$li.data("arquivo", this);
							$grid.append($li);
						});

					}

					_this.setEventsInThumbs($modal);

					if(!o.multiple){

						$grid.find(".chkbox").hide();
						$grid.find("li").off("click").on("click", function(){

							var arquivo = $(this).data("arquivo");

							t.selecteds = [arquivo.idarquivo];
							if(typeof o.callback === "function") o.callback([arquivo]);

							selectedsToInput(true);

						});

					}else{

						$btnInsert = $modal.find('[type="submit"]');
						
						$btnInsert.removeAttr("disabled").off("click").on("click", function(event){

							event.preventDefault();

							$btnInsert.attr("disabled", "disabled");

							if($grid.find(".portfolio-item.selected").length){

								var arquivos = [];
								t.selecteds = [];

								$grid.find(".portfolio-item.selected").each(function(){

									var arquivo = $(this).closest("li").data("arquivo");
									arquivos.push(arquivo);
									t.selecteds.push(arquivo.idarquivo);

								});

								if(typeof o.callback === "function") o.callback(arquivos);

								selectedsToInput(true);

							}else{

								toastr.error("Selecione pelo menos um arquivo");

							}

							return false;

						});

					}

				}

				function initEventsTabURL(){

					var $desurl = $modal.find("#desurl");
					var $btnInsert = $modal.find('[type="submit"]');

					$desurl.off("change paste").on("change paste", function(){

						var url = $(this).val();

						$desurl.next("i").attr("class", "fa fa-refresh fa-spin");

						if(url.length){

							$btnInsert.attr("disabled", "disabled");

							$.getJSON(PATH_URL+"/modules/files/actions/loadurl.php", $.param({
								url:url
							}), function(r){

								if(r.success){

									$desurl.next("i").attr("class", "fa fa-check text-success");

									$desurl.closest(".tab-pane").find("#text-instructions").addClass("hide");
									$desurl.closest(".tab-pane").find("#image-preview").removeClass("hide").css({
										"background-url":'url("'+PATH_URL+'/res/arquivos/'+r.data.desarquivo+'")',
										"background-size":"cover"
									}).data("arquivo", r.data);
						
									$btnInsert.removeAttr("disabled").off("click").on("click", function(event){

										event.preventDefault();

										$btnInsert.attr("disabled", "disabled");

										if($desurl.closest(".tab-pane").find("#image-preview").data("arquivo")){

											var arquivo = $desurl.closest(".tab-pane").find("#image-preview").data("arquivo");

											t.selecteds = [arquivo.idarquivo];

											if(typeof o.callback === "function") o.callback(arquivo);

											selectedsToInput(true);

										}else{

											toastr.error("Nenhuma imagem foi encontrada");

										}

										return false;

									});

								}else{

									$desurl.next("i").attr("class", "fa fa-warning text-danger");

									toastr.error(r.error || "Tente novamente mais tarde.");

								}

							});

						}else{

							$desurl.next("i").attr("class", "fa fa-link");

							$desurl.closest(".tab-pane").find("#text-instructions").removeClass("hide");
							$desurl.closest(".tab-pane").find("#image-preview").addClass("hide");

						}

					});

				}

				$element.on("click", function(){

					$iframe.on("load", function(){

								var response = $($iframe[0].contentDocument).find("body").text();

								if(response){

									var data = $.parseJSON(response);

									var arquivo = data.data;
									var $tr = $modal.find("#panel-uploads table").find('tr[data-name="'+arquivo.name+'"]');

									if(data.success){

										if(arquivo.idarquivo > 0){

											$tr.trigger("upload_complete");
											t.selecteds.push(arquivo.idarquivo);
											if(typeof o.callback === "function") o.callback([arquivo]);
											checkComplete();

										}else{

											$tr.trigger("upload_error");

										}

									}else{

										$tr.trigger("upload_error");

										toastr.error(data.error || "Não foi possível fazer o upload. Tente novamente mais tarde.");

										$modal.find("#panel-uploads table").closest(".panel").addClass("hide");
										$modal.find(".tabbable").removeClass("hide");

									}

								}

							});

							$modal.find("#filepickerform").remove();

							$modal.append(
								'<form style="display:none" target="filepickeriframe" id="filepickerform" enctype="multipart/form-data" method="post" action="'+PATH_URL+'/modules/files/actions/upload.php">'+
									'<input type="file" name="file" '+((o.multiple)?'multiple="true"':'')+' accept="'+o.accept+'">'+
									'<input type="hidden" name="inputname" value="file">'+
									'<button>Enviar</button>'+
								'</form>'
							);

							var $inputFile = $modal.find('#filepickerform').find('[type="file"]');

							$inputFile.on("change", function(event){

								for (var i = this.files.length - 1; i >= 0; i--) {
									var file = this.files[i];
									var $tr = addFileUpload(file);
								};

								$modal.find('#filepickerform')[0].submit();

							});

							$modal.find(".btn-upload").off("click", clickUpload).on("click", clickUpload);

					setButtonStatus("Escolhendo arquivos...");

					var $modals = $(".modal:visible");

					$modals.each(function(){

						$(this).hide();
						$(this).modal("hide");

					});

					_this.setEventsInThumbs($modal);

					$modal
						.on('shown.bs.modal', function (e) {
					 		
							$(".modal-overflow").css({
								"width":"initial",
								"marginLeft":"initial"
							});

							$modal.find("#tab-upload").css({
								"padding-top":parseInt(($modal.find("#tab-upload").height() - $modal.find("#tab-upload .tab-upload-content").height())/2)+"px"
							});

							$modal.find('[type="submit"]').attr("disabled", "disabled");

							if($modal.find('[href="#tab-myfiles"]').closest("li").hasClass("active")){

								callbackLoadMeusArquivos([]);

							}

							initEventsTabURL();

						})
						.on('hidden.bs.modal', function (e) {
					 		
							$modals.each(function(){

								$(this).modal("show");

							});

						});

						$modal.find('[href="#tab-myfiles"]').on('shown.bs.tab', function (e) {

							loadArquivosPessoa(function(data){

								callbackLoadMeusArquivos(data);
								
							});

						});

						//

					$modal.modal("show");

				});
			
    		});
    	}
	});
	
})(jQuery);
initJquery.push(function(){

	$('[picker="file"]').filepicker();

});