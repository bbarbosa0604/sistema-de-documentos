<?php 

require_once("inc/configuration.php");

$config->lock();

$url = (get("url"))?get("url"):Configuration::URL_HOME;

$page = new Page(array(
	"url"=>$url,
	"layout"=>array(
		"sidebar"=>false,
		"topbar"=>false,
		"footer"=>false,
		"wrapper"=>false
	),
	"body"=>array(
		"class"=>"lock-screen"
	)
));

$page->setTpl("lock");

?>