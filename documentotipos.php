<?php 

require_once("inc/configuration.php");

$obj = new DocumentoTipo();

$documentotipo = $obj->documentotipo_list();

$sql = new Sql();

$permissao = array();
$permissao["delecao"] = false;
$permissao["edicao"] = false;
$permissao["inclusao"] = false;

foreach ($sql->arrays("select * from tb_permissaoacao where idmenu = 6 and idusuario = ".$_SESSION["idusuario"]) as $value) {
	if($value["idacao"] == 1) $permissao["delecao"] = true;
	if($value["idacao"] == 2) $permissao["edicao"] = true;
	if($value["idacao"] == 3) $permissao["inclusao"] = true;
}

$page = new Page(array(
	"header"=>array(
		"title"=>"Tipos de Documentos",
		"subtitle"=>"visualize todos os tipos de documentos",
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
		
	),"documentotipos"=>$documentotipo,
	"p"=>$permissao
));


$page->setTpl("documentotipos");



?>