<?php 

require_once("inc/configuration.php");

$obj_correspondencia = new Correspondencia();
$obj_documentotipo = new DocumentoTipo();
$obj_enviotipo = new EnvioTipos();
$obj_correspodenciatipo = new CorrespondenciaTipo();

 $correspondencias = $obj_correspondencia->correspondencia_list();
 $documentotipos = $obj_documentotipo->documentotipo_list();
 $enviotipos = $obj_enviotipo->enviotipos_list();
 $correspondenciatipos = $obj_correspodenciatipo->correspondenciatipo_list();


$page = new Page(array(
	"header"=>array(
		"title"=>"Correspondencias",
		"subtitle"=>"visualize todos os correspondencias",
		"head"=>false
	),
	"footer"=>array(
		"foot"=>false
	),
	"layout"=>array(
		"sidebar"=>false,
		"topbar"=>false,
		"footer"=>false
	),
	"correspondencias"=>$correspondencias,
	"documentotipos"=>$documentotipos,
	"enviotipos"=>$enviotipos,
	"correspondenciatipos"=>$correspondenciatipos
));

$page->setTpl("correspondencias_load");

?>