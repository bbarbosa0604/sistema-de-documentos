<?php 

require_once("inc/configuration.php");

$obj = new Pessoa();
$pessoas = $obj->pessoa_list();

$sql = new Sql();

$permissao = array();
$permissao["delecao"] = false;
$permissao["edicao"] = false;
$permissao["inclusao"] = false;

foreach ($sql->arrays("select * from tb_permissaoacao where idmenu = 7 and idusuario = ".$_SESSION["idusuario"]) as $value) {
	if($value["idacao"] == 1) $permissao["delecao"] = true;
	if($value["idacao"] == 2) $permissao["edicao"] = true;
	if($value["idacao"] == 3) $permissao["inclusao"] = true;
}

$page = new Page(array(
	"header"=>array(
		"title"=>"Pessoas",
		"subtitle"=>"visualize todos as pessoas do sistema",
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
		
	), "pessoas"=>$pessoas,
	"p"=>$permissao
));

$page->setTpl("pessoas");



?>