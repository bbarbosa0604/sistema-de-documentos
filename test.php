<?php 
require_once("inc/configuration.php");

$sql = new Sql();

$obj_documentotipo = new DocumentoTipo();
$obj_enviotipo = new EnvioTipos();
$obj_correspodenciatipo = new CorrespondenciaTipo();

$documentotipos = $obj_documentotipo->documentotipo_list();
$enviotipos = $obj_enviotipo->enviotipos_list();
$correspondenciatipos = $obj_correspodenciatipo->correspondenciatipo_list();

 $status = $sql->arrays("call sp_status_list()");

$page = new Page(array(
	"header"=>array(
		"title"=>"Calendário",
		"subtitle"=>"visualize as correspondencias no periodo",
		"head-title"=>true,
	),
	"js"=>true,
	"documentotipos"=>$documentotipos,
	"enviotipos"=>$enviotipos,
	"correspondenciatipos"=>$correspondenciatipos,
	"status"=>$status
));

$page->setTpl("calendario");

?>