<?php 

require_once("inc/configuration.php");

$sql = new Sql();

$obj_pessoa = new Pessoa();
$pessoas_view = $obj_pessoa->pessoa_fisica_juridica_get(post('idpessoa'));

if(count($pessoas_view)){

	$obj_documentotipos = new DocumentoTipo();
	$documentotipos = $obj_documentotipos->documentotipo_list(2);

	$obj_contatotipos = new ContatoTipo();
	$contatotipos = $obj_contatotipos->contatotipos_list();

	$estados = $sql->arrays('call sp_estado_list()');

	$pessoas_view['dtnascimento'] = $pessoas_view['dtnascimento']?date_format(new DateTime($pessoas_view['dtnascimento']),'d/m/Y'):NULL;

}else{

	$pessoas_view['idpessoa'] = 0;
}

$campo = new Campo();
$c = array();

foreach ($campo->getFormulario(1) as $val) {
	$options = "";
	if($val["idcampotipo"] == 5){
		$options = $campo->getOptions($val["idcampo"]);
	}
	array_push($c, array(
		"idcampo"=>$val["idcampo"],
		"idcampotipo"=>$val["idcampotipo"],
		"desnomeexibicao"=>$val["desnomeexibicao"],
		"options"=>$options,
	));
}

$page = new Page(array(
	'header'=>array(
		'title'=>'Pessoas',
		'subtitle'=>'visualizacão dados da Pessoas',
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
	),
	'pessoas_view'=>$pessoas_view,
	'estados'=>$estados,
	'documentotipos'=>$documentotipos,
	'contatotipos'=>$contatotipos,
	'idpessoa'=>post('idpessoa'),
	'campos'=>$c,
	'camposoutros'=>$obj_pessoa->getOutros()

));

$page->setTpl('pessoas.view');
unset($obj_pessoa,$pessoas_view,$page);
?>