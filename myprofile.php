<?php 

require_once("inc/configuration.php");

$string = new String();

$page = new Page(array(
	"header"=>array(
		"title"=>"Meus Dados",
		"subtitle"=>"Dados de acesso ao Sistema",
		"head-title"=>true
	),
	"layout"=>array(
		"sidebar"=>true,
		"topbar"=>true,
		"footer"=>true
	),
));

$page->setTpl("myprofile");

?>